* 00000009
*! version 1.0.0
* Do not erase or edit this file
* It is used by Stata to track the ado and help
* files you have installed.

S http://fmwww.bc.edu/repec/bocode/e
N estout.pkg
D 22 Jun 2022
U 1
d 'ESTOUT': module to make regression tables
d 
d  estout produces a table of regression results from one or
d several models for use with spreadsheets, LaTeX, HTML, or a
d word-processor table. eststo stores a quick copy of the active
d estimation results for later tabulation. esttab is a wrapper for
d estout. It displays a pretty looking publication-style regression
d table without much typing. estadd adds additional results to the
d e()-returns for one or several models previously fitted and
d stored. This package subsumes the previously circulated esto,
d esta, estadd,  and estadd_plus. An earlier version of estout is
d available  as estout1.
d 
d KW: estimates
d KW: LaTeX
d KW: HTML
d KW: word processor
d KW: output
d 
d Requires: Stata version 8.2
d 
d Distribution-Date: 20220326
d 
d Author: Ben Jann, University of Bern
d Support: email jann@@soz.unibe.ch
d 
f _/_eststo.ado
f _/_eststo.hlp
f e/estadd.ado
f e/estadd.hlp
f e/estout.ado
f e/estout.hlp
f e/eststo.ado
f e/eststo.hlp
f e/estpost.ado
f e/estpost.hlp
f e/esttab.ado
f e/esttab.hlp
e
S http://fmwww.bc.edu/repec/bocode/r
N reghdfe.pkg
D 22 Jun 2022
U 2
d 'REGHDFE': module to perform linear or instrumental-variable regression absorbing any number of high-dimensional fixed effects
d 
d  reghdfe fits a linear or instrumental-variable regression
d absorbing an arbitrary number of categorical factors and
d factorial interactions Optionally, it saves the estimated fixed
d effects.
d 
d KW: regression
d KW: instrumental variables
d KW: fixed effects
d KW: high dimension fixed effects
d 
d Requires: Stata version 11.2
d 
d Distribution-Date: 20191118
d 
d Author: Sergio Correia, Board of Governors of the Federal Reserve System
d Support: email sergio.correia@@gmail.com
d 
f r/reghdfe.ado
f r/reghdfe.mata
f r/reghdfe_old.ado
f r/reghdfe_p.ado
f r/reghdfe_old_p.ado
f r/reghdfe_estat.ado
f r/reghdfe_parse.ado
f r/reghdfe_footnote.ado
f r/reghdfe_old_estat.ado
f r/reghdfe_old_footnote.ado
f e/estfe.ado
f r/reghdfe_header.ado
f r/reghdfe_store_alphas.ado
f r/reghdfe.sthlp
f r/reghdfe_old.sthlp
f r/reghdfe_accelerations.mata
f r/reghdfe_bipartite.mata
f r/reghdfe_class.mata
f r/reghdfe_common.mata
f r/reghdfe_constructor.mata
f r/reghdfe_lsmr.mata
f r/reghdfe_projections.mata
f r/reghdfe_transforms.mata
f r/reghdfe_mata.sthlp
e
S http://fmwww.bc.edu/repec/bocode/g
N gtools.pkg
D 22 Jun 2022
U 3
d 'GTOOLS': module to provide a fast implementation of common group commands
d 
d    gtools is a Stata package that provides a fast implementation
d of common group commands like    collapse, egen, isid, levelsof,
d contract, distinct, and so on using C plugins for a massive
d speed improvement.
d 
d KW:  data management
d KW: collapse
d KW: egen
d KW: isid
d KW: levelsof
d KW: contract
d KW: distinct
d KW: plugins
d KW: hash
d 
d Requires: Stata version 13.1
d 
d Distribution-Date: 20190403
d 
d Author: Mauricio Caceres Bravo
d Support: email mauricio.caceres.bravo@@gmail.com
d 
f g/gtools.ado
f g/gtools.sthlp
f _/_gtools_internal.ado
f _/_gtools_internal.mata
f f/fasterxtile.ado
f f/fasterxtile.sthlp
f g/gcollapse.ado
f g/gcollapse.sthlp
f g/gcontract.ado
f g/gcontract.sthlp
f g/gdistinct.ado
f g/gdistinct.sthlp
f g/gduplicates.ado
f g/gduplicates.sthlp
f g/gegen.ado
f g/gegen.sthlp
f g/gisid.ado
f g/gisid.sthlp
f g/glevelsof.ado
f g/glevelsof.sthlp
f g/gquantiles.ado
f g/gquantiles.sthlp
f g/greshape.ado
f g/greshape.sthlp
f g/gstats.ado
f g/gstats.sthlp
f g/gstats_sum.sthlp
f g/gstats_summarize.sthlp
f g/gstats_tab.sthlp
f g/gstats_winsor.sthlp
f g/gtools_macosx_v2.plugin
f g/gtools_macosx_v3.plugin
f g/gtools_unix_v2.plugin
f g/gtools_unix_v3.plugin
f g/gtools_windows_v2.plugin
f g/gtools_windows_v3.plugin
f g/gtop.ado
f g/gtop.sthlp
f g/gtoplevelsof.ado
f g/gtoplevelsof.sthlp
f g/gunique.ado
f g/gunique.sthlp
f h/hashsort.ado
f h/hashsort.sthlp
f l/lgtools.mlib
e
S http://fmwww.bc.edu/repec/bocode/e
N egenmore.pkg
D 22 Jun 2022
U 4
d 'EGENMORE': modules to extend the generate function
d 
d  This package includes various -egen- functions. For full
d details, please read the help file (ssc type egenmore.sthlp).
d Some of these routines are updates of those published in STB-50.
d _gfilter, _ggroup2, _gegroup and _gcorr require Stata 7.
d _gminutes, _grepeat and _gseconds require Stata 8.  _gcorr and
d _gnoccur were written by Nick Winter
d (nwinter@policystudies.com). _grsum2 was written by Steven
d Stillman (s.stillman@verizon.net). _gdhms, _gelap, _gelap2,
d _ghms, _gtod and _gtruncdig were written by Kit Baum
d (baum@bc.edu).
d 
d KW: egen
d KW: grouping
d KW: graphing
d KW: strings
d KW: dates
d KW: first
d KW: last
d KW: random
d KW: row
d 
d Requires: Stata version 6.0 (7.0 for _gfilter7, _ggroup2, _gegroup, _gsieve, _gnoccur; 8.2 for _gcorr, _grepeat, _gminutes, _gseconds, _gfilter, _gaxis, _gdensity, _gxtile, _gtotal0, _gmixnorm, _gw
d 
d 
d Author: Nicholas J. Cox, University of Durham
d Support: email N.J.Cox@@durham.ac.uk
d 
d Distribution-Date: 20190124
d 
f e/egenmore.sthlp
f _/_gclsst.ado
f _/_gnmiss.ado
f _/_ggmean.ado
f _/_ghmean.ado
f _/_grall.ado
f _/_grany.ado
f _/_grcount.ado
f _/_grndsub.ado
f _/_grndint.ado
f _/_gfilter.ado
f _/_gifirst.ado
f _/_gilast.ado
f _/_gncyear.ado
f _/_ghmm.ado
f _/_ghmmss.ado
f _/_gincss.ado
f _/_gfirst.ado
f _/_glastnm.ado
f _/_gnss.ado
f _/_gbom.ado
f _/_gbomd.ado
f _/_geom.ado
f _/_geomd.ado
f _/_gnwords.ado
f _/_gwordof.ado
f _/_gnvals.ado
f _/_gridit.ado
f _/_gsemean.ado
f _/_gmsub.ado
f _/_gston.ado
f _/_gntos.ado
f _/_gewma.ado
f _/_grecord.ado
f _/_gbase.ado
f _/_gdecimal.ado
f _/_ggroup2.ado
f _/_gsumoth.ado
f _/_gegroup.ado
f _/_gcorr.ado
f _/_grsum2.ado
f _/_gsieve.ado
f _/_gdhms.ado
f _/_ghms.ado
f _/_gtod.ado
f _/_gelap.ado
f _/_gelap2.ado
f _/_gnoccur.ado
f _/_gwtfreq.ado
f _/_gvar.ado
f _/_gminutes.ado
f _/_gseconds.ado
f _/_grepeat.ado
f _/_gaxis.ado
f _/_gdensity.ado
f _/_gxtile.ado
f _/_gtotal0.ado
f _/_gmixnorm.ado
f _/_gadjl.ado
f _/_gadju.ado
f _/_gdayofyear.ado
f _/_gfoy.ado
f _/_goutside.ado
f _/_gmlabvpos.ado
f _/_giso3166.ado
f _/_growmedian.ado
f _/_gwpctile.ado
f _/_gd2.ado
f _/_grownvals.ado
f _/_growsvals.ado
f _/_gtruncdig.ado
e
S http://www.stata-journal.com/software/sj4-3
N gr0002_3.pkg
D 13 Jul 2022
U 5
d SJ4-3 gr0002_3.  Lean mainstream schemes for Stata 8 graphics
d Lean mainstream schemes for Stata 8 graphics
d by Svend Juul, University of Aarhus, Denmark
d Support:  sj@@soci.au.dk
f s/scheme-lean2.scheme
f s/scheme-lean1.scheme
f s/scheme_lean.hlp
f s/scheme_lean1.hlp
f s/scheme_lean2.hlp
e
S http://fmwww.bc.edu/repec/bocode/f
N ftools.pkg
D 14 Jul 2022
U 6
d 'FTOOLS': module to provide alternatives to common Stata commands optimized for large datasets
d 
d  ftools consists of a Mata file and several Stata commands: The
d Mata file creates identifiers (factors) from variables by using
d hash functions instead of sorting the data, so it runs in time
d O(N) and not in O(N log N). The Stata commands exploit this to
d avoid sort operations,  at the cost of being slower for small
d datasets (mainly because of the cost involved in moving data from
d Stata to Mata). Implemented commands are fcollapse, fegen group,
d and fsort. Note that most of the capabilities of levels and
d contract are already supported by these commands. Possible
d commands include more egen functions and merge and reshape
d alternatives.
d 
d KW: levels
d KW: collapse
d KW: contract
d KW: egen
d KW: sort
d KW: factor variables
d KW: Mata
d 
d Requires: Stata version 11.2
d 
d Distribution-Date: 20191118
d 
d Author: Sergio Correia, Board of Governors of the Federal Reserve System
d Support: email sergio.correia@@gmail.com
d 
f f/ftools.ado
f f/ftools.mata
f f/ftools.sthlp
f f/fcollapse.ado
f f/fcollapse.sthlp
f f/fegen.ado
f f/fegen_group.ado
f f/fegen.sthlp
f f/fisid.ado
f f/fisid.sthlp
f f/flevelsof.ado
f f/flevelsof.sthlp
f f/fmerge.ado
f f/fmerge.sthlp
f f/freshape.ado
f f/fsort.ado
f f/fsort.sthlp
f f/ftab.ado
f j/join.ado
f j/join.sthlp
f l/local_inlist.ado
f l/local_inlist.sthlp
f f/fcollapse_functions.mata
f f/fcollapse_main.mata
f f/ftools_type_aliases.mata
f f/ftools.mata
f f/ftools_common.mata
f f/ftools_hash1.mata
f f/ftools_main.mata
f f/ftools_experimental.mata
f f/ftools_plugin.mata
f f/ftools_type_aliases.mata
f m/ms_compile_mata.ado
f m/ms_expand_varlist.ado
f m/ms_fvstrip.ado
f m/ms_fvstrip.sthlp
f m/ms_fvunab.ado
f m/ms_get_version.ado
f m/ms_parse_absvars.ado
f m/ms_parse_varlist.ado
f m/ms_parse_vce.ado
f m/ms_add_comma.ado
e
S http://fmwww.bc.edu/repec/bocode/c
N coefplot.pkg
D 14 Jul 2022
U 7
d 'COEFPLOT': module to plot regression coefficients and other results
d 
d   coefplot plots results from estimation commands or Stata
d matrices. Results from multiple models or matrices can be
d combined in a single graph. The default behavior of coefplot is
d to draw markers for coefficients and horizontal spikes for
d confidence intervals. However, coefplot can also produce various
d other types of graphs.
d 
d KW: graphics
d KW: coefficients
d KW: estimation
d 
d Requires: Stata version 11
d 
d Distribution-Date: 20220421
d 
d Author: Ben Jann, University of Bern
d Support: email jann@@soz.unibe.ch
d 
f c/coefplot.ado
f c/coefplot.sthlp
e
S http://fmwww.bc.edu/repec/bocode/p
N parmest.pkg
D 14 Jul 2022
U 8
d 'PARMEST': module to create new data set with one observation per parameter of most recent model
d 
d   The parmest package has 4 modules: parmest, parmby,
d parmcip and metaparm. parmest creates an output dataset, with 1
d observation per parameter of the most recent estimation results,
d and variables corresponding to parameter names, estimates,
d standard errors, z- or t-test statistics, P-values, confidence
d limits and other parameter attributes. parmby is a quasi-byable
d extension to parmest, which calls an estimation command, and
d creates a new dataset, with 1 observation per parameter if the
d by() option is unspecified, or 1 observation per parameter per
d by-group if the by() option is specified. parmcip inputs
d variables containing estimates, standard errors and (optionally)
d degrees of freedom, and computes new variables containing
d confidence intervals and P-values. metaparm inputs a parmest-type
d dataset with 1 observation for each of a set of
d independently-estimated parameters, and outputs a dataset with 1
d observation for each of a set of linear combinations of these
d parameters, with confidence intervals and P-values, as for a
d meta-analysis. The output datasets created by parmest, parmby or
d metaparm  may be listed to the Stata log and/or saved to a file
d and/or retained in memory (overwriting any pre-existing dataset).
d The confidence intervals, P-values and other parameter attributes
d in the dataset may be listed and/or plotted and/or tabulated.
d 
d 
d Requires: Stata version 16.0
d 
d Distribution-Date: 20220711
d 
d Author: Roger Newson, Imperial College London
d Support: email r.newson@@imperial.ac.uk
d 
f p/parmest.ado
f p/parmest.sthlp
f m/metaparm_content_opts.sthlp
f m/metaparm_outdest_opts.sthlp
f m/metaparm_resultssets.sthlp
f m/metaparm.ado
f m/metaparm.sthlp
f p/parmby_only_opts.sthlp
f p/parmby.ado
f p/parmby.sthlp
f p/parmcip_opts.sthlp
f p/parmcip.ado
f p/parmcip.sthlp
f p/parmest_ci_opts.sthlp
f p/parmest_outdest_opts.sthlp
f p/parmest_resultssets.sthlp
f p/parmest_varadd_opts.sthlp
f p/parmest_varmod_opts.sthlp
e
S http://fmwww.bc.edu/repec/bocode/d
N distplot.pkg
D 15 Jul 2022
U 9
d 'DISTPLOT': module to generate distribution function plot
d 
d distplot produces a plot of cumulative distribution function(s).
d This shows the proportion (or if desired the frequency) of values
d  less than or equal to each value. With the reverse option,
d distplot produces a plot of the complementary function.  This
d version is for Stata 8 or later.  Also included in this package
d are distplot7 files, a clone of the  last version of this program
d written for Stata 7.
d 
d KW: graphics
d KW: frequency distributions
d KW: cumulative distribution function
d 
d Requires: Stata version 8.0 (7.0 for distplot7)
d 
d 
d Author: Nicholas J. Cox, University of Durham
d Support: email N.J.Cox@@durham.ac.uk
d 
d Distribution-Date: 20170916
d 
f d/distplot.ado
f d/distplot.sthlp
f d/distplot7.ado
f d/distplot7.hlp
e
