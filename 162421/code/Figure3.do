

*
* FIGURE 3: Dynamic DiD
* 

* PROGRAMS

* store estimates
capture program drop store_estimate
program store_estimate 
    args treatment name

    parmest, fast
    drop if parm!="`treatment'_1950" & parm!="`treatment'_1940" & parm!="`treatment'_1960" & parm!="`treatment'_1970"
    rename estimate b`name'_
    rename min95 l`name'ci_
    rename max95 u`name'ci_

    g year = substr(parm,-4,.)
    sort year
    destring year, replace
    
    * Add 0 coefficient for omitted year 
    expand 2 in 1
    replace year = 1940 in 1
    replace b`name'_ = 0 in 1
    replace u`name'ci_ = 0 in 1
    replace l`name'ci_ = 0 in 1
    keep b`name'_ u`name'ci_ l`name'ci_ year
    
    replace year=year-1
    g id=1
    sort year
end

/*********************************************************/
** Treatment group
/*********************************************************/

eststo clear
use "${temp_path}/F3", clear

* keep treated occupations
keep if treated_occupation == 1

* DiD estimate
reghdfe Pct_A99_PTW   amenities year_FE* dummy_occ* station_1950 station_1960 station_1970  [aw=CZpeople] , absorb( czone )  cluster(czone)

* store estimates
store_estimate station
tempfile temp_file
save `temp_file', replace


/*********************************************************/
** Frozen stations
/*********************************************************/

use  "${temp_path}/F3", clear

* keep treated occupations & frozen TV launch 
keep if treated_occupation == 1 & count_stations==0

* DiD estimate
reghdfe Pct_A99_PTW   frozen_1950 frozen_1960 frozen_1970 amenities dummy_occ* year_FE* [aw=CZpeople] , absorb( czone )  cluster(czone)
* store estimates
store_estimate frozen Fro

* merge with previous results
merge 1:1 id year using `temp_file'
drop _m
save `temp_file', replace

/*********************************************************/
** Placebo Occupations
/*********************************************************/

use  "${temp_path}/F3", clear

* keep placebo occupations
keep if treated_occupation==0

* DiD estimate
reghdfe Pct_A99_PTW   amenities year_FE* dummy_occ* station_1950 station_1960 station_1970  [aw=CZpeople] , absorb( czone )  cluster(czone)

* store estimates
store_estimate station Placebo

* merge with previous results
merge 1:1 id year using `temp_file'

/*********************************************************/
* Plot DiD Results
/*********************************************************/


* Style commands
graph set window fontface "Times New Roman"
local color "49 130 189"
local ci_color `"222 235 247"'
label var b_ "Entertainer in top 1% (growth in %)"
label var bFro "Entertainer in top 1% (growth in %)"
label var bPlacebo "High Pay Profession in top 1% (growth in %)"

* Figure 3
twoway (rarea uci_ lci_ year, color("222 235 247%60"))  (connected  b_ year,  mlcolor("`color'") mfcolor("`color'") lcolor("`color'")) (rarea uFroci lFroci year, color("red%10")) ///
    (connected  bFro year, mlcolor("red%50") mfcolor("red%50") msymbol(D) lcolor("red%50") lpattern(dash)) ///
    (rarea uPlaceboci lPlaceboci year, color("orange%10") ) (connected  bPlacebo year, mlcolor("orange%50") msymbol(T) mfcolor("orange%50") lcolor("orange%50") lpattern(dot)) ///
    , legend(position(6) row(1)) yscale(range(-1 7)) ylabel(#6) xlabel(1939 1949 1959 1969) legend(order(2  4 6) label(2 "TV Station") label( 4 "Frozen Station") label( 6 "Placebo Occupations")) ///
    xline(1941 1956.92, lpattern(dot)) text(7 1942 "TV" ) text(7 1960 "Videotape" ) name(g2, replace) ytitle("{&Delta} Share Entertainers in Top US Wage Percentile (in ptp)", size(medsmall)) graphregion(color(white))  yline(0, lpattern(dot))

graph export  "${output_path}/Figure3.pdf", replace

