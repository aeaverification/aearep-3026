* Table 1: Employment effects


eststo clear
use "${temp_path}/T1", clear


*****************************
* Panel A: 1940-1970 sample
*****************************

* Baseline: DiD estimate of TV signal on entertainer employment
eststo: reghdfe asin_worker ITM_dummy if year>1930   , cluster(czone) absorb( czone year#occ195) 
estadd local clusterID `e(N_clust)'
sum year if e(sample)==1
estadd local sample "1940-1970"

* Baseline + demographic controls
* Note employment reg doesn't control for med_income b/c data is missing for < 1940 and hence unavailable for panel B
eststo: reghdfe asin_worker  med_age share_male   pop_density c.year#urban_place share_NW   ITM_dummy   if year>1930   , cluster(czone) absorb( czone year#occ195)
estadd local demog "{$\checkmark$}"
estadd local clusterID `e(N_clust)'
sum year if e(sample)==1
estadd local sample "{1940-1970}"

** Baseline + CZ trends
eststo: reghdfe asin_worker  ITM_dummy   if year>1930   , cluster(czone) absorb( czone year#occ195 c.year#czone)
estadd local CZ "{$\checkmark$}"
sum year if e(sample)==1
estadd local sample "{1940-1970}"
estadd local clusterID `e(N_clust)'


*****************************
* Panel B: Placebo stations
*****************************

* Baseline: for placebo TV signal
eststo: reghdfe asin_worker  TVfrozen  if year>1930  , cluster(czone) absorb( czone year#occ195)
sum year if e(sample)==1
estadd local sample "{1940-1970}"
estadd local clusterID `e(N_clust)'

* Baseline + demographic controls 
* Note employment reg doesn't control for med_income b/c data is missing for < 1940 and hence unavailable for panel B
eststo: reghdfe asin_worker  med_age share_male  pop_density  c.year#urban_place share_NW   TVfrozen     if year>1930, cluster(czone) absorb( czone year#occ195)
estadd local demog "{$\checkmark$}"
estadd local clusterID `e(N_clust)'
sum year if e(sample)==1
estadd local sample "{1940-1970}"

** Baseline + CZ trends
eststo: reghdfe asin_worker  TVfrozen    if year>1930  , cluster(czone) absorb( czone year#occ195 c.year#czone)
estadd local CZ "{$\checkmark$}"
sum year if e(sample)==1
estadd local sample "{1940-1970}"
estadd local clusterID `e(N_clust)'


*****************************
* Panel C: 1930-1970 sample
*****************************

* Baseline + extended sample
eststo: reghdfe asin_worker ITM_dummy    , cluster(czone) absorb( czone year#occ195) 
estadd local clusterID `e(N_clust)'
sum year if e(sample)==1
estadd local sample "1930-1970"

* Baseline + extended sample + demographic controls
* Note employment reg doesn't control for med_income b/c data is missing for < 1940 and hence unavailable for panel B
eststo: reghdfe asin_worker  med_age share_male  pop_density  c.year#urban_place share_NW   ITM_dummy      , cluster(czone) absorb( czone year#occ195)
estadd local demog "{$\checkmark$}"
estadd local clusterID `e(N_clust)'
sum year if e(sample)==1
estadd local sample "{1930-1970}"

* Baseline + extended sample + CZ trends
eststo: reghdfe asin_worker  ITM_dummy       , cluster(czone) absorb( czone year#occ195 c.year#czone)
estadd local CZ "{$\checkmark$}"
sum year if e(sample)==1
estadd local sample "{1930-1970}"
estadd local clusterID `e(N_clust)'


* Baseline + extended sample + treatment lead
eststo: reghdfe asin_worker ITM_dummy lead_ITM    , cluster(czone) absorb(czone year##occ)
estadd local clusterID `e(N_clust)'
sum year if e(sample)==1
estadd local sample "1930-1970"



* write esimates to csv file
esttab using "${output_path}/Table1", label replace keep( TVfrozen *TM_dumm* lead_ITM ) varwidth(20)    interactio(" X \\")  se  scalars("demog demographics"  "CZ commuting zone trends" "sample sample" "clusterID number of cluster" ) csv

