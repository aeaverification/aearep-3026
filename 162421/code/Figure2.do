* Figure 2


* INPUT: F2_data, employment_F2


**************************
* PANEL A:
use "${temp_path}/F2A.dta", clear


twoway (kdensity linc if year==1940 [fw=int(wage_weight)], lcolor("49 130 189") range(3.5 9.5) bw(0.4))|| ///
	(kdensity linc if year==1970 [fw=int(wage_weight)], lcolor("158 202 225") bw(0.4) lpattern(dash)  range(3.5 9.5) ) ///
	 , legend(position(6)) xlabel(4 "`=int(exp(4))'" 6 "`=int(exp(6))'" 8 "`=int(exp(8))'" 10 "`=int(exp(10))'") ///
	 legend(lab(1 "1940") lab(2 "1970")) xtitle("real wage (1950 $, log scale)") ytitle("Density", height(5)) ylab(, nogrid)

graph export "${output_path}/Figure2A.pdf", replace

**************************
* PANEL B:
use "${temp_path}/F2B.dta", clear


twoway (line total_E year, lpattern(dash) lcolor("158 202 225")) || (line total_PE year , lpattern(solid) lcolor("49 130 189" )), ///
	legend(position(6)) ytitle("Employment per Capita") ylab(, nogrid)

graph export  "${output_path}/Figure2B.pdf", replace
