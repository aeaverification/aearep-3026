

/*********************************************************
*************        Table 2   ********************
*********************************************************/
est clear
use "${temp_path}/T2", clear

*****************************
** PANEL A: DiD of TV on migration
*****************************

* Baseline: DiD estimate of TV signal on entertainer migration
eststo: reghdfe share_migrated station_1950 amenities [pw=CZpeople], absorb( czone year#occ195)  cluster(czone)
estadd local clusterID `e(N_clust)'

* Baseline + demographic controls
eststo: reghdfe share_migrated med_age med_inc share_male pop_density c.year#urban_place share_NW   station_1950 amenities [pw=CZpeople], absorb(czone year#occ195)  cluster(czone)
estadd local demog "{$\checkmark$}"
estadd local clusterID `e(N_clust)'

** Baseline + CZ trends
eststo: reghdfe share_migrated  station_1950 amenities [pw=CZpeople], absorb( czone##c.year year#occ195)  cluster(czone)
estadd local CZ "{$\checkmark$}"
estadd local clusterID `e(N_clust)'

*****************************
** PANEL B: DiD of TV on Top Incomes (excluding neighbour CZs to TV launches)
*****************************

* Drop CZs adjacent to TV launch CZs
drop if neighbour!=0 & count_stations==0

* Baseline: DiD estimate of TV signal on entertainer migration
eststo: reghdfe Pct_A99_PTW   station_1950   amenities [pw=CZpeople] , absorb( czone year#occ195)  cluster(czone)
estadd local clusterID `e(N_clust)'

* Baseline + demographic controls
eststo: reghdfe Pct_A99_PTW  med_age med_inc share_male pop_density c.year#urban_place share_NW  station_1950   amenities [pw=CZpeople] , absorb( czone year#occ195)  cluster(czone)
estadd local demog "{$\checkmark$}"
estadd local clusterID `e(N_clust)'

** Baseline + CZ trends
eststo: reghdfe Pct_A99_PTW   station_1950   amenities [pw=CZpeople] , absorb( czone##c.year year#occ195)    cluster(czone)
estadd local CZ "{$\checkmark$}"
estadd local clusterID `e(N_clust)'


* Save results in Table2.csv
esttab using "${output_path}/Table2.csv", label replace keep( station_1950  )   rename(1.TVinCZ TVinCZ)      sfmt(a2)  se  mgroups(`"{migration}"' `"{Top Earners}"', pattern(1 0 0 1) ) scalars("demog demographics"  "CZ commuting zone trends" "clusterID number of cluster") csv
