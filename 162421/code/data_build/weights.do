* hamonise weights
**
** weights
**

** 1970 combines two represenatitive samples. Half the weight variable to make combined sample representative.
replace perwt=perwt/2 if year==1970

* Define wage weight variable 
g wage_weight = perwt if year!=1950

* In 1950 only sample line individuals are asked about wages
* In Census 20% are asked about wages. IPUMS mainly samples individuals on the sample line (other household members are also included)
cap replace wage_weight = weight if year==1950

