
****************************************************************************
** Figure B1
****************************************************************************

* 1940 wage data
use "${temp_path}/IPUMS1940_raw", clear
replace incwage = . if empstat != 1 
replace incwage = . if incwage == 999998 

****************************************************************************
** Mincer regression
****************************************************************************

* define regressors
g lwage = ln(incwage)
drop if lwage==. 

g age2 = age* age
g age3 = age * age *age

* mincer regression
eststo: reg lwage i.educ age age2 age3 i.sex  if educ != 99
* store coefficients
local educ_0_1 = _b[0b.educ]
local educ_1_1 = _b[1.educ]
local educ_2_1 = _b[2.educ]
local educ_3_1 = _b[3.educ]
local educ_4_1 = _b[4.educ]
local educ_5_1 = _b[5.educ]
local educ_6_1 = _b[6.educ]
local educ_7_1 = _b[7.educ]
local educ_8_1 = _b[8.educ]
local educ_9_1 = _b[9.educ]
local educ_10_1 =_b[10.educ]
local educ_11_1 =_b[11.educ]
local age_1 = _b[age]
local age2_1 = _b[age2]
local age3_1 = _b[age3]
local female_1 = _b[2.sex]
local const_1 = _b[_cons]


****************************************************************************
** Disrtibution of residualised earnings
****************************************************************************

* residualised wages
predict wage_resid, residual

* compute percentiles of residualised earnings distribution
pctile pct_wage = wage_resid, nquantiles(100)
drop if pct_wage == .
keep pct_wage 

* keep all percentiles in a local variable
levelsof pct_wage, local(centile_wage)


**********************************************************
* Data on TV Stars
**********************************************************

import excel using "${orig}/Top artist.xlsx", clear sheet("1949 radio annual") firstr
* for inomce that is top coded, use top code (some people report 5000+ income)
replace income = subinstr(income, "+", "",.)
destring income wks hrs, replace
g lwage = ln(income)

* sample restrictions
drop if income==.
drop if emp!="1"

* clean data transcription
* gender
replace gender = "F" if gender == "female"
replace gender = "M" if gender == "male"
encode gender, g(female)
* education 
g ed_bin11 = 	(schooling== "college, 5th or subsequent")
g ed_bin10 = 	(schooling== "college, 4th year")
g ed_bin8 = 	(schooling== "college, 2nd year")
g ed_bin7 = 	(schooling== "college, 1st year")
g ed_bin6 = 	(schooling== "high school, 4th year")
g ed_bin4 = 	(schooling== "high school, 2nd year")


**********************************************************
* Earnings Rank in 1939 of later TV Stars
**********************************************************

* residualise wage
g wage_hat = (`const_1' + age*`age_1' + age*age*`age2_1' + age*age*age*`age3_1' + (2-female)* `female_1' + ed_bin4*`educ_4_1' + ed_bin6*`educ_6_1' + ed_bin7*`educ_7_1' + ed_bin8*`educ_8_1' + ed_bin10*`educ_10_1' + ed_bin11*`educ_11_1')
g resid_wage = lwage - wage_hat


* rank in the wage distribution
g wage_percentile=.
local LB = -100
local i=1
foreach thresh of local centile_wage {
	local UB = `thresh'
	replace wage_percentile = `i' if resid_wage>=`LB' & resid_wage<`UB'
	local LB = `thresh'
	local i=`i'+1
}
* account for top code
replace wage_percentile = 100 if resid_wage>=`LB' & resid_wage!=.
replace wage_percentile=99 if income>=5000
* Actor ID
g actor_ID =_n
* save data
keep wage_percentile resid_wage actor_ID
save "${temp_path}/rank_mobility", replace
