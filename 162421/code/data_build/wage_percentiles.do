
*******************************************************************************
**** Read in 1920 IPUMS file
*******************************************************************************


  cd "$do_path"
  do "${do_path}/IPUMS/usa_1920.do"
  save "${temp_path}/IPUMS1920_raw", replace

  drop if age<16

   
  * select micro data on entertainment occupations
  do "${do_path}/data_build/Census_micro.do" 1920
  save "${temp_path}/IPUMS1920_ent", replace

*******************************************************************************
**** Read in 1930 IPUMS file
*******************************************************************************


  cd "$do_path"
  do "${do_path}/IPUMS/usa_1930.do"
  save "${temp_path}/IPUMS1930_raw", replace

  drop if age<16


  * select micro data on entertainment occupations
  do "${do_path}/data_build/Census_micro.do" 1930
  save "${temp_path}/IPUMS1930_ent", replace

*******************************************************************************
**** Read in 1940 IPUMS file
*******************************************************************************


  cd "$do_path"
  do "${do_path}/IPUMS/usa_1940.do"
  save "${temp_path}/IPUMS1940_raw", replace

  drop if age<16
  
  preserve
  g nominal_wage = incwage
  replace nominal_wage=. if incwage==999999 |  incwage==999998
  replace nominal_wage=. if incwage==0
  drop if nominal_wage==.

  
  gquantiles wage_pctile = nominal_wage [pw=perwt], pctile genp(pct) nq(100)
  *pctile  wage_pctile = nominal_wage [pw=perwt], genp(pct) nq(100)
  keep if _n<100
  save "${temp_path}/percentile1940", replace
  restore

  * select micro data on entertainment occupations
  do "${do_path}/data_build/Census_micro.do" 1940
  save "${temp_path}/IPUMS1940_ent", replace

*******************************************************************************
**** Read in 1950 IPUMS file
*******************************************************************************

  cd "$do_path"
  do "${do_path}/IPUMS/usa_1950.do"
  save "${temp_path}/IPUMS1950_raw", replace

  drop if age<16


  preserve
  g nominal_wage = incwage
  replace nominal_wage=. if incwage==999999 |  incwage==999998
  replace nominal_wage=. if incwage==0
  drop if nominal_wage==.

  gquantiles wage_pctile = nominal_wage [pw=perwt], pctile genp(pct) nq(100)
  keep if _n<100
  save "${temp_path}/percentile1950", replace
  restore

  * select micro data on entertainment occupations
  do "${do_path}/data_build/Census_micro.do" 1950
  save "${temp_path}/IPUMS1950_ent", replace

*******************************************************************************
**** Read in 1960 IPUMS file
*******************************************************************************


  cd "$do_path"
  do "${do_path}/IPUMS/usa_1960.do"
  save "${temp_path}/IPUMS1960_raw", replace

  drop if age<16


  preserve
  g nominal_wage = incwage
  replace nominal_wage=. if incwage==999999 |  incwage==999998
  replace nominal_wage=. if incwage==0
  drop if nominal_wage==.
  gquantiles wage_pctile = nominal_wage [pw=perwt], pctile genp(pct) nq(100)
  keep if _n<100
  save "${temp_path}/percentile1960", replace
  restore

    * select micro data on entertainment occupations
    do "${do_path}/data_build/Census_micro.do" 1960
  save "${temp_path}/IPUMS1960_ent", replace


*******************************************************************************
**** Read in 1970 IPUMS file
*******************************************************************************


  cd "$do_path"
  do "${do_path}/IPUMS/usa_1970.do"
  save "${temp_path}/IPUMS1970_raw", replace

  drop if age<16


  preserve
  g nominal_wage = incwage
  replace nominal_wage=. if incwage==999999 |  incwage==999998
  replace nominal_wage=. if incwage==0
  drop if nominal_wage==.

  gquantiles wage_pctile = nominal_wage [pw=perwt], pctile genp(pct) nq(100)
  keep if _n<100
  save "${temp_path}/percentile1970", replace
  restore

  * select micro data on entertainment occupations
  do "${do_path}/data_build/Census_micro.do" 1970
  save "${temp_path}/IPUMS1970_ent", replace

*******************************************************************************
**** Combine micro data on entertainer
*******************************************************************************
clear
foreach year of numlist 1920(10)1970 {
  append using "${temp_path}/IPUMS`year'_ent"
}
save "${temp_path}/Census_micro", replace
