args year zone

* This file combines observations in the same CZ from different states and computes CZ level demographics

* List of state IDs
if `year'!=1970 {
	import delimited "$orig/ID lists/state_list.txt", clear
	levelsof v1, local(state)
}
if `year'==1970 {
	import delimited "$orig/ID lists/state_list70.txt", clear
	levelsof v1, local(state)
}


* Combine CZ observations from different states
display "`state'"

clear
set obs 1
gen x=.
* append CZ data from all states
foreach st of local state {
	cap noisily append using "${temp_path}/micro_med`year'_`st'_`zone'"
}
drop in 1

* harmonise weights
do "${do_path}/data_build/weights.do"
 

if `year'>1930 {
	rename  incwage med_inc

}

* Compute CZ aggregate demographics
 
if  `year'==1950 {
	preserve
	collapse   (median)  med_inc [pw=wage_weight], by(czone)
	save "${temp_path}/wage`year'_`zone'", replace
	restore

	preserve

	collapse (sum)  CZpeople=people CZworker=workerTOT (mean)  share_male share_NW (median) year med_age   [pw=perwt], by(czone)
	merge 1:1 czone using "${temp_path}/wage`year'_`zone'"
	drop _merge

}
if `year'<=1930 {

	collapse (sum) CZpeople=people (mean) share_male share_NW (median) year med_age [pw=perwt], by(czone)

}

if `year'==1960 | `year'==1970 {
	collapse  (sum)  CZpeople=people CZworker=workerTOT  (mean)  share_NW share_male (median) year med_age med_inc   [pw=perwt], by(czone)
	label var med_inc "median income"

}

if `year'==1940  {
	collapse (sum)  CZpeople=people CZworker=workerTOT  (mean) urban_share share_male share_NW (median) year med_age med_inc   [pw=perwt], by(czone)

	label var med_inc "median income"

}


save "${temp_path}/czone_`zone'_`year'", replace

foreach st of local state {
		cap noisily erase "${temp_path}/micro_med`year'_`st'_`zone'.dta"
		cap noisily erase "${temp_path}/wage`year'_`zone'.dta"
}

