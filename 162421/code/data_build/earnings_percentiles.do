

** cpi values
local cpi50_1940 =11.986/7
local cpi50_1950 =1
local cpi50_1960 =5.725/7
local cpi50_1970 =4.540/7

* income percentiles (real $)
forvalue year = 1940(10)1970 {
	use "${temp_path}/percentile`year'", clear
	global pct50_`year' = round(wage_pctile[50]   * `cpi50_`year'')
	global pct25_`year' =  round(wage_pctile[25]  * `cpi50_`year'')
	global pct75_`year'  = round(wage_pctile[75] * `cpi50_`year'')
	global pct90_`year'  = round(wage_pctile[90] * `cpi50_`year'')
	global pct95_`year'  = round(wage_pctile[95] * `cpi50_`year'')
	global pct99_`year'  = round(wage_pctile[99] * `cpi50_`year'')

}

