* Estimate the shape of the local top wage distribution (assuming Pareto distributed incomes)
use incwage year czone pernum wage_weight occ1950 ind1950 ///
using "${temp_path}/census_CZ", clear


*drop years without wage data
drop if year < 1940


* generate real income
do "${do_path}/data_build/deflate.do" incwage

* keep treated occupations
do "${do_path}/data_build/occupation_groups.do"
keep if group == 1

* PARETO REGULARITY: Rank in distribution related to income level

*
* Rank of worker in CZ
* 
drop if incwage==. 

* drop top coded cases
bys year: egen max_inc = max(incwage)
g TC =  ( int(max_inc)==int(incwage) ) if year>1940
replace TC =1 if year==1940 &  int(incwage)>8561
drop if TC==1

*In 1940 we have data on everyone (no imputation needed). 

preserve
keep if year ==1940
g wage_imputed = incwage
g sample_frequency = 1
bys czone: egen population=sum(sample_frequency)
tempfile w1940
save `w1940', replace 
restore

* For years >1940 we need to impute wages for unobserved (not sampled) individuals
keep if year>1940
* rank in wage distribution in sample
sort year czone incwage
by year czone: egen wage_rank = rank(incwage) if incwage != . ,  field
* for tied wages order randomly
bys year czone wage_rank: g wage_rank_running = wage_rank-1 +_n

* rank in population distribution 
* this is different from the sample rank as each sample individual represents a group of the population.
sort year czone wage_rank_running
by year czone: g pop_rank = sum(wage_weight)
* within represented group place person in the midle
g weight_halve = wage_weight/2 
* top ranked person
g pop_rank2 = pop_rank/2  if wage_rank_running==1
* lower ranked persons
by year czone: g rank_distance = weight_halve[_n-1] +weight_halve
by year czone: replace pop_rank2 = pop_rank2[_n-1]+rank_distance  if wage_rank_running!=1
drop pop_rank
rename pop_rank2 pop_rank


* Number of workers in CZ
* based on population rank of last person in sample
bys year czone: egen emp_count = max(pop_rank)
* Assumed that people rank in the middle of the group they represent. Hence need to add half of group to rank of last person in sample to get the population total 
replace emp_count = emp_count + weight_halve if emp_count == pop_rank
bys year czone: egen population = max(emp_count)
drop emp_count

*
* Estimate Pareto coefficient (Kuznet's method)
*


* infer wages between two sample observations by calculating Pareto alpha for the bin

* Pareto parameters

sort year czone pop_rank
* change in rank
by year czone: g rank_change = log(pop_rank/pop_rank[_n-1])
* change in income
by year czone: g inc_change = log(incwage[_n-1]/incwage)
* Kuznet's method for estimating pareto parameters:  pareto parameter = devide ratio of income and ratio of percentile of two individuals
* take ratio of rank rather than percentile as total number of worker cancels
by year czone: g alpha_bracket = rank_change/inc_change
drop *_change

* CZ pareto coefficient: Currently, we have a pareto coefficient for every two adjacent individuals. Now we will average this for the CZ
by year czone : egen alpha_bracket_avg = mean(alpha_bracket) 

* Use Pareto parameter to impute wage distribution:
* generates a dataset with full population in the CZ and an imputed wage for each individual 

* expand sample to full population
* expected number of individuals ranked between two sampled individuals is the sum of half the weight of each sampled individual
bys year czone: g sample_frequency = 1/int(rank_distance)
replace sample_frequency=1 if sample_frequency==.
replace rank_distance = pop_rank  if wage_rank_running==1
expand int(rank_distance), g(duplics)

* infer wage for people between two observations in the sample
bys year czone  pop_rank: g dup_rank = _n
by year czone : g wage_imputed = incwage * (pop_rank/(pop_rank-dup_rank+1))^(1/alpha_bracket_avg)
* between 2 sample wage ties, assume everyone has same wage
replace wage_imputed = incwage if wage_imputed  == .

* add 1940 data
append using `w1940'
keep czone year   population  sample_frequency wage_imputed  

save "${temp_path}/earning_extrapolated", replace
