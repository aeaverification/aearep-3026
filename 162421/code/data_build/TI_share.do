
use "${temp_path}/earning_extrapolated", clear

* Calculate top income shares

* RANKS
gsort year czone -wage_imputed
bys year czone: g PL_rank = _n
bys year czone: egen tot_pop = max(PL_rank)

* sample restriction places>=20 entertainers
drop if tot_pop<20

* INCOMES
* calculate sum of wage in tail for different tail cut-offs:
foreach value in 2 20 {
	by year czone: egen inc_`value' = sum(wage_imputed/100000) if PL_rank <=`value'
	replace inc_`value' = . if tot_pop<`value'
}

*PL coefficient
g PL = ln(2/20)/ln(inc_2/inc_20)


collapse (sum) cell_size=sample_frequency (max)  tot_pop PL population , by(year czone)
label var tot_pop "cell population"
label var population "local entertainer count"

* generate top income shares (based on Pareto regularity)
g S_1 = 0.01^(1/PL)*100
g S_10 = 0.1^(1/PL)*100
g S_01 = 0.001^(1/PL)*100

* log top income shares
g l_T1 = ln(S_1)
g l_T01 = ln(S_01)
g l_T10 = ln(S_10)

* label
label var l_T1 "Top 1% Income Share (log)"
label var l_T01 "Top 0.1% Income Share (log)"
label var l_T10 "Top 10 % Income Share (log)"
label var S_1 "Top 1% Income Share "
label var S_01 "Top 0.1% Income Share"
label var S_10 "Top 10% Income Share"

save "${temp_path}/TI_share", replace

