* select micro data for entertainer groups
local year `1'

  do "${do_path}/data_build/occupation_groups.do"
  drop if group==.
  drop if qocc!=0 | qind!=0
  
  * age restriction
  drop if age<16

  * Mainland US restriction
  drop if stateicp==82 | stateicp==81

*harmonise migration variable
if `year'>1920{

	** 1950 difference: looks at migration in last year & for age >1. Other years look migration over 5 years and age >5
	if  `year'!=1950 & `year'>1930 {
		g migrated=1 if (migrate5>1 & migrate5<9 & empstat==1)
	}
	if  `year'==1950  {
		g migrated=1 if (migrate1>1 & migrate1<9  & empstat==1)
	}
	if `year' ==  1970 {
		** weights need to be multiplied, as only halve the sample is asked about this
		replace migrated = migrated*2
	}
}



order year  state* group
compress