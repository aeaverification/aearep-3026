*
* create real variables
* deflates nominal values based on CPI data from IPUMS
* base period is 1950

** cpi values
local cpi50_1940 =11.986/7
local cpi50_1950 =1
local cpi50_1960 =5.725/7
local cpi50_1970 =4.540/7

levelsof year, local(year_list)

foreach year of local year_list {
	replace `1' = `1' * `cpi50_`year'' if year ==`year'
}