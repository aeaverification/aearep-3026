* This file combines data on all CZs and years
** Append all files
clear

* list of CZs
import delimited "${orig}/ID lists/czones.txt", clear
levelsof v1, local(czones)

local iter=1

* append
foreach yr of numlist 1920(10)1970 {
	foreach zone of local czones {
		if `iter'==1 {
			use "${temp_path}/czone_`zone'_`yr'", clear

		}
		if `iter'!=1 {
			append using "${temp_path}/czone_`zone'_`yr'"
		}
	local iter=`iter'+1
	}
}

* compute population density
* add area information
merge m:1 czone using "${orig}/CZ_area", keep(3)
drop _merge
g pop_density = CZpeople/cz_area*100000
label var pop_density "population density"

* urban dummy
** since two CZ not populated in 1940 - urban dummy can't be defined for those...
bys czone: egen urban_prop=max(urban_share)
g urban_place = (urban_prop>0.5) if urban_prop!=.
drop urban_prop

*labels
label var CZpeople "people in CZ"
label var med_inc "median income"
label var med_age "median age"
label var share_NW "non white"
label var share_male "men"

* Demographics data for  1920-1970
save "${temp_path}/CZ_demographicsF2", replace
* Demographics data for  1930-1970
keep if year>1920
save "${temp_path}/CZ_demographics", replace

* clean up

foreach yr of numlist 1920(10)1970 {
	foreach zone of local czones {
			cap noisily erase "${temp_path}/czone_`zone'_`yr'.dta"
	}
}

