* Define treated and untreated occupations


* treated occupations: 
** Athlete, Actor,  dancers , Entertainer (n.e.c.), musician - not in teaching
local entertainer 	"occ1950==5 | occ1950==1 | (occ1950==31 & ind1950!=888 & ind1950!=0  & ind1950!=999  & ind1950!=998  & ind1950!=997  & ind1950!=.) |   occ1950==51 | (occ1950==57 & ind1950!=888   & ind1950!=0  & ind1950!=999  & ind1950!=998  & ind1950!=997  & ind1950!=.)"

* untreated leisure occupations
** Bartender, cooks, counter workers in industry in eating and drinking, waiters
local dine   "occ1950==750 | occ1950==754 | (occ1950==760 & ind1950==679) | occ1950==784"
**recreation worker, attendants recreation, sports isntructor, Artist & teacher outside teaching, usher in amusement, authors
local recreation "occ1950==77 | (occ1950==4 & ind1950!=888  & ind1950!=0  & ind1950!=999  & ind1950!=998  & ind1950!=997  & ind1950!=.) | occ1950==732 | occ1950==91 | occ1950==6"

* control occupations (high income)
** Engineers
local engineers 	"(occ1950>40 & occ1950<50)"
** Medical profession (dentist, physicians&surgeons, psychologist, pharmacist)
local medics 		"occ1950==32 | occ1950==75 | occ1950==82 | occ1950==73"
** Managers: Buyers and department heads, store; Buyers and shippers, farm products; Floormen and floor managers, store; Managers and superintendents, building; Purchasing agents and buyers (n.e.c.) ; Managers, officials, and proprietors (n.e.c.))
local managers 		"(occ1950>199 & occ1950<202) | (occ1950 == 205) | occ1950 == 230 | occ1950 == 280 | occ1950 == 290"
** Service professionals: accountant, stock and bond salesmen, creditmen, lawyer
local professionals "occ1950==0 |  occ1950==480 | occ1950==204 | occ1950==55"

local control		"`engineers' | `managers' | `medics' | `professionals' "


*********************************************************************************************
** occupation groups
*********************************************************************************************
	g group = 1 if `entertainer'
	replace group = 2  if `recreation'
	replace group = 3  if `dine'
	replace group = 5  if `professionals'
	replace group = 6  if `medics'
	replace group = 7  if `engineers'
	replace group = 8  if `managers'

g treated_occupation = 1 if `entertainer'
replace treated_occupation = 0 if `control'