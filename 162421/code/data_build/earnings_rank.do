*******************************************
* Earnings Rank
*******************************************



* load earnings percentiles
do "${do_path}/data_build/earnings_percentiles.do"

*******************************************
* * Compute worker rank in earnings distribution 
*******************************************

* CZ level aggregate: share of local employment in national rank percentile
 
*******************************************
* Numerator: employment count in percentile in CZ-occ-year cell
*******************************************

use incwage year czone pernum wage_weight occ1950 stateicp ///
using  "${temp_path}/census_CZ", clear

* keep only year with earning data
keep if year>=1940
drop if incwage==.


* generate real wage
do "${do_path}/data_build/deflate.do" incwage


* Compute rank in distribution
g Pct_B25 = .
g Pct_2550 = .
g Pct_5075 = .
g Pct_7590 = .
g Pct_9095 = .
g Pct_9599 = .
g Pct_A99 = .

forvalue year = 1940(10)1970 {
	replace Pct_A99 = 1 if incwage >= ${pct99_`year'} & incwage!=.  & year == `year'
	replace Pct_9599 = 1 if incwage >= ${pct95_`year'} & incwage < ${pct99_`year'} & year==`year'
	replace Pct_9095 = 1 if incwage < ${pct95_`year'} & incwage >= ${pct90_`year'} & year==`year'
	replace Pct_7590 = 1 if incwage < ${pct90_`year'} & incwage >= ${pct75_`year'} & year==`year'
	replace Pct_5075 = 1 if incwage >= ${pct50_`year'} & incwage < ${pct75_`year'} & year==`year'
	replace Pct_2550 = 1 if incwage >= ${pct25_`year'} & incwage < ${pct50_`year'} & year==`year'
	replace Pct_B25 = 1 if  incwage < ${pct25_`year'} & year==`year'
	display "${pct99_`year'}"
}


* Aggregate at the CZ-year-occ level
collapse  (sum) Pct_* [pw=wage_weight] , by(occ1950 czone year)

* keep sample occ-cz-year cells
merge 1:1 czone year occ1950 using "${orig}/ID lists/sample_definition"
* wage sample years
keep if year>=1940

* employment counts are missing in cells w/o employment. Filling these 0s in here
drop _merge
foreach var of varlist Pct_* {
	replace `var' = 0 if `var' ==.
}

* save CZ-year-occ counts
tempfile percentile_rank
save `percentile_rank', replace

*******************************************
* Denominator: 
* employment in occupation-year cell in treatment areas
*******************************************
use czone occ1950 year worker using "${temp_path}/employment", clear

* restrict to years with wage data
keep if year>=1940

* merge treatment area identifier
merge m:1 czone year using "${orig}/TVStudio" , keepusing(count_stations)
drop _m 

* calculate employment in treated areas
g denominator = .
levelsof occ19 , local(all_occs)
forvalue year= 1940(10)1970 {
	foreach occ of local all_occs {
			cap sum  worker  if count_stations>0   & occ19==`occ' & year==`year'
			display _rc
			if _rc==0 {
				replace denominator = r(mean) if occ19==`occ' & year==`year'
			}	
	}
}

* Calculate employment shares

* Add numerator
merge 1:1 czone year occ using `percentile_rank' 
drop _merge

* Share
foreach var of varlist  Pct* {
	g `var'_PTW = `var'/denominator*100
	
}

label var Pct_B25_PTW "Below 25th pct"
label var Pct_2550_PTW "25th-50th pct"
label var Pct_5075_PTW "50th-75th pct"
label var Pct_7590_PTW "75th-90th pct"
label var Pct_9095_PTW "90th-95th pct"
label var Pct_9599_PTW "95th-99th pct"
label var Pct_A99_PTW "99th pct and above"




keep czone occ1950 year  treated_occupation group Pct*_PTW  Pct_A99

* keep occupations used in analysis
preserve
keep if treated_occupation!=.
* save
save  "${temp_path}/earnings_rank", replace
restore

* keep alternative (larger) sample 
save  "${temp_path}/earnings_rank_APX", replace


