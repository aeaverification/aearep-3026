* Prepare IPUMS data

local year `1'
local stateicp `2'

** load data
if `year'==1930 | `year'==1920 {
	local variables "*"
}

if `year'==1940 {
	local variables "year stateicp county *_pop labforce wkswork2 hrswork2 empstat  sex educ race incwage  slwt perwt age ind1950 occ1950 migrate* urban  q* qempstat"
}

if  `year'==1950 {
	local variables "year stateicp sea county *_pop labforce wkswork2 hrswork2 empstat  sex educ race incwage  slwt perwt age ind1950 occ1950 migrate* q*"
}
if `year'==1960{
	local variables "puma tv year statef stateic *_pop wkswork2 hhwt hrswork2 empstat samp datanum pernum  serial  labforce educ sex   race incwage  perwt age ind1950 occ1950 migrate* q*"
}

if `year'==1970{
	local variables "cntygp97 uhf tv year statef *_pop stateic wkswork2 hrswork2 empstat  serial datanum pernum hhwt  labforce educ sex  race incwage  perwt age ind1950 occ1950 migrate* q*"
}



use `variables' if stateicp==`stateicp' using "${temp_path}/IPUMS`year'_raw", clear




****************************************************************************
** Wages
****************************************************************************
**wages - deflate and set non response to missing (e.g.only sample line individuals report wages in 1950)
if `year'>1930 {
	replace incwage=. if incwage==999999 |  incwage==999998
	do "${do_path}/data_build/deflate.do" incwage
	replace incwage=. if incwage==0
}


****************************************************************************
** match individual to CZ
****************************************************************************
do "${do_path}/data_build/cz_match2.do" `year'




****************************************************************************
** CZ Demographics:
* - share non white, median age, sample size per CZ, median wage
****************************************************************************

g people=1
g observationTOT=1/perwt
g NW=(race>1)
g share_male=(sex==1)
if `year'>1930 {
	g workerTOT=(empstat==1)
}
** urban location
if `year'==1940 {
	g urban_share=1 if urban==2
	replace urban_share=0 if urban==1
}


** split data by czone-state and save
levelsof czone , local(zones)
foreach zone of local zones {
	preserve
	rename  age med_age
	rename  NW share_NW
	label var med_age "median age"
	label var share_NW "non white"
	label var share_male "men"
	drop if czone!=`zone'
	save "${temp_path}/micro_med`year'_`stateicp'_`zone'", replace
	restore

}

