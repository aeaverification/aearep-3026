* Data of top earnings in CZ-year

* load micro data
use incwage year czone pernum wage_weight occ1950 ind1950 ///
using  "${temp_path}/census_CZ", clear

* define treated & untreated occupations
do "${do_path}/data_build/occupation_groups.do"

* Keep core sample
keep if treated_occupation == 1 & year>1930 & incwage!=.

* generate real income
do "${do_path}/data_build/deflate.do" incwage

* Top wage percentile
egen  centile99 =  wpctile(incwage), by(year czone) weights(wage_weight) p(99)

* keep only year with earning data
label var centile99 "Entertainment pay 99th percentile"

* collapse at CZ-year level
g cell_size = 1
collapse (rawsum) cell_size (mean)  centile* , by(czone year)

* restrict to larger CZs
preserve
drop if cell_size<3
bys czone: egen panel_frequency = count(centile99)
drop if panel_frequency<2
save "${temp_path}/earningsTB1", replace
restore
*************************
* top code adjustments
*************************
g lcentile99 = ln(centile99)

* fixed multiple imputation
g Top_code = 0
replace Top_code = 1 if year==1940 & centile99>=8561 & centile99!=.
replace Top_code = 1 if year==1950 & centile99>=10000 & centile99!=.
replace Top_code = 1 if year==1960 & centile99>=20446 & centile99!=.
replace Top_code = 1 if year==1970 & centile99>=32428 & centile99!=.

g fixed_multiple = centile99
replace fixed_multiple = 8561*3/2 if year==1940 &  Top_code==1
replace fixed_multiple = 10000*3/2 if year==1950 & Top_code==1
replace fixed_multiple = 20446*3/2 if year==1960 & Top_code==1
replace fixed_multiple = 32428*3/2 if year==1970 & Top_code==1
g lfixed_multiple = ln(fixed_multiple)

* pareto imputation

* add pareto coefficients
merge 1:1 year czone using "${temp_path}/TI_share",  keepusing(cell_size tot_pop l_T1 l_T01 l_T10 PL S_01  S_10 S_1 PL)
rename PL pareto_coeficient
drop if pareto_coeficient==.

g pareto_inc = centile99
replace pareto_inc = 8561*pareto_coeficient if year==1940 &  Top_code==1
replace pareto_inc = 10000*pareto_coeficient if year==1950 & Top_code==1
replace pareto_inc = 20446*pareto_coeficient if year==1960 & Top_code==1
replace pareto_inc = 32428*pareto_coeficient if year==1970 & Top_code==1
g lpareto_inc = ln(pareto_inc)


* Relative top income shares
g rel_share_01_1 = S_01 / S_1
g rel_share_10_1 =  S_10 / S_1



save "${temp_path}/TopEarnings", replace