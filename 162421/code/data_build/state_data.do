


*********************************************************************************************************
**			station location at state level
*********************************************************************************************************
use "${orig}/TVStudio", clear
merge m:1 czone using "${orig}/CZ crosswalk/cw_czone_state 3.dta"
drop if _m==2
collapse  (sum) count_stations  amenities, by(statef year)
tempfile TVinSTATE
save `TVinSTATE', replace

*********************************************************************************************************
**			state level data
*********************************************************************************************************


* Census Data

* State population 
use "${temp_path}/CZ_demographics", clear
* crosswalk to state
merge m:1 czone using "${orig}/CZ crosswalk/cw_czone_state 3.dta"
** drop hawaii and alaska
drop if _m==2
* average population of state
collapse (mean) CZpeople, by(statefip)
tempfile StatePop
save     `StatePop', replace

* State FIPS ISCPR crosswalk
import excel "${orig}/ID lists/StateFIPSicsprAB.xls", first clear
keep STICPSR FIPS
rename STICPSR stateicp
rename FIPS statefip
tempfile xwalk_FIPS_ISCPR
save     `xwalk_FIPS_ISCPR', replace


* Top earning entertainers 
use incwage year czone pernum wage_weight occ1950 stateicp if ///
year>=1940 & (occ1950==1 | occ1950==5 | occ1950==31 | occ1950==51 | occ1950==57 )  & incwage!=. ///
using  "${temp_path}/census_CZ", clear

* generate real wage
do "${do_path}/data_build/deflate.do" incwage

* keep top earners
g Pct_A99 = .
forvalue year = 1940(10)1970 {
    replace Pct_A99 = 1 if incwage >= ${pct99_`year'} & incwage!=.  & year == `year'
}
keep if Pct_A99==1

* crosswalk to state
* use IPUMS state (if available)
merge m:1 stateicp using `xwalk_FIPS_ISCPR' , keep(3 1)
** no match for states that are unidentified & DC
rename _m IPUMS_state
rename statefip statefip_IPUMS

* fill in 0 counts for missing state-occupation-year cells
merge m:1 czone year occ1950 using "${orig}/ID lists/sample_definition"
keep if (occ1950==1 | occ1950==5 | occ1950==31 | occ1950==51 | occ1950==57 ) 
* wage sample years
keep if year>=1940
rename _m empty_cell
replace wage_weight = 0.1 if wage_weight==.
replace Pct_A99 = 0 if Pct_A99==.

*  if IPUMS does not identify state: infer from main state of czone
merge m:1 czone using "${orig}/CZ crosswalk/cw_czone_state 3.dta", keep(1 3)
replace statefip = statefip_IPUMS if IPUMS_state==3 

* sum number of top earners in each state
collapse (sum) Pct_A99 [pw=wage_weight] , by(statefip occ1950 year)

* COMBINE DATA

* append IRS data (1915)
append using "${orig}/IRS"
* combine top earner counts from IRS and Census
egen maxEarner = rowtotal(Pct_A99 EarnMax16_occ)

* add state population 
merge m:1 statef using `StatePop'
drop _m
* add TV information
merge m:1 statef year using `TVinSTATE'
drop _m
* fill in TV data for 1915
bys statef: egen helper_Station = mean(count_stations)
replace count_stations= helper_Station if year==1915
replace amenities= 0 if year==1915

save "${temp_path}/data_state", replace

