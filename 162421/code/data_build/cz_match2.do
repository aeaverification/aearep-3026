
****************************************************************************
** match individual to CZ
****************************************************************************
local year `1'
**SHAPE file based CZ match. Dorn files for the various years - matching across smalest regional unit
if `year'<1950{
	g ICPSR_cty=stateicp*10000+county
	rename slwt weight
	joinby ICPSR_cty using "${orig}/CZ crosswalk/cw_cty`year'_czone", unmatched(master)
	**drop Alaska and Hawaii
	drop if stateic==81 | stateic==82
	**yellowstone, sometimes empty bin
	drop if ICPSR_cty==630875 & _m==2
	drop if ICPSR_cty==641130   & _m==2
	drop if ICPSR_cty==680455 & _m==2
	** not in IPUMS
	display "`year'"
	tab ICPSR_cty if _m==2
	** not in TIGER/LINE
	tab ICPSR_cty if _m==1
	replace weight=afactor*weight



}
if `year'==1950{
	rename sea sea1950
	rename slwt weight
	joinby sea1950 using "${orig}/CZ crosswalk/cw_sea1950_czone.dta", unmatched(master)
	//no commuting zone found for SEA 42 & 357. Assgin manually
	replace czone=38300 if sea1950==42
	replace czone=38901 if sea1950==357
	replace afactor=1 if sea1950==42 | sea1950==357
	replace weight=afactor*weight
}
if `year'==1960{
	g puma60=statef*10000+puma
	**Will use PUMA 2000 to crosswalk. Only two IDs change from 60 puma to 2000
	replace puma60=422044 if puma60==422098
	replace puma60=482073 if puma60==482097
	joinby puma60 using "${orig}/CZ crosswalk/cw_puma1960_czone", unmatched(master)
}
if `year'==1970{
	rename cntygp97 ctygrp1970
	joinby ctygrp1970 using "${orig}/CZ crosswalk/cw_ctygrp1970_czone.dta", unmatched(both)
	//missing county group in Dorn's crosswalk: 505 and 506 - assign based on the czone of the comprising counties in 1990
	replace czone=20800 if ctygrp1970==505
	replace afactor=1 if ctygrp1970==505
	g num=_n
	expand 2 if ctygrp1970==506, g(dup)
	bys num: g sumDUP=_n
	replace czone=20800 if ctygrp1970==506 & sumDUP==1
	replace czone=20700 if ctygrp1970==506 & sumDUP==2
	replace afactor=0.6598 if ctygrp1970==506 & sumDUP==1
	replace afactor=0.3402 if ctygrp1970==506 & sumDUP==2
	drop dup sumDUP num
	drop if _m==2

}


//Dorn file includes sea1950 code of 28. No such statistical area exists in IPUMS data or code book.
** drop if not in IPUMS
drop if _m==2

//readjust weights given that a person could now appear in more than one commuting zone
	rename perwt perwt2
	g perwt=afactor*perwt2
