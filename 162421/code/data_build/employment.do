
* Employment count (CZ-occ-year cells)

use empstat labforce czone year occ1950 ind1950 perwt stateicp using "${temp_path}/census_CZ", clear


* dummy for employed person 
g worker =(empstat==1)
* dummy for employed person (consistent definition since 1920)
g worker1920 =(labforce==2)

* sum employment at CZ-year-occ level
collapse  (sum)    worker worker1920 [pw=perwt], by(year czone occ1950)


* create a balanced panel
merge 1:1 czone year occ  using "${orig}/ID lists/sample_definition"
drop _m

** missing data  if employment=0. Setting missings to 0
replace worker1920=0 if worker1920==.
replace worker=0 if worker==. & year>1920 & year!=. 
replace worker=. if year==1920


** Employment (inverse hyperbolic sine transformation)
g asin_worker = asinh(worker)



********************************
* entertainer occupations
********************************
preserve
keep if treated_occupation==1 & worker!=.
keep year occ1950 czone asin_worker
save "${temp_path}/employment_T1", replace
restore

********************************
* entertainer & control occupations
********************************
preserve

save "${temp_path}/employment", replace
restore

********************************
* entertainers & other leisure related professions
********************************
keep if treated_occupation==1 | treated_occupation==.
merge m:1 czone year using "${temp_path}/CZ_demographicsF2", keep(3) keepusing(CZpeople)

* entertainer per capita
g workerPC1920=worker1920/(CZpeople)*100000

* Total workers in performance entertaiment & other leisure occupation
bys czone year: g total_PE = sum(workerPC1920) if group==1
bys czone year: g total_E = sum(workerPC1920) if group>1
collapse (mean) total* [pw = CZpeople], by(year)
label var total_PE "Performance Entertainers"
label var total_E "Other Leisure Occupations"
save "${temp_path}/F2B", replace
