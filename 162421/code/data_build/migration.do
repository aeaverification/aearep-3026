* Migration from other CZ

use year czone occ1950 migrate5 migrate1 empstat perwt ///
using  "${temp_path}/census_CZ", clear

* baseline sample period
drop if year<1940


* dummy for employed person 
g worker =(empstat==1)

* harmonize migration variables across census vintages
g migrated=1 if (migrate5>1 & migrate5<9 & worker==1) & year !=1950
replace migrated=1 if (migrate1>1 & migrate1<9  & worker==1) & year==1950

* count migration
collapse (sum) migrated worker [pw=perwt], by(year czone occ1950)

* select sample
merge 1:1 czone year occ1950 using "${orig}/ID lists/sample_definition", keep(3 2)
* keep treated occupations (entertainers) 
keep if treated_occupation==1
* wage sample years
keep if year>=1940

* employment data is missing in cells w/o workers. Filling these 0s in here
drop _merge
foreach var of varlist migrated worker {
	replace `var' = 0 if `var' ==.
}


* share migrants
g migrant_share = migrated / (worker+1)

* harmonise migration across Census vintages 
* In 1950 asks about migration since t-1, other years since t-5. Harmonise by assuming stationary migration rates
g stationary_migration = migrant_share
replace stationary_migration = 1-(1-stationary_migration)^5 if year == 1950

* housekeeping
keep czone occ1950 year stationary_migration
rename stationary_migration share_migrated
sort occ1950 czone year

* save data
save "${temp_path}/migration", replace
