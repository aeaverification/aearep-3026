* Build data for analysis 


**************************************************************************************
**		Entertainer micro data
**************************************************************************************

* Saves raw census data and computes percentiles of the wage distribution 
do "${do_path}/data_build/wage_percentiles.do"
 
**************************************************************************************
**		CZ Demographics
**************************************************************************************

* IPUMS Data 1920 - 1960
local stateicp_ID "41 61 42 71 62 1 11 98 43 44 63 21 22 31 32 51 45 2 52 3 23 33 46 34 64 35 65 4 12 66 13 47 36 24 53 72 14 5 48 37 54 49 67 6 40 73 56 25 68"
foreach year of numlist 1920(10)1960 {
	foreach state of local  stateicp_ID {
	do "${do_path}/data_build/IPUMS_data.do" `year' `state'
	}
}
* IPUMS Data 1970
local stateicp_70ID " 99 41 61 42 71 62 1 98 43 44 21 22 31 32 51 45 2 52 3 23 33 46 34 35 65 4 12 66 13 47 24 53 72 14 5 48 54 49 67 40 73 56 25 "
foreach state of local  stateicp_70ID {
	do "${do_path}/data_build/IPUMS_data.do" 1970 `state'
}

* append czone observations from multiple states and compute CZ aggregates
import delimited "$orig/ID lists/czones.txt", clear
levelsof v1, local(czone)
foreach year of numlist 1920(10)1970 {
	foreach zone of local  czone {
		do "${do_path}/data_build/cz_append.do" `year' `zone'
	}
}

* append czone-year demographics across years 
do "${do_path}/data_build/demographics_append.do" 


**************************************************************************************
**		Census micro data 
**************************************************************************************

* Micro data: IPUMS Census data 1920-1970 for treatment and control occupations
use "${temp_path}/Census_micro", clear
drop group 

* clean income variable
replace incwage=. if incwage==999999 |  incwage==999998
replace incwage=. if incwage==0

* match micro data to CZs (separately by year)
levelsof year, local(year_list)
foreach year of local year_list {
	preserve
	keep if year==`year'
	do "${do_path}/data_build/cz_match2.do" `year'
	tempfile census_`year'
	save `census_`year'', replace
	restore
}
* append yearly data
use `census_1970', clear
foreach year of numlist 1920(10)1960 {
	display "census_`year'"
	append using `census_`year''
} 

* harmonize weights
do "${do_path}/data_build/weights.do"

save "${temp_path}/census_CZ" , replace

**************************************************************************************
**		compute CZ-occ-year aggregates
**************************************************************************************

* employment counts
do "${do_path}/data_build/employment.do"

* earnings rank
do "${do_path}/data_build/earnings_rank.do" 

* migration across CZs
do "${do_path}/data_build/migration.do"


* alternative wage extrapolation at top
do "${do_path}/data_build/earning_extrapolation.do" 
do "${do_path}/data_build/TI_share.do" 

* real earnings data
do "${do_path}/data_build/earnings.do" 

* state level data
do "${do_path}/data_build/state_data.do" 

* intra generational mobility of TV stars
do "${do_path}/data_build/rank_mobility.do" 


**************************************************************************************
**		Figure 1 
**************************************************************************************


* Earnings rank data
use "${temp_path}/earnings_rank", clear

* Keep treated occupation (entertainer)
keep if treated_occupation==1

** add data on TV (studio)
merge m:1 czone year using "${orig}/TVStudio"
drop _m

** add CZ demographics
merge m:1 czone year  using "${temp_path}/CZ_demographics", keep(3)
drop _m 

* save file
save "${temp_path}/F1B", replace 


**************************************************************************************
**		Figure 2A
**************************************************************************************

* income micro data
use czone year occ1950 ind1950 incwage perwt weight if year==1940 | year==1970 using "${temp_path}/census_CZ", clear

* keep observations with income data
drop if incwage==.

* generate real income 
do "${do_path}/data_build/deflate.do" incwage


* impose common top-code
g income=incwage if incwage<8500
replace income=8500 if incwage>8500
g linc = ln(income)

* harmonize weights
do "${do_path}/data_build/weights.do"

* define treated & untreated occupations
do "${do_path}/data_build/occupation_groups.do"
keep if treated_occupation==1

* save
cd "${do_path}"
save "${temp_path}/F2A", replace



**************************************************************************************
**		Figure 3
**************************************************************************************

* Data for Figure 3
use "${temp_path}/earnings_rank", clear

* Keep treated & control occupations
keep if treated_occupation==1 | treated_occupation==0

** add data on TV (studio)
merge m:1 czone year using "${orig}/TVStudio"
drop _m


** add CZ demographics
merge m:1 czone year  using "${temp_path}/CZ_demographics",  keep(3)
drop _m

* define FE
egen occ_year_FE=group(year occ19)
tab occ_year_FE, g(dummy_occ)
tab year, g(year_FE)

save "${temp_path}/F3", replace 

**************************************************************************************
**		Table 1
**************************************************************************************
* employment data
use "${temp_path}/employment_T1", clear

** add data on TV (signal)
merge m:1 czone year using "${orig}/TVsignal"
drop _m 

* add CZ demographics
merge m:1 czone year  using "${temp_path}/CZ_demographics", keep(3)
drop _m

* save data
save "${temp_path}/T1", replace
erase "${temp_path}/employment_T1.dta"

**************************************************************************************
**		Table 2
**************************************************************************************

* earnings rank data
use "${temp_path}/F1B", clear

* add migration data
merge 1:1 occ czone year using "${temp_path}/migration"
drop _m 

* save data
save "${temp_path}/T2", replace
erase "${temp_path}/migration.dta"


**************************************************************************************
**		APX 6
**************************************************************************************

* Data for Figure 3
use "${temp_path}/earnings_rank_APX", clear

** add data on TV (studio)
merge m:1 czone year using "${orig}/TVStudio"
drop _m

** add CZ demographics
merge m:1 czone year  using "${temp_path}/CZ_demographics",  keep(3)
drop _m

* broader control groups
** Bartender, cooks, counter workers in industry in eating and drinking, waiters
replace group = 3 if occ1950==750 | occ1950==754 | (occ1950==760) | occ1950==784
**recreation worker, attendants recreation, sports isntructor, Artist & teacher outside teaching, usher in amusement, authors
replace group = 2 if occ1950==77 | (occ1950==4 ) | occ1950==732 | occ1950==91 | occ1950==6
**Editor & reporter, Author,Photographer in , Newsboy
replace group = 4 if occ1950==36| (occ1950==74) |  occ1950==460
** Alternative treatment dummy
g treated=( occ1950 == 5 | occ1950 == 1   | occ1950 == 31   | occ1950 == 57 | occ1950 == 51 )
g Untreated_group=( occ1950==4 | occ1950==6 | occ1950==77 | occ1950==91 | occ1950==732 | occ1950==750 | occ1950==754 | occ1950==784| occ==760 | group>4)


save "${temp_path}/TA6", replace 
