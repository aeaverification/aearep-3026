/*************************************************/
/*************************************************/
/****************  Superstars  ***************/
/*************************************************/
/*************************************************/

	clear
	graph set print logo off

	graph set print tmargin 1
	graph set print lmargin 1
	set more off, perm
	set emptycells drop

	clear
	clear matrix
	set varabbrev on



**********************************
**Change paths********************
**********************************/

	// global dir "/Users/fkoenig/OneDrive/TV expansion/analysis/code/final"
    include "config.do"
	global dir "$rootdir"


	global do_path "$dir/code"
	global orig "$dir/orig"
	global temp_path "$dir/processed_data"
	global output_path "$dir/results"
	global log_path "$dir/log"
	global datum = subinstr(c(current_date)," ","",.)

	// added
	cap mkdir "$temp_path"
	cap mkdir "$output_path"
	cap mkdir "$log_path"

	cd "$temp_path" 


**********************************
** required ado files ************
**********************************/
	/* INSTALL OF GTOOLS ON an M1 MacBook
 	# Use TERMINAL and execute:
	git clone https://github.com/mcaceresb/stata-gtools
	cd stata-gtools
	git submodule update --init --recursive
	make clean SPI=3.0 SPIVER=v3
	make all SPI=3.0 SPIVER=v3

	# In STATA run:
 	net install gtools, from(/path_to_folder_where_gtools_was_compiled/stata-gtools/build) replace
	*/

	//cap ssc install reghdfe

**********************************
** clean raw data ****************
**********************************/
cap log close

log using "${log_path}/data_preparation", replace
cd "$do_path"
do "./data_build/data_preparation.do"




**********************************
** Figures ****************
**********************************/
cap log close
log using "${log_path}/results", replace

 set scheme lean2 , permanent

cd "$do_path"
do Figure1B.do

cd "$do_path"
do Figure2.do

cd "$do_path"
do Figure3.do

**********************************
** Tables ****************
**********************************/

cd "$do_path"
do Table1.do

cd "$do_path"
do Table2.do

**********************************
** Appendix ****************
**********************************/

cd "$do_path"
do "./Appendix/Appendix.do"


cap log close
