* NOTE: You need to set the Stata working directory to the path
* where the data file is located.

set more off

clear
quietly infix                     ///
  int     year           1-4      ///
  byte    datanum        5-6      ///
  double  serial         7-14     ///
  float   hhwt           15-24    ///
  float   cpi99          25-29    ///
  byte    stateicp       30-31    ///
  int     county         32-35    ///
  byte    urban          36-36    ///
  byte    gq             37-37    ///
  int     pernum         38-41    ///
  float   perwt          42-51    ///
  float   slwt           52-61    ///
  byte    slrec          62-62    ///
  byte    sex            63-63    ///
  int     age            64-66    ///
  byte    race           67-67    ///
  int     raced          68-70    ///
  byte    educ           71-72    ///
  int     educd          73-75    ///
  byte    empstat        76-76    ///
  byte    empstatd       77-78    ///
  byte    labforce       79-79    ///
  int     occ            80-83    ///
  int     occ1950        84-86    ///
  int     ind1950        87-89    ///
  byte    wkswork2       90-90    ///
  byte    hrswork2       91-91    ///
  int     uocc95         92-94    ///
  long    incwage        95-100   ///
  byte    incnonwg       101-101  ///
  byte    migrate5       102-102  ///
  byte    migrate5d      103-104  ///
  byte    qage           105-105  ///
  byte    qsex           106-106  ///
  byte    qrace          107-107  ///
  byte    qempstat       108-108  ///
  byte    qind           109-109  ///
  byte    qocc           110-110  ///
  byte    educ_pop       111-112  ///
  int     educd_pop      113-115  ///
  int     occ1950_pop    116-118  ///
  int     ind1950_pop    119-121  ///
  byte    wkswork2_pop   122-122  ///
  byte    hrswork2_pop   123-123  ///
  int     uocc95_pop     124-126  ///
  long    incwage_pop    127-132  ///
  byte    incnonwg_pop   133-133  ///
  byte    migrate5_pop   134-134  ///
  byte    migrate5d_pop  135-136  ///
  using `"${orig}/usa_00060.dat"'

replace hhwt          = hhwt          / 100
replace cpi99         = cpi99         / 1000
replace perwt         = perwt         / 100
replace slwt          = slwt          / 100

format serial        %8.0f
format hhwt          %10.2f
format cpi99         %5.3f
format perwt         %10.2f
format slwt          %10.2f

label var year          `"Census year"'
label var datanum       `"Data set number"'
label var serial        `"Household serial number"'
label var hhwt          `"Household weight"'
label var cpi99         `"CPI-U adjustment factor to 1999 dollars"'
label var stateicp      `"State (ICPSR code)"'
label var county        `"County"'
label var urban         `"Urban/rural status"'
label var gq            `"Group quarters status"'
label var pernum        `"Person number in sample unit"'
label var perwt         `"Person weight"'
label var slwt          `"Sample-line weight"'
label var slrec         `"Sample-line person identifier"'
label var sex           `"Sex"'
label var age           `"Age"'
label var race          `"Race [general version]"'
label var raced         `"Race [detailed version]"'
label var educ          `"Educational attainment [general version]"'
label var educd         `"Educational attainment [detailed version]"'
label var empstat       `"Employment status [general version]"'
label var empstatd      `"Employment status [detailed version]"'
label var labforce      `"Labor force status"'
label var occ           `"Occupation"'
label var occ1950       `"Occupation, 1950 basis"'
label var ind1950       `"Industry, 1950 basis"'
label var wkswork2      `"Weeks worked last year, intervalled"'
label var hrswork2      `"Hours worked last week, intervalled"'
label var uocc95        `"Usual occupation, 1950 classification"'
label var incwage       `"Wage and salary income"'
label var incnonwg      `"Had non-wage/salary income over $50"'
label var migrate5      `"Migration status, 5 years [general version]"'
label var migrate5d     `"Migration status, 5 years [detailed version]"'
label var qage          `"Flag for Age"'
label var qsex          `"Flag for Sex"'
label var qrace         `"Flag for Race, Racamind, Racasian, Racblk, Racpais, Racwht, Racoth, Racnum, Race"'
label var qempstat      `"Flag for Empstat, Labforce"'
label var qind          `"Flag for Ind, Ind1950"'
label var qocc          `"Flag for Occ, Occ1950, SEI, Occscore, Occsoc, Labforce"'
label var educ_pop      `"Educational attainment [of father; general version]"'
label var educd_pop     `"Educational attainment [of father; detailed version]"'
label var occ1950_pop   `"Occupation, 1950 basis [of father]"'
label var ind1950_pop   `"Industry, 1950 basis [of father]"'
label var wkswork2_pop  `"Weeks worked last year, intervalled [of father]"'
label var hrswork2_pop  `"Hours worked last week, intervalled [of father]"'
label var uocc95_pop    `"Usual occupation, 1950 classification [of father]"'
label var incwage_pop   `"Wage and salary income [of father]"'
label var incnonwg_pop  `"Had non-wage/salary income over $50 [of father]"'
label var migrate5_pop  `"Migration status, 5 years [of father; general version]"'
label var migrate5d_pop `"Migration status, 5 years [of father; detailed version]"'

label define year_lbl 1850 `"1850"'
label define year_lbl 1860 `"1860"', add
label define year_lbl 1870 `"1870"', add
label define year_lbl 1880 `"1880"', add
label define year_lbl 1900 `"1900"', add
label define year_lbl 1910 `"1910"', add
label define year_lbl 1920 `"1920"', add
label define year_lbl 1930 `"1930"', add
label define year_lbl 1940 `"1940"', add
label define year_lbl 1950 `"1950"', add
label define year_lbl 1960 `"1960"', add
label define year_lbl 1970 `"1970"', add
label define year_lbl 1980 `"1980"', add
label define year_lbl 1990 `"1990"', add
label define year_lbl 2000 `"2000"', add
label define year_lbl 2001 `"2001"', add
label define year_lbl 2002 `"2002"', add
label define year_lbl 2003 `"2003"', add
label define year_lbl 2004 `"2004"', add
label define year_lbl 2005 `"2005"', add
label define year_lbl 2006 `"2006"', add
label define year_lbl 2007 `"2007"', add
label define year_lbl 2008 `"2008"', add
label define year_lbl 2009 `"2009"', add
label define year_lbl 2010 `"2010"', add
label define year_lbl 2011 `"2011"', add
label define year_lbl 2012 `"2012"', add
label define year_lbl 2013 `"2013"', add
label define year_lbl 2014 `"2014"', add
label values year year_lbl

label define stateicp_lbl 01 `"Connecticut"'
label define stateicp_lbl 02 `"Maine"', add
label define stateicp_lbl 03 `"Massachusetts"', add
label define stateicp_lbl 04 `"New Hampshire"', add
label define stateicp_lbl 05 `"Rhode Island"', add
label define stateicp_lbl 06 `"Vermont"', add
label define stateicp_lbl 11 `"Delaware"', add
label define stateicp_lbl 12 `"New Jersey"', add
label define stateicp_lbl 13 `"New York"', add
label define stateicp_lbl 14 `"Pennsylvania"', add
label define stateicp_lbl 21 `"Illinois"', add
label define stateicp_lbl 22 `"Indiana"', add
label define stateicp_lbl 23 `"Michigan"', add
label define stateicp_lbl 24 `"Ohio"', add
label define stateicp_lbl 25 `"Wisconsin"', add
label define stateicp_lbl 31 `"Iowa"', add
label define stateicp_lbl 32 `"Kansas"', add
label define stateicp_lbl 33 `"Minnesota"', add
label define stateicp_lbl 34 `"Missouri"', add
label define stateicp_lbl 35 `"Nebraska"', add
label define stateicp_lbl 36 `"North Dakota"', add
label define stateicp_lbl 37 `"South Dakota"', add
label define stateicp_lbl 40 `"Virginia"', add
label define stateicp_lbl 41 `"Alabama"', add
label define stateicp_lbl 42 `"Arkansas"', add
label define stateicp_lbl 43 `"Florida"', add
label define stateicp_lbl 44 `"Georgia"', add
label define stateicp_lbl 45 `"Louisiana"', add
label define stateicp_lbl 46 `"Mississippi"', add
label define stateicp_lbl 47 `"North Carolina"', add
label define stateicp_lbl 48 `"South Carolina"', add
label define stateicp_lbl 49 `"Texas"', add
label define stateicp_lbl 51 `"Kentucky"', add
label define stateicp_lbl 52 `"Maryland"', add
label define stateicp_lbl 53 `"Oklahoma"', add
label define stateicp_lbl 54 `"Tennessee"', add
label define stateicp_lbl 56 `"West Virginia"', add
label define stateicp_lbl 61 `"Arizona"', add
label define stateicp_lbl 62 `"Colorado"', add
label define stateicp_lbl 63 `"Idaho"', add
label define stateicp_lbl 64 `"Montana"', add
label define stateicp_lbl 65 `"Nevada"', add
label define stateicp_lbl 66 `"New Mexico"', add
label define stateicp_lbl 67 `"Utah"', add
label define stateicp_lbl 68 `"Wyoming"', add
label define stateicp_lbl 71 `"California"', add
label define stateicp_lbl 72 `"Oregon"', add
label define stateicp_lbl 73 `"Washington"', add
label define stateicp_lbl 81 `"Alaska"', add
label define stateicp_lbl 82 `"Hawaii"', add
label define stateicp_lbl 83 `"Puerto Rico"', add
label define stateicp_lbl 96 `"State groupings (1980 Urban/rural sample)"', add
label define stateicp_lbl 97 `"Military/Mil. Reservations"', add
label define stateicp_lbl 98 `"District of Columbia"', add
label define stateicp_lbl 99 `"State not identified"', add
label values stateicp stateicp_lbl

label define urban_lbl 0 `"N/A"'
label define urban_lbl 1 `"Rural"', add
label define urban_lbl 2 `"Urban"', add
label values urban urban_lbl

label define gq_lbl 0 `"Vacant unit"'
label define gq_lbl 1 `"Households under 1970 definition"', add
label define gq_lbl 2 `"Additional households under 1990 definition"', add
label define gq_lbl 3 `"Group quarters--Institutions"', add
label define gq_lbl 4 `"Other group quarters"', add
label define gq_lbl 5 `"Additional households under 2000 definition"', add
label define gq_lbl 6 `"Fragment"', add
label values gq gq_lbl

label define slrec_lbl 1 `"Not a sample-line person"'
label define slrec_lbl 2 `"Sample-line person"', add
label values slrec slrec_lbl

label define sex_lbl 1 `"Male"'
label define sex_lbl 2 `"Female"', add
label values sex sex_lbl

label define age_lbl 000 `"Less than 1 year old"'
label define age_lbl 001 `"1"', add
label define age_lbl 002 `"2"', add
label define age_lbl 003 `"3"', add
label define age_lbl 004 `"4"', add
label define age_lbl 005 `"5"', add
label define age_lbl 006 `"6"', add
label define age_lbl 007 `"7"', add
label define age_lbl 008 `"8"', add
label define age_lbl 009 `"9"', add
label define age_lbl 010 `"10"', add
label define age_lbl 011 `"11"', add
label define age_lbl 012 `"12"', add
label define age_lbl 013 `"13"', add
label define age_lbl 014 `"14"', add
label define age_lbl 015 `"15"', add
label define age_lbl 016 `"16"', add
label define age_lbl 017 `"17"', add
label define age_lbl 018 `"18"', add
label define age_lbl 019 `"19"', add
label define age_lbl 020 `"20"', add
label define age_lbl 021 `"21"', add
label define age_lbl 022 `"22"', add
label define age_lbl 023 `"23"', add
label define age_lbl 024 `"24"', add
label define age_lbl 025 `"25"', add
label define age_lbl 026 `"26"', add
label define age_lbl 027 `"27"', add
label define age_lbl 028 `"28"', add
label define age_lbl 029 `"29"', add
label define age_lbl 030 `"30"', add
label define age_lbl 031 `"31"', add
label define age_lbl 032 `"32"', add
label define age_lbl 033 `"33"', add
label define age_lbl 034 `"34"', add
label define age_lbl 035 `"35"', add
label define age_lbl 036 `"36"', add
label define age_lbl 037 `"37"', add
label define age_lbl 038 `"38"', add
label define age_lbl 039 `"39"', add
label define age_lbl 040 `"40"', add
label define age_lbl 041 `"41"', add
label define age_lbl 042 `"42"', add
label define age_lbl 043 `"43"', add
label define age_lbl 044 `"44"', add
label define age_lbl 045 `"45"', add
label define age_lbl 046 `"46"', add
label define age_lbl 047 `"47"', add
label define age_lbl 048 `"48"', add
label define age_lbl 049 `"49"', add
label define age_lbl 050 `"50"', add
label define age_lbl 051 `"51"', add
label define age_lbl 052 `"52"', add
label define age_lbl 053 `"53"', add
label define age_lbl 054 `"54"', add
label define age_lbl 055 `"55"', add
label define age_lbl 056 `"56"', add
label define age_lbl 057 `"57"', add
label define age_lbl 058 `"58"', add
label define age_lbl 059 `"59"', add
label define age_lbl 060 `"60"', add
label define age_lbl 061 `"61"', add
label define age_lbl 062 `"62"', add
label define age_lbl 063 `"63"', add
label define age_lbl 064 `"64"', add
label define age_lbl 065 `"65"', add
label define age_lbl 066 `"66"', add
label define age_lbl 067 `"67"', add
label define age_lbl 068 `"68"', add
label define age_lbl 069 `"69"', add
label define age_lbl 070 `"70"', add
label define age_lbl 071 `"71"', add
label define age_lbl 072 `"72"', add
label define age_lbl 073 `"73"', add
label define age_lbl 074 `"74"', add
label define age_lbl 075 `"75"', add
label define age_lbl 076 `"76"', add
label define age_lbl 077 `"77"', add
label define age_lbl 078 `"78"', add
label define age_lbl 079 `"79"', add
label define age_lbl 080 `"80"', add
label define age_lbl 081 `"81"', add
label define age_lbl 082 `"82"', add
label define age_lbl 083 `"83"', add
label define age_lbl 084 `"84"', add
label define age_lbl 085 `"85"', add
label define age_lbl 086 `"86"', add
label define age_lbl 087 `"87"', add
label define age_lbl 088 `"88"', add
label define age_lbl 089 `"89"', add
label define age_lbl 090 `"90 (90+ in 1980 and 1990)"', add
label define age_lbl 091 `"91"', add
label define age_lbl 092 `"92"', add
label define age_lbl 093 `"93"', add
label define age_lbl 094 `"94"', add
label define age_lbl 095 `"95"', add
label define age_lbl 096 `"96"', add
label define age_lbl 097 `"97"', add
label define age_lbl 098 `"98"', add
label define age_lbl 099 `"99"', add
label define age_lbl 100 `"100 (100+ in 1960-1970)"', add
label define age_lbl 101 `"101"', add
label define age_lbl 102 `"102"', add
label define age_lbl 103 `"103"', add
label define age_lbl 104 `"104"', add
label define age_lbl 105 `"105"', add
label define age_lbl 106 `"106"', add
label define age_lbl 107 `"107"', add
label define age_lbl 108 `"108"', add
label define age_lbl 109 `"109"', add
label define age_lbl 110 `"110"', add
label define age_lbl 111 `"111"', add
label define age_lbl 112 `"112 (112+ in the 1980 internal data)"', add
label define age_lbl 113 `"113"', add
label define age_lbl 114 `"114"', add
label define age_lbl 115 `"115 (115+ in the 1990 internal data)"', add
label define age_lbl 116 `"116"', add
label define age_lbl 117 `"117"', add
label define age_lbl 118 `"118"', add
label define age_lbl 119 `"119"', add
label define age_lbl 120 `"120"', add
label define age_lbl 121 `"121"', add
label define age_lbl 122 `"122"', add
label define age_lbl 123 `"123"', add
label define age_lbl 124 `"124"', add
label define age_lbl 125 `"125"', add
label define age_lbl 126 `"126"', add
label define age_lbl 129 `"129"', add
label define age_lbl 130 `"130"', add
label define age_lbl 135 `"135"', add
label values age age_lbl

label define race_lbl 1 `"White"'
label define race_lbl 2 `"Black/Negro"', add
label define race_lbl 3 `"American Indian or Alaska Native"', add
label define race_lbl 4 `"Chinese"', add
label define race_lbl 5 `"Japanese"', add
label define race_lbl 6 `"Other Asian or Pacific Islander"', add
label define race_lbl 7 `"Other race, nec"', add
label define race_lbl 8 `"Two major races"', add
label define race_lbl 9 `"Three or more major races"', add
label values race race_lbl

label define raced_lbl 100 `"White"'
label define raced_lbl 110 `"Spanish write_in"', add
label define raced_lbl 120 `"Blank (white) (1850)"', add
label define raced_lbl 130 `"Portuguese"', add
label define raced_lbl 140 `"Mexican (1930)"', add
label define raced_lbl 150 `"Puerto Rican (1910 Hawaii)"', add
label define raced_lbl 200 `"Black/Negro"', add
label define raced_lbl 210 `"Mulatto"', add
label define raced_lbl 300 `"American Indian/Alaska Native"', add
label define raced_lbl 302 `"Apache"', add
label define raced_lbl 303 `"Blackfoot"', add
label define raced_lbl 304 `"Cherokee"', add
label define raced_lbl 305 `"Cheyenne"', add
label define raced_lbl 306 `"Chickasaw"', add
label define raced_lbl 307 `"Chippewa"', add
label define raced_lbl 308 `"Choctaw"', add
label define raced_lbl 309 `"Comanche"', add
label define raced_lbl 310 `"Creek"', add
label define raced_lbl 311 `"Crow"', add
label define raced_lbl 312 `"Iroquois"', add
label define raced_lbl 313 `"Kiowa"', add
label define raced_lbl 314 `"Lumbee"', add
label define raced_lbl 315 `"Navajo"', add
label define raced_lbl 316 `"Osage"', add
label define raced_lbl 317 `"Paiute"', add
label define raced_lbl 318 `"Pima"', add
label define raced_lbl 319 `"Potawatomi"', add
label define raced_lbl 320 `"Pueblo"', add
label define raced_lbl 321 `"Seminole"', add
label define raced_lbl 322 `"Shoshone"', add
label define raced_lbl 323 `"Sioux"', add
label define raced_lbl 324 `"Tlingit (Tlingit_Haida, 2000/ACS)"', add
label define raced_lbl 325 `"Tohono O Odham"', add
label define raced_lbl 326 `"All other tribes (1990)"', add
label define raced_lbl 328 `"Hopi"', add
label define raced_lbl 329 `"Central American Indian"', add
label define raced_lbl 330 `"Spanish American Indian"', add
label define raced_lbl 350 `"Delaware"', add
label define raced_lbl 351 `"Latin American Indian"', add
label define raced_lbl 352 `"Puget Sound Salish"', add
label define raced_lbl 353 `"Yakama"', add
label define raced_lbl 354 `"Yaqui"', add
label define raced_lbl 355 `"Colville"', add
label define raced_lbl 356 `"Houma"', add
label define raced_lbl 357 `"Menominee"', add
label define raced_lbl 358 `"Yuman"', add
label define raced_lbl 359 `"South American Indian"', add
label define raced_lbl 360 `"Mexican American Indian"', add
label define raced_lbl 361 `"Other Amer. Indian tribe (2000,ACS)"', add
label define raced_lbl 362 `"2+ Amer. Indian tribes (2000,ACS)"', add
label define raced_lbl 370 `"Alaskan Athabaskan"', add
label define raced_lbl 371 `"Aleut"', add
label define raced_lbl 372 `"Eskimo"', add
label define raced_lbl 373 `"Alaskan mixed"', add
label define raced_lbl 374 `"Inupiat"', add
label define raced_lbl 375 `"Yup'ik"', add
label define raced_lbl 379 `"Other Alaska Native tribe(s) (2000,ACS)"', add
label define raced_lbl 398 `"Both Am. Ind. and Alaska Native (2000,ACS)"', add
label define raced_lbl 399 `"Tribe not specified"', add
label define raced_lbl 400 `"Chinese"', add
label define raced_lbl 410 `"Taiwanese"', add
label define raced_lbl 420 `"Chinese and Taiwanese"', add
label define raced_lbl 500 `"Japanese"', add
label define raced_lbl 600 `"Filipino"', add
label define raced_lbl 610 `"Asian Indian (Hindu 1920_1940)"', add
label define raced_lbl 620 `"Korean"', add
label define raced_lbl 630 `"Hawaiian"', add
label define raced_lbl 631 `"Hawaiian and Asian (1900,1920)"', add
label define raced_lbl 632 `"Hawaiian and European (1900,1920)"', add
label define raced_lbl 634 `"Hawaiian mixed"', add
label define raced_lbl 640 `"Vietnamese"', add
label define raced_lbl 641 `"   Bhutanese"', add
label define raced_lbl 642 `"   Mongolian "', add
label define raced_lbl 643 `"   Nepalese"', add
label define raced_lbl 650 `"Other Asian or Pacific Islander (1920,1980)"', add
label define raced_lbl 651 `"Asian only (CPS)"', add
label define raced_lbl 652 `"Pacific Islander only (CPS)"', add
label define raced_lbl 653 `"Asian or Pacific Islander, n.s. (1990 Internal Census files)"', add
label define raced_lbl 660 `"Cambodian"', add
label define raced_lbl 661 `"Hmong"', add
label define raced_lbl 662 `"Laotian"', add
label define raced_lbl 663 `"Thai"', add
label define raced_lbl 664 `"Bangladeshi"', add
label define raced_lbl 665 `"Burmese"', add
label define raced_lbl 666 `"Indonesian"', add
label define raced_lbl 667 `"Malaysian"', add
label define raced_lbl 668 `"Okinawan"', add
label define raced_lbl 669 `"Pakistani"', add
label define raced_lbl 670 `"Sri Lankan"', add
label define raced_lbl 671 `"Other Asian, n.e.c."', add
label define raced_lbl 672 `"Asian, not specified"', add
label define raced_lbl 673 `"Chinese and Japanese"', add
label define raced_lbl 674 `"Chinese and Filipino"', add
label define raced_lbl 675 `"Chinese and Vietnamese"', add
label define raced_lbl 676 `"Chinese and Asian write_in"', add
label define raced_lbl 677 `"Japanese and Filipino"', add
label define raced_lbl 678 `"Asian Indian and Asian write_in"', add
label define raced_lbl 679 `"Other Asian race combinations"', add
label define raced_lbl 680 `"Samoan"', add
label define raced_lbl 681 `"Tahitian"', add
label define raced_lbl 682 `"Tongan"', add
label define raced_lbl 683 `"Other Polynesian (1990)"', add
label define raced_lbl 684 `"1+ other Polynesian races (2000,ACS)"', add
label define raced_lbl 685 `"Guamanian/Chamorro"', add
label define raced_lbl 686 `"Northern Mariana Islander"', add
label define raced_lbl 687 `"Palauan"', add
label define raced_lbl 688 `"Other Micronesian (1990)"', add
label define raced_lbl 689 `"1+ other Micronesian races (2000,ACS)"', add
label define raced_lbl 690 `"Fijian"', add
label define raced_lbl 691 `"Other Melanesian (1990)"', add
label define raced_lbl 692 `"1+ other Melanesian races (2000,ACS)"', add
label define raced_lbl 698 `"2+ PI races from 2+ PI regions"', add
label define raced_lbl 699 `"Pacific Islander, n.s."', add
label define raced_lbl 700 `"Other race, n.e.c."', add
label define raced_lbl 801 `"White and Black"', add
label define raced_lbl 802 `"White and AIAN"', add
label define raced_lbl 810 `"White and Asian"', add
label define raced_lbl 811 `"White and Chinese"', add
label define raced_lbl 812 `"White and Japanese"', add
label define raced_lbl 813 `"White and Filipino"', add
label define raced_lbl 814 `"White and Asian Indian"', add
label define raced_lbl 815 `"White and Korean"', add
label define raced_lbl 816 `"White and Vietnamese"', add
label define raced_lbl 817 `"White and Asian write_in"', add
label define raced_lbl 818 `"White and other Asian race(s)"', add
label define raced_lbl 819 `"White and two or more Asian groups"', add
label define raced_lbl 820 `"White and PI  "', add
label define raced_lbl 821 `"White and Native Hawaiian"', add
label define raced_lbl 822 `"White and Samoan"', add
label define raced_lbl 823 `"White and Guamanian"', add
label define raced_lbl 824 `"White and PI write_in"', add
label define raced_lbl 825 `"White and other PI race(s)"', add
label define raced_lbl 826 `"White and other race write_in"', add
label define raced_lbl 827 `"White and other race, n.e.c."', add
label define raced_lbl 830 `"Black and AIAN"', add
label define raced_lbl 831 `"Black and Asian"', add
label define raced_lbl 832 `"Black and Chinese"', add
label define raced_lbl 833 `"Black and Japanese"', add
label define raced_lbl 834 `"Black and Filipino"', add
label define raced_lbl 835 `"Black and Asian Indian"', add
label define raced_lbl 836 `"Black and Korean"', add
label define raced_lbl 837 `"Black and Asian write_in"', add
label define raced_lbl 838 `"Black and other Asian race(s)"', add
label define raced_lbl 840 `"Black and PI"', add
label define raced_lbl 841 `"Black and PI write_in"', add
label define raced_lbl 842 `"Black and other PI race(s)"', add
label define raced_lbl 845 `"Black and other race write_in"', add
label define raced_lbl 850 `"AIAN and Asian"', add
label define raced_lbl 851 `"AIAN and Filipino (2000 1%)"', add
label define raced_lbl 852 `"AIAN and Asian Indian"', add
label define raced_lbl 853 `"AIAN and Asian write_in (2000 1%)"', add
label define raced_lbl 854 `"AIAN and other Asian race(s)"', add
label define raced_lbl 855 `"AIAN and PI"', add
label define raced_lbl 856 `"AIAN and other race write_in"', add
label define raced_lbl 860 `"Asian and PI"', add
label define raced_lbl 861 `"Chinese and Hawaiian"', add
label define raced_lbl 862 `"Chinese, Filipino, Hawaiian (2000 1%)"', add
label define raced_lbl 863 `"Japanese and Hawaiian (2000 1%)"', add
label define raced_lbl 864 `"Filipino and Hawaiian"', add
label define raced_lbl 865 `"Filipino and PI write_in"', add
label define raced_lbl 866 `"Asian Indian and PI write_in (2000 1%)"', add
label define raced_lbl 867 `"Asian write_in and PI write_in"', add
label define raced_lbl 868 `"Other Asian race(s) and PI race(s)"', add
label define raced_lbl 869 `"Japanese and Korean (ACS)"', add
label define raced_lbl 880 `"Asian and other race write_in"', add
label define raced_lbl 881 `"Chinese and other race write_in"', add
label define raced_lbl 882 `"Japanese and other race write_in"', add
label define raced_lbl 883 `"Filipino and other race write_in"', add
label define raced_lbl 884 `"Asian Indian and other race write_in"', add
label define raced_lbl 885 `"Asian write_in and other race write_in"', add
label define raced_lbl 886 `"Other Asian race(s) and other race write_in"', add
label define raced_lbl 887 `"      Chinese and Korean"', add
label define raced_lbl 890 `"PI and other race write_in: "', add
label define raced_lbl 891 `"PI write_in and other race write_in"', add
label define raced_lbl 892 `"Other PI race(s) and other race write_in"', add
label define raced_lbl 893 `"         Native Hawaiian or PI other race(s)"', add
label define raced_lbl 899 `"API and other race write_in"', add
label define raced_lbl 901 `"White, Black, AIAN"', add
label define raced_lbl 902 `"White, Black, Asian"', add
label define raced_lbl 903 `"White, Black, PI"', add
label define raced_lbl 904 `"White, Black, other race write_in"', add
label define raced_lbl 905 `"White, AIAN, Asian"', add
label define raced_lbl 906 `"White, AIAN, PI"', add
label define raced_lbl 907 `"White, AIAN, other race write_in"', add
label define raced_lbl 910 `"White, Asian, PI "', add
label define raced_lbl 911 `"White, Chinese, Hawaiian"', add
label define raced_lbl 912 `"White, Chinese, Filipino, Hawaiian (2000 1%)"', add
label define raced_lbl 913 `"White, Japanese, Hawaiian (2000 1%)"', add
label define raced_lbl 914 `"White, Filipino, Hawaiian"', add
label define raced_lbl 915 `"Other White, Asian race(s), PI race(s)"', add
label define raced_lbl 916 `"      White, AIAN and Filipino"', add
label define raced_lbl 917 `"      White, Black, and Filipino"', add
label define raced_lbl 920 `"White, Asian, other race write_in"', add
label define raced_lbl 921 `"White, Filipino, other race write_in (2000 1%)"', add
label define raced_lbl 922 `"White, Asian write_in, other race write_in (2000 1%)"', add
label define raced_lbl 923 `"Other White, Asian race(s), other race write_in (2000 1%)"', add
label define raced_lbl 925 `"White, PI, other race write_in"', add
label define raced_lbl 930 `"Black, AIAN, Asian"', add
label define raced_lbl 931 `"Black, AIAN, PI"', add
label define raced_lbl 932 `"Black, AIAN, other race write_in"', add
label define raced_lbl 933 `"Black, Asian, PI"', add
label define raced_lbl 934 `"Black, Asian, other race write_in"', add
label define raced_lbl 935 `"Black, PI, other race write_in"', add
label define raced_lbl 940 `"AIAN, Asian, PI"', add
label define raced_lbl 941 `"AIAN, Asian, other race write_in"', add
label define raced_lbl 942 `"AIAN, PI, other race write_in"', add
label define raced_lbl 943 `"Asian, PI, other race write_in"', add
label define raced_lbl 944 `"Asian (Chinese, Japanese, Korean, Vietnamese); and Native Hawaiian or PI; and Other"', add
label define raced_lbl 949 `"2 or 3 races (CPS)"', add
label define raced_lbl 950 `"White, Black, AIAN, Asian"', add
label define raced_lbl 951 `"White, Black, AIAN, PI"', add
label define raced_lbl 952 `"White, Black, AIAN, other race write_in"', add
label define raced_lbl 953 `"White, Black, Asian, PI"', add
label define raced_lbl 954 `"White, Black, Asian, other race write_in"', add
label define raced_lbl 955 `"White, Black, PI, other race write_in"', add
label define raced_lbl 960 `"White, AIAN, Asian, PI"', add
label define raced_lbl 961 `"White, AIAN, Asian, other race write_in"', add
label define raced_lbl 962 `"White, AIAN, PI, other race write_in"', add
label define raced_lbl 963 `"White, Asian, PI, other race write_in"', add
label define raced_lbl 964 `"White, Chinese, Japanese, Native Hawaiian"', add
label define raced_lbl 970 `"Black, AIAN, Asian, PI"', add
label define raced_lbl 971 `"Black, AIAN, Asian, other race write_in"', add
label define raced_lbl 972 `"Black, AIAN, PI, other race write_in"', add
label define raced_lbl 973 `"Black, Asian, PI, other race write_in"', add
label define raced_lbl 974 `"AIAN, Asian, PI, other race write_in"', add
label define raced_lbl 975 `"AIAN, Asian, PI, Hawaiian other race write_in"', add
label define raced_lbl 976 `"Two specified Asian  (Chinese and other Asian, Chinese and Japanese, Japanese and other Asian, Korean and other Asian); Native Hawaiian/PI; and Other Race"', add
label define raced_lbl 980 `"White, Black, AIAN, Asian, PI"', add
label define raced_lbl 981 `"White, Black, AIAN, Asian, other race write_in"', add
label define raced_lbl 982 `"White, Black, AIAN, PI, other race write_in"', add
label define raced_lbl 983 `"White, Black, Asian, PI, other race write_in"', add
label define raced_lbl 984 `"White, AIAN, Asian, PI, other race write_in"', add
label define raced_lbl 985 `"Black, AIAN, Asian, PI, other race write_in"', add
label define raced_lbl 986 `"Black, AIAN, Asian, PI, Hawaiian, other race write_in"', add
label define raced_lbl 989 `"4 or 5 races (CPS)"', add
label define raced_lbl 990 `"White, Black, AIAN, Asian, PI, other race write_in"', add
label define raced_lbl 991 `"White race; Some other race; Black or African American race and/or American Indian and Alaska Native race and/or Asian groups and/or Native Hawaiian and Other Pacific Islander groups"', add
label define raced_lbl 996 `"2+ races, n.e.c. (CPS)"', add
label values raced raced_lbl

label define educ_lbl 00 `"N/A or no schooling"'
label define educ_lbl 01 `"Nursery school to grade 4"', add
label define educ_lbl 02 `"Grade 5, 6, 7, or 8"', add
label define educ_lbl 03 `"Grade 9"', add
label define educ_lbl 04 `"Grade 10"', add
label define educ_lbl 05 `"Grade 11"', add
label define educ_lbl 06 `"Grade 12"', add
label define educ_lbl 07 `"1 year of college"', add
label define educ_lbl 08 `"2 years of college"', add
label define educ_lbl 09 `"3 years of college"', add
label define educ_lbl 10 `"4 years of college"', add
label define educ_lbl 11 `"5+ years of college"', add
label values educ educ_lbl

label define educd_lbl 000 `"N/A or no schooling"'
label define educd_lbl 001 `"N/A"', add
label define educd_lbl 002 `"No schooling completed"', add
label define educd_lbl 010 `"Nursery school to grade 4"', add
label define educd_lbl 011 `"Nursery school, preschool"', add
label define educd_lbl 012 `"Kindergarten"', add
label define educd_lbl 013 `"Grade 1, 2, 3, or 4"', add
label define educd_lbl 014 `"Grade 1"', add
label define educd_lbl 015 `"Grade 2"', add
label define educd_lbl 016 `"Grade 3"', add
label define educd_lbl 017 `"Grade 4"', add
label define educd_lbl 020 `"Grade 5, 6, 7, or 8"', add
label define educd_lbl 021 `"Grade 5 or 6"', add
label define educd_lbl 022 `"Grade 5"', add
label define educd_lbl 023 `"Grade 6"', add
label define educd_lbl 024 `"Grade 7 or 8"', add
label define educd_lbl 025 `"Grade 7"', add
label define educd_lbl 026 `"Grade 8"', add
label define educd_lbl 030 `"Grade 9"', add
label define educd_lbl 040 `"Grade 10"', add
label define educd_lbl 050 `"Grade 11"', add
label define educd_lbl 060 `"Grade 12"', add
label define educd_lbl 061 `"12th grade, no diploma"', add
label define educd_lbl 062 `"High school graduate or GED"', add
label define educd_lbl 063 `"Regular high school diploma"', add
label define educd_lbl 064 `"GED or alternative credential"', add
label define educd_lbl 065 `"Some college, but less than 1 year"', add
label define educd_lbl 070 `"1 year of college"', add
label define educd_lbl 071 `"1 or more years of college credit, no degree"', add
label define educd_lbl 080 `"2 years of college"', add
label define educd_lbl 081 `"Associate's degree, type not specified"', add
label define educd_lbl 082 `"Associate's degree, occupational program"', add
label define educd_lbl 083 `"Associate's degree, academic program"', add
label define educd_lbl 090 `"3 years of college"', add
label define educd_lbl 100 `"4 years of college"', add
label define educd_lbl 101 `"Bachelor's degree"', add
label define educd_lbl 110 `"5+ years of college"', add
label define educd_lbl 111 `"6 years of college (6+ in 1960-1970)"', add
label define educd_lbl 112 `"7 years of college"', add
label define educd_lbl 113 `"8+ years of college"', add
label define educd_lbl 114 `"Master's degree"', add
label define educd_lbl 115 `"Professional degree beyond a bachelor's degree"', add
label define educd_lbl 116 `"Doctoral degree"', add
label define educd_lbl 999 `"Missing"', add
label values educd educd_lbl

label define empstat_lbl 0 `"N/A"'
label define empstat_lbl 1 `"Employed"', add
label define empstat_lbl 2 `"Unemployed"', add
label define empstat_lbl 3 `"Not in labor force"', add
label values empstat empstat_lbl

label define empstatd_lbl 00 `"N/A"'
label define empstatd_lbl 10 `"At work"', add
label define empstatd_lbl 11 `"At work, public emerg"', add
label define empstatd_lbl 12 `"Has job, not working"', add
label define empstatd_lbl 13 `"Armed forces"', add
label define empstatd_lbl 14 `"Armed forces--at work"', add
label define empstatd_lbl 15 `"Armed forces--not at work but with job"', add
label define empstatd_lbl 20 `"Unemployed"', add
label define empstatd_lbl 21 `"Unemp, exper worker"', add
label define empstatd_lbl 22 `"Unemp, new worker"', add
label define empstatd_lbl 30 `"Not in Labor Force"', add
label define empstatd_lbl 31 `"NILF, housework"', add
label define empstatd_lbl 32 `"NILF, unable to work"', add
label define empstatd_lbl 33 `"NILF, school"', add
label define empstatd_lbl 34 `"NILF, other"', add
label values empstatd empstatd_lbl

label define labforce_lbl 0 `"N/A"'
label define labforce_lbl 1 `"No, not in the labor force"', add
label define labforce_lbl 2 `"Yes, in the labor force"', add
label values labforce labforce_lbl

label define occ1950_lbl 000 `"Accountants and auditors"'
label define occ1950_lbl 001 `"Actors and actresses"', add
label define occ1950_lbl 002 `"Airplane pilots and navigators"', add
label define occ1950_lbl 003 `"Architects"', add
label define occ1950_lbl 004 `"Artists and art teachers"', add
label define occ1950_lbl 005 `"Athletes"', add
label define occ1950_lbl 006 `"Authors"', add
label define occ1950_lbl 007 `"Chemists"', add
label define occ1950_lbl 008 `"Chiropractors"', add
label define occ1950_lbl 009 `"Clergymen"', add
label define occ1950_lbl 010 `"College presidents and deans"', add
label define occ1950_lbl 012 `"Agricultural sciences-Professors and instructors"', add
label define occ1950_lbl 013 `"Biological sciences-Professors and instructors"', add
label define occ1950_lbl 014 `"Chemistry-Professors and instructors"', add
label define occ1950_lbl 015 `"Economics-Professors and instructors"', add
label define occ1950_lbl 016 `"Engineering-Professors and instructors"', add
label define occ1950_lbl 017 `"Geology and geophysics-Professors and instructors"', add
label define occ1950_lbl 018 `"Mathematics-Professors and instructors"', add
label define occ1950_lbl 019 `"Medical Sciences-Professors and instructors"', add
label define occ1950_lbl 023 `"Physics-Professors and instructors"', add
label define occ1950_lbl 024 `"Psychology-Professors and instructors"', add
label define occ1950_lbl 025 `"Statistics-Professors and instructors"', add
label define occ1950_lbl 026 `"Natural science (nec)-Professors and instructors"', add
label define occ1950_lbl 027 `"Social sciences (nec)-Professors and instructors"', add
label define occ1950_lbl 028 `"Non-scientific subjects-Professors and instructors"', add
label define occ1950_lbl 029 `"Subject not specified-Professors and instructors"', add
label define occ1950_lbl 031 `"Dancers and dancing teachers"', add
label define occ1950_lbl 032 `"Dentists"', add
label define occ1950_lbl 033 `"Designers"', add
label define occ1950_lbl 034 `"Dietitians and nutritionists"', add
label define occ1950_lbl 035 `"Draftsmen"', add
label define occ1950_lbl 036 `"Editors and reporters"', add
label define occ1950_lbl 041 `"Aeronautical-Engineers"', add
label define occ1950_lbl 042 `"Chemical-Engineers"', add
label define occ1950_lbl 043 `"Civil-Engineers"', add
label define occ1950_lbl 044 `"Electrical-Engineers"', add
label define occ1950_lbl 045 `"Industrial-Engineers"', add
label define occ1950_lbl 046 `"Mechanical-Engineers"', add
label define occ1950_lbl 047 `"Metallurgical, metallurgists-Engineers"', add
label define occ1950_lbl 048 `"Mining-Engineers"', add
label define occ1950_lbl 049 `"Engineers (nec)"', add
label define occ1950_lbl 051 `"Entertainers (nec)"', add
label define occ1950_lbl 052 `"Farm and home management advisors"', add
label define occ1950_lbl 053 `"Foresters and conservationists"', add
label define occ1950_lbl 054 `"Funeral directors and embalmers"', add
label define occ1950_lbl 055 `"Lawyers and judges"', add
label define occ1950_lbl 056 `"Librarians"', add
label define occ1950_lbl 057 `"Musicians and music teachers"', add
label define occ1950_lbl 058 `"Nurses, professional"', add
label define occ1950_lbl 059 `"Nurses, student professional"', add
label define occ1950_lbl 061 `"Agricultural scientists"', add
label define occ1950_lbl 062 `"Biological scientists"', add
label define occ1950_lbl 063 `"Geologists and geophysicists"', add
label define occ1950_lbl 067 `"Mathematicians"', add
label define occ1950_lbl 068 `"Physicists"', add
label define occ1950_lbl 069 `"Misc. natural scientists"', add
label define occ1950_lbl 070 `"Optometrists"', add
label define occ1950_lbl 071 `"Osteopaths"', add
label define occ1950_lbl 072 `"Personnel and labor relations workers"', add
label define occ1950_lbl 073 `"Pharmacists"', add
label define occ1950_lbl 074 `"Photographers"', add
label define occ1950_lbl 075 `"Physicians and surgeons"', add
label define occ1950_lbl 076 `"Radio operators"', add
label define occ1950_lbl 077 `"Recreation and group workers"', add
label define occ1950_lbl 078 `"Religious workers"', add
label define occ1950_lbl 079 `"Social and welfare workers, except group"', add
label define occ1950_lbl 081 `"Economists"', add
label define occ1950_lbl 082 `"Psychologists"', add
label define occ1950_lbl 083 `"Statisticians and actuaries"', add
label define occ1950_lbl 084 `"Misc social scientists"', add
label define occ1950_lbl 091 `"Sports instructors and officials"', add
label define occ1950_lbl 092 `"Surveyors"', add
label define occ1950_lbl 093 `"Teachers (n.e.c.)"', add
label define occ1950_lbl 094 `"Medical and dental-technicians"', add
label define occ1950_lbl 095 `"Testing-technicians"', add
label define occ1950_lbl 096 `"Technicians (nec)"', add
label define occ1950_lbl 097 `"Therapists and healers (nec)"', add
label define occ1950_lbl 098 `"Veterinarians"', add
label define occ1950_lbl 099 `"Professional, technical and kindred workers (nec)"', add
label define occ1950_lbl 100 `"Farmers (owners and tenants)"', add
label define occ1950_lbl 123 `"Farm managers"', add
label define occ1950_lbl 200 `"Buyers and dept heads, store"', add
label define occ1950_lbl 201 `"Buyers and shippers, farm products"', add
label define occ1950_lbl 203 `"Conductors, railroad"', add
label define occ1950_lbl 204 `"Credit men"', add
label define occ1950_lbl 205 `"Floormen and floor managers, store"', add
label define occ1950_lbl 210 `"Inspectors, public administration"', add
label define occ1950_lbl 230 `"Managers and superintendants, building"', add
label define occ1950_lbl 240 `"Officers, pilots, pursers and engineers, ship"', add
label define occ1950_lbl 250 `"Officials and administratators (nec), public administration"', add
label define occ1950_lbl 260 `"Officials, lodge, society, union, etc."', add
label define occ1950_lbl 270 `"Postmasters"', add
label define occ1950_lbl 280 `"Purchasing agents and buyers (nec)"', add
label define occ1950_lbl 290 `"Managers, officials, and proprietors (nec)"', add
label define occ1950_lbl 300 `"Agents (nec)"', add
label define occ1950_lbl 301 `"Attendants and assistants, library"', add
label define occ1950_lbl 302 `"Attendants, physicians and dentists office"', add
label define occ1950_lbl 304 `"Baggagemen, transportation"', add
label define occ1950_lbl 305 `"Bank tellers"', add
label define occ1950_lbl 310 `"Bookkeepers"', add
label define occ1950_lbl 320 `"Cashiers"', add
label define occ1950_lbl 321 `"Collectors, bill and account"', add
label define occ1950_lbl 322 `"Dispatchers and starters, vehicle"', add
label define occ1950_lbl 325 `"Express messengers and railway mail clerks"', add
label define occ1950_lbl 335 `"Mail carriers"', add
label define occ1950_lbl 340 `"Messengers and office boys"', add
label define occ1950_lbl 341 `"Office machine operators"', add
label define occ1950_lbl 342 `"Shipping and receiving clerks"', add
label define occ1950_lbl 350 `"Stenographers, typists, and secretaries"', add
label define occ1950_lbl 360 `"Telegraph messengers"', add
label define occ1950_lbl 365 `"Telegraph operators"', add
label define occ1950_lbl 370 `"Telephone operators"', add
label define occ1950_lbl 380 `"Ticket, station, and express agents"', add
label define occ1950_lbl 390 `"Clerical and kindred workers (n.e.c.)"', add
label define occ1950_lbl 400 `"Advertising agents and salesmen"', add
label define occ1950_lbl 410 `"Auctioneers"', add
label define occ1950_lbl 420 `"Demonstrators"', add
label define occ1950_lbl 430 `"Hucksters and peddlers"', add
label define occ1950_lbl 450 `"Insurance agents and brokers"', add
label define occ1950_lbl 460 `"Newsboys"', add
label define occ1950_lbl 470 `"Real estate agents and brokers"', add
label define occ1950_lbl 480 `"Stock and bond salesmen"', add
label define occ1950_lbl 490 `"Salesmen and sales clerks (nec)"', add
label define occ1950_lbl 500 `"Bakers"', add
label define occ1950_lbl 501 `"Blacksmiths"', add
label define occ1950_lbl 502 `"Bookbinders"', add
label define occ1950_lbl 503 `"Boilermakers"', add
label define occ1950_lbl 504 `"Brickmasons,stonemasons, and tile setters"', add
label define occ1950_lbl 505 `"Cabinetmakers"', add
label define occ1950_lbl 510 `"Carpenters"', add
label define occ1950_lbl 511 `"Cement and concrete finishers"', add
label define occ1950_lbl 512 `"Compositors and typesetters"', add
label define occ1950_lbl 513 `"Cranemen,derrickmen, and hoistmen"', add
label define occ1950_lbl 514 `"Decorators and window dressers"', add
label define occ1950_lbl 515 `"Electricians"', add
label define occ1950_lbl 520 `"Electrotypers and stereotypers"', add
label define occ1950_lbl 521 `"Engravers, except photoengravers"', add
label define occ1950_lbl 522 `"Excavating, grading, and road machinery operators"', add
label define occ1950_lbl 523 `"Foremen (nec)"', add
label define occ1950_lbl 524 `"Forgemen and hammermen"', add
label define occ1950_lbl 525 `"Furriers"', add
label define occ1950_lbl 530 `"Glaziers"', add
label define occ1950_lbl 531 `"Heat treaters, annealers, temperers"', add
label define occ1950_lbl 532 `"Inspectors, scalers, and graders log and lumber"', add
label define occ1950_lbl 533 `"Inspectors (nec)"', add
label define occ1950_lbl 534 `"Jewelers, watchmakers, goldsmiths, and silversmiths"', add
label define occ1950_lbl 535 `"Job setters, metal"', add
label define occ1950_lbl 540 `"Linemen and servicemen, telegraph, telephone, and power"', add
label define occ1950_lbl 541 `"Locomotive engineers"', add
label define occ1950_lbl 542 `"Locomotive firemen"', add
label define occ1950_lbl 543 `"Loom fixers"', add
label define occ1950_lbl 544 `"Machinists"', add
label define occ1950_lbl 545 `"Airplane-mechanics and repairmen"', add
label define occ1950_lbl 550 `"Automobile-mechanics and repairmen"', add
label define occ1950_lbl 551 `"Office machine-mechanics and repairmen"', add
label define occ1950_lbl 552 `"Radio and television-mechanics and repairmen"', add
label define occ1950_lbl 553 `"Railroad and car shop-mechanics and repairmen"', add
label define occ1950_lbl 554 `"Mechanics and repairmen (nec)"', add
label define occ1950_lbl 555 `"Millers, grain, flour, feed, etc"', add
label define occ1950_lbl 560 `"Millwrights"', add
label define occ1950_lbl 561 `"Molders, metal"', add
label define occ1950_lbl 562 `"Motion picture projectionists"', add
label define occ1950_lbl 563 `"Opticians and lens grinders and polishers"', add
label define occ1950_lbl 564 `"Painters, construction and maintenance"', add
label define occ1950_lbl 565 `"Paperhangers"', add
label define occ1950_lbl 570 `"Pattern and model makers, except paper"', add
label define occ1950_lbl 571 `"Photoengravers and lithographers"', add
label define occ1950_lbl 572 `"Piano and organ tuners and repairmen"', add
label define occ1950_lbl 573 `"Plasterers"', add
label define occ1950_lbl 574 `"Plumbers and pipe fitters"', add
label define occ1950_lbl 575 `"Pressmen and plate printers, printing"', add
label define occ1950_lbl 580 `"Rollers and roll hands, metal"', add
label define occ1950_lbl 581 `"Roofers and slaters"', add
label define occ1950_lbl 582 `"Shoemakers and repairers, except factory"', add
label define occ1950_lbl 583 `"Stationary engineers"', add
label define occ1950_lbl 584 `"Stone cutters and stone carvers"', add
label define occ1950_lbl 585 `"Structural metal workers"', add
label define occ1950_lbl 590 `"Tailors and tailoresses"', add
label define occ1950_lbl 591 `"Tinsmiths, coppersmiths, and sheet metal workers"', add
label define occ1950_lbl 592 `"Tool makers, and die makers and setters"', add
label define occ1950_lbl 593 `"Upholsterers"', add
label define occ1950_lbl 594 `"Craftsmen and kindred workers (nec)"', add
label define occ1950_lbl 595 `"Members of the armed services"', add
label define occ1950_lbl 600 `"Auto mechanics apprentice"', add
label define occ1950_lbl 601 `"Bricklayers and masons apprentice"', add
label define occ1950_lbl 602 `"Carpenters apprentice"', add
label define occ1950_lbl 603 `"Electricians apprentice"', add
label define occ1950_lbl 604 `"Machinists and toolmakers apprentice"', add
label define occ1950_lbl 605 `"Mechanics, except auto apprentice"', add
label define occ1950_lbl 610 `"Plumbers and pipe fitters apprentice"', add
label define occ1950_lbl 611 `"Apprentices, building trades (nec)"', add
label define occ1950_lbl 612 `"Apprentices, metalworking trades (nec)"', add
label define occ1950_lbl 613 `"Apprentices, printing  trades"', add
label define occ1950_lbl 614 `"Apprentices, other specified trades"', add
label define occ1950_lbl 615 `"Apprentices, trade not specified"', add
label define occ1950_lbl 620 `"Asbestos and insulation workers"', add
label define occ1950_lbl 621 `"Attendants, auto service and parking"', add
label define occ1950_lbl 622 `"Blasters and powdermen"', add
label define occ1950_lbl 623 `"Boatmen, canalmen, and lock keepers"', add
label define occ1950_lbl 624 `"Brakemen, railroad"', add
label define occ1950_lbl 625 `"Bus drivers"', add
label define occ1950_lbl 630 `"Chainmen, rodmen, and axmen, surveying"', add
label define occ1950_lbl 631 `"Conductors, bus and street railway"', add
label define occ1950_lbl 632 `"Deliverymen and routemen"', add
label define occ1950_lbl 633 `"Dressmakers and seamstresses, except factory"', add
label define occ1950_lbl 634 `"Dyers"', add
label define occ1950_lbl 635 `"Filers, grinders, and polishers, metal"', add
label define occ1950_lbl 640 `"Fruit, nut, and vegetable graders, and packers, except facto"', add
label define occ1950_lbl 641 `"Furnacemen, smeltermen and pourers"', add
label define occ1950_lbl 642 `"Heaters, metal"', add
label define occ1950_lbl 643 `"Laundry and dry cleaning Operatives"', add
label define occ1950_lbl 644 `"Meat cutters, except slaughter and packing house"', add
label define occ1950_lbl 645 `"Milliners"', add
label define occ1950_lbl 650 `"Mine operatives and laborers"', add
label define occ1950_lbl 660 `"Motormen, mine, factory, logging camp, etc"', add
label define occ1950_lbl 661 `"Motormen, street, subway, and elevated railway"', add
label define occ1950_lbl 662 `"Oilers and greaser, except auto"', add
label define occ1950_lbl 670 `"Painters, except construction or maintenance"', add
label define occ1950_lbl 671 `"Photographic process workers"', add
label define occ1950_lbl 672 `"Power station operators"', add
label define occ1950_lbl 673 `"Sailors and deck hands"', add
label define occ1950_lbl 674 `"Sawyers"', add
label define occ1950_lbl 675 `"Spinners, textile"', add
label define occ1950_lbl 680 `"Stationary firemen"', add
label define occ1950_lbl 681 `"Switchmen, railroad"', add
label define occ1950_lbl 682 `"Taxicab drivers and chauffeurs"', add
label define occ1950_lbl 683 `"Truck and tractor drivers"', add
label define occ1950_lbl 684 `"Weavers, textile"', add
label define occ1950_lbl 685 `"Welders and flame cutters"', add
label define occ1950_lbl 690 `"Operative and kindred workers (nec)"', add
label define occ1950_lbl 700 `"Housekeepers, private household"', add
label define occ1950_lbl 710 `"Laundressses, private household"', add
label define occ1950_lbl 720 `"Private household workers (nec)"', add
label define occ1950_lbl 730 `"Attendants, hospital and other institution"', add
label define occ1950_lbl 731 `"Attendants, professional and personal service (nec)"', add
label define occ1950_lbl 732 `"Attendants, recreation and amusement"', add
label define occ1950_lbl 740 `"Barbers, beauticians, and manicurists"', add
label define occ1950_lbl 750 `"Bartenders"', add
label define occ1950_lbl 751 `"Bootblacks"', add
label define occ1950_lbl 752 `"Boarding and lodging house keepers"', add
label define occ1950_lbl 753 `"Charwomen and cleaners"', add
label define occ1950_lbl 754 `"Cooks, except private household"', add
label define occ1950_lbl 760 `"Counter and fountain workers"', add
label define occ1950_lbl 761 `"Elevator operators"', add
label define occ1950_lbl 762 `"Firemen, fire protection"', add
label define occ1950_lbl 763 `"Guards, watchmen, and doorkeepers"', add
label define occ1950_lbl 764 `"Housekeepers and stewards, except private household"', add
label define occ1950_lbl 770 `"Janitors and sextons"', add
label define occ1950_lbl 771 `"Marshals and constables"', add
label define occ1950_lbl 772 `"Midwives"', add
label define occ1950_lbl 773 `"Policemen and detectives"', add
label define occ1950_lbl 780 `"Porters"', add
label define occ1950_lbl 781 `"Practical nurses"', add
label define occ1950_lbl 782 `"Sheriffs and bailiffs"', add
label define occ1950_lbl 783 `"Ushers, recreation and amusement"', add
label define occ1950_lbl 784 `"Waiters and waitresses"', add
label define occ1950_lbl 785 `"Watchmen (crossing) and bridge tenders"', add
label define occ1950_lbl 790 `"Service workers, except private household (nec)"', add
label define occ1950_lbl 810 `"Farm foremen"', add
label define occ1950_lbl 820 `"Farm laborers, wage workers"', add
label define occ1950_lbl 830 `"Farm laborers, unpaid family workers"', add
label define occ1950_lbl 840 `"Farm service laborers, self-employed"', add
label define occ1950_lbl 910 `"Fishermen and oystermen"', add
label define occ1950_lbl 920 `"Garage laborers and car washers and greasers"', add
label define occ1950_lbl 930 `"Gardeners, except farm and groundskeepers"', add
label define occ1950_lbl 940 `"Longshoremen and stevedores"', add
label define occ1950_lbl 950 `"Lumbermen, raftsmen, and woodchoppers"', add
label define occ1950_lbl 960 `"Teamsters"', add
label define occ1950_lbl 970 `"Laborers (nec)"', add
label define occ1950_lbl 979 `"Not yet classified"', add
label define occ1950_lbl 980 `"Keeps house/housekeeping at home/housewife"', add
label define occ1950_lbl 981 `"Imputed keeping house (1850-1900)"', add
label define occ1950_lbl 982 `"Helping at home/helps parents/housework"', add
label define occ1950_lbl 983 `"At school/student"', add
label define occ1950_lbl 984 `"Retired"', add
label define occ1950_lbl 985 `"Unemployed/without occupation"', add
label define occ1950_lbl 986 `"Invalid/disabled w/ no occupation reported"', add
label define occ1950_lbl 987 `"Inmate"', add
label define occ1950_lbl 990 `"New Worker"', add
label define occ1950_lbl 991 `"Gentleman/lady/at leisure"', add
label define occ1950_lbl 995 `"Other non-occupation"', add
label define occ1950_lbl 997 `"Occupation missing/unknown"', add
label define occ1950_lbl 999 `"N/A (blank)"', add
label values occ1950 occ1950_lbl

label define ind1950_lbl 000 `"N/A or none reported"'
label define ind1950_lbl 105 `"Agriculture"', add
label define ind1950_lbl 116 `"Forestry"', add
label define ind1950_lbl 126 `"Fisheries"', add
label define ind1950_lbl 206 `"Metal mining"', add
label define ind1950_lbl 216 `"Coal mining"', add
label define ind1950_lbl 226 `"Crude petroleum and natural gas extraction"', add
label define ind1950_lbl 236 `"Nonmettalic  mining and quarrying, except fuel"', add
label define ind1950_lbl 239 `"Mining, not specified"', add
label define ind1950_lbl 246 `"Construction"', add
label define ind1950_lbl 306 `"Logging"', add
label define ind1950_lbl 307 `"Sawmills, planing mills, and mill work"', add
label define ind1950_lbl 308 `"Misc wood products"', add
label define ind1950_lbl 309 `"Furniture and fixtures"', add
label define ind1950_lbl 316 `"Glass and glass products"', add
label define ind1950_lbl 317 `"Cement, concrete, gypsum and plaster products"', add
label define ind1950_lbl 318 `"Structural clay products"', add
label define ind1950_lbl 319 `"Pottery and related prods"', add
label define ind1950_lbl 326 `"Misc nonmetallic mineral and stone products"', add
label define ind1950_lbl 336 `"Blast furnaces, steel works, and rolling mills"', add
label define ind1950_lbl 337 `"Other primary iron and steel industries"', add
label define ind1950_lbl 338 `"Primary nonferrous industries"', add
label define ind1950_lbl 346 `"Fabricated steel products"', add
label define ind1950_lbl 347 `"Fabricated nonferrous metal products"', add
label define ind1950_lbl 348 `"Not specified metal industries"', add
label define ind1950_lbl 356 `"Agricultural machinery and tractors"', add
label define ind1950_lbl 357 `"Office and store machines"', add
label define ind1950_lbl 358 `"Misc machinery"', add
label define ind1950_lbl 367 `"Electrical machinery, equipment and supplies"', add
label define ind1950_lbl 376 `"Motor vehicles and motor vehicle equipment"', add
label define ind1950_lbl 377 `"Aircraft and parts"', add
label define ind1950_lbl 378 `"Ship and boat building and repairing"', add
label define ind1950_lbl 379 `"Railroad and misc transportation equipment"', add
label define ind1950_lbl 386 `"Professional equipment"', add
label define ind1950_lbl 387 `"Photographic equipment and supplies"', add
label define ind1950_lbl 388 `"Watches, clocks, and clockwork-operated devices"', add
label define ind1950_lbl 399 `"Misc manufacturing industries"', add
label define ind1950_lbl 406 `"Meat products"', add
label define ind1950_lbl 407 `"Dairy products"', add
label define ind1950_lbl 408 `"Canning and preserving fruits, vegetables, and seafoods"', add
label define ind1950_lbl 409 `"Grain-mill products"', add
label define ind1950_lbl 416 `"Bakery products"', add
label define ind1950_lbl 417 `"Confectionery and related products"', add
label define ind1950_lbl 418 `"Beverage industries"', add
label define ind1950_lbl 419 `"Misc food preparations and kindred products"', add
label define ind1950_lbl 426 `"Not specified food industries"', add
label define ind1950_lbl 429 `"Tobacco manufactures"', add
label define ind1950_lbl 436 `"Knitting mills"', add
label define ind1950_lbl 437 `"Dyeing and finishing textiles, except knit goods"', add
label define ind1950_lbl 438 `"Carpets, rugs, and other floor coverings"', add
label define ind1950_lbl 439 `"Yarn, thread, and fabric"', add
label define ind1950_lbl 446 `"Misc textile mill products"', add
label define ind1950_lbl 448 `"Apparel and accessories"', add
label define ind1950_lbl 449 `"Misc fabricated textile products"', add
label define ind1950_lbl 456 `"Pulp, paper, and paper-board mills"', add
label define ind1950_lbl 457 `"Paperboard containers and boxes"', add
label define ind1950_lbl 458 `"Misc paper and pulp products"', add
label define ind1950_lbl 459 `"Printing, publishing, and allied industries"', add
label define ind1950_lbl 466 `"Synthetic fibers"', add
label define ind1950_lbl 467 `"Drugs and medicines"', add
label define ind1950_lbl 468 `"Paints, varnishes, and related products"', add
label define ind1950_lbl 469 `"Misc chemicals and allied products"', add
label define ind1950_lbl 476 `"Petroleum refining"', add
label define ind1950_lbl 477 `"Misc petroleum and coal products"', add
label define ind1950_lbl 478 `"Rubber products"', add
label define ind1950_lbl 487 `"Leather: tanned, curried, and finished"', add
label define ind1950_lbl 488 `"Footwear, except rubber"', add
label define ind1950_lbl 489 `"Leather products, except footwear"', add
label define ind1950_lbl 499 `"Not specified manufacturing industries"', add
label define ind1950_lbl 506 `"Railroads and railway"', add
label define ind1950_lbl 516 `"Street railways and bus lines"', add
label define ind1950_lbl 526 `"Trucking service"', add
label define ind1950_lbl 527 `"Warehousing and storage"', add
label define ind1950_lbl 536 `"Taxicab service"', add
label define ind1950_lbl 546 `"Water transportation"', add
label define ind1950_lbl 556 `"Air transportation"', add
label define ind1950_lbl 567 `"Petroleum and gasoline pipe lines"', add
label define ind1950_lbl 568 `"Services incidental to transportation"', add
label define ind1950_lbl 578 `"Telephone"', add
label define ind1950_lbl 579 `"Telegraph"', add
label define ind1950_lbl 586 `"Electric light and power"', add
label define ind1950_lbl 587 `"Gas and steam supply systems"', add
label define ind1950_lbl 588 `"Electric-gas utilities"', add
label define ind1950_lbl 596 `"Water supply"', add
label define ind1950_lbl 597 `"Sanitary services"', add
label define ind1950_lbl 598 `"Other and not specified utilities"', add
label define ind1950_lbl 606 `"Motor vehicles and equipment"', add
label define ind1950_lbl 607 `"Drugs, chemicals, and allied products"', add
label define ind1950_lbl 608 `"Dry goods apparel"', add
label define ind1950_lbl 609 `"Food and related products"', add
label define ind1950_lbl 616 `"Electrical goods, hardware, and plumbing equipment"', add
label define ind1950_lbl 617 `"Machinery, equipment, and supplies"', add
label define ind1950_lbl 618 `"Petroleum products"', add
label define ind1950_lbl 619 `"Farm prods--raw materials"', add
label define ind1950_lbl 626 `"Misc wholesale trade"', add
label define ind1950_lbl 627 `"Not specified wholesale trade"', add
label define ind1950_lbl 636 `"Food stores, except dairy"', add
label define ind1950_lbl 637 `"Dairy prods stores and milk retailing"', add
label define ind1950_lbl 646 `"General merchandise"', add
label define ind1950_lbl 647 `"Five and ten cent stores"', add
label define ind1950_lbl 656 `"Apparel and accessories stores, except shoe"', add
label define ind1950_lbl 657 `"Shoe stores"', add
label define ind1950_lbl 658 `"Furniture and house furnishings stores"', add
label define ind1950_lbl 659 `"Household appliance and radio stores"', add
label define ind1950_lbl 667 `"Motor vehicles and accessories retailing"', add
label define ind1950_lbl 668 `"Gasoline service stations"', add
label define ind1950_lbl 669 `"Drug stores"', add
label define ind1950_lbl 679 `"Eating and drinking  places"', add
label define ind1950_lbl 686 `"Hardware and farm implement stores"', add
label define ind1950_lbl 687 `"Lumber and building material retailing"', add
label define ind1950_lbl 688 `"Liquor stores"', add
label define ind1950_lbl 689 `"Retail florists"', add
label define ind1950_lbl 696 `"Jewelry stores"', add
label define ind1950_lbl 697 `"Fuel and ice retailing"', add
label define ind1950_lbl 698 `"Misc retail stores"', add
label define ind1950_lbl 699 `"Not specified retail trade"', add
label define ind1950_lbl 716 `"Banking and credit"', add
label define ind1950_lbl 726 `"Security and commodity brokerage and invest companies"', add
label define ind1950_lbl 736 `"Insurance"', add
label define ind1950_lbl 746 `"Real estate"', add
label define ind1950_lbl 756 `"Real estate-insurance-law  offices"', add
label define ind1950_lbl 806 `"Advertising"', add
label define ind1950_lbl 807 `"Accounting, auditing, and bookkeeping services"', add
label define ind1950_lbl 808 `"Misc business services"', add
label define ind1950_lbl 816 `"Auto repair services and garages"', add
label define ind1950_lbl 817 `"Misc repair services"', add
label define ind1950_lbl 826 `"Private households"', add
label define ind1950_lbl 836 `"Hotels and lodging places"', add
label define ind1950_lbl 846 `"Laundering, cleaning, and dyeing"', add
label define ind1950_lbl 847 `"Dressmaking shops"', add
label define ind1950_lbl 848 `"Shoe repair shops"', add
label define ind1950_lbl 849 `"Misc personal services"', add
label define ind1950_lbl 856 `"Radio broadcasting and television"', add
label define ind1950_lbl 857 `"Theaters and motion pictures"', add
label define ind1950_lbl 858 `"Bowling alleys, and billiard and pool parlors"', add
label define ind1950_lbl 859 `"Misc entertainment and recreation services"', add
label define ind1950_lbl 868 `"Medical and other health services, except hospitals"', add
label define ind1950_lbl 869 `"Hospitals"', add
label define ind1950_lbl 879 `"Legal services"', add
label define ind1950_lbl 888 `"Educational services"', add
label define ind1950_lbl 896 `"Welfare and religious services"', add
label define ind1950_lbl 897 `"Nonprofit membership organizs."', add
label define ind1950_lbl 898 `"Engineering and architectural services"', add
label define ind1950_lbl 899 `"Misc professional and related"', add
label define ind1950_lbl 906 `"Postal service"', add
label define ind1950_lbl 916 `"Federal public administration"', add
label define ind1950_lbl 926 `"State public administration"', add
label define ind1950_lbl 936 `"Local public administration"', add
label define ind1950_lbl 946 `"Public Administration, level not specified"', add
label define ind1950_lbl 976 `"Common or general laborer"', add
label define ind1950_lbl 979 `"Not yet specified"', add
label define ind1950_lbl 982 `"Housework at home"', add
label define ind1950_lbl 983 `"School response (students, etc.)"', add
label define ind1950_lbl 984 `"Retired"', add
label define ind1950_lbl 987 `"Institution response"', add
label define ind1950_lbl 991 `"Lady/Man of leisure"', add
label define ind1950_lbl 995 `"Non-industrial response"', add
label define ind1950_lbl 997 `"Nonclassifiable"', add
label define ind1950_lbl 998 `"Industry not reported"', add
label define ind1950_lbl 999 `"Blank or blank equivalent"', add
label values ind1950 ind1950_lbl

label define wkswork2_lbl 0 `"N/A"'
label define wkswork2_lbl 1 `"1-13 weeks"', add
label define wkswork2_lbl 2 `"14-26 weeks"', add
label define wkswork2_lbl 3 `"27-39 weeks"', add
label define wkswork2_lbl 4 `"40-47 weeks"', add
label define wkswork2_lbl 5 `"48-49 weeks"', add
label define wkswork2_lbl 6 `"50-52 weeks"', add
label values wkswork2 wkswork2_lbl

label define hrswork2_lbl 0 `"N/A"'
label define hrswork2_lbl 1 `"1-14 hours"', add
label define hrswork2_lbl 2 `"15-29 hours"', add
label define hrswork2_lbl 3 `"30-34 hours"', add
label define hrswork2_lbl 4 `"35-39 hours"', add
label define hrswork2_lbl 5 `"40 hours"', add
label define hrswork2_lbl 6 `"41-48 hours"', add
label define hrswork2_lbl 7 `"49-59 hours"', add
label define hrswork2_lbl 8 `"60+ hours"', add
label values hrswork2 hrswork2_lbl

label define uocc95_lbl 000 `"   Accountants and auditors"'
label define uocc95_lbl 001 `"   Actors and actresses"', add
label define uocc95_lbl 002 `"   Airplane pilots and navigators"', add
label define uocc95_lbl 003 `"   Architects"', add
label define uocc95_lbl 004 `"   Artists and art teachers"', add
label define uocc95_lbl 005 `"   Athletes"', add
label define uocc95_lbl 006 `"   Authors"', add
label define uocc95_lbl 007 `"   Chemists"', add
label define uocc95_lbl 008 `"   Chiropractors"', add
label define uocc95_lbl 009 `"   Clergymen"', add
label define uocc95_lbl 010 `"   College presidents and deans"', add
label define uocc95_lbl 012 `"      Agricultural sciences"', add
label define uocc95_lbl 013 `"      Biological sciences"', add
label define uocc95_lbl 014 `"      Chemistry"', add
label define uocc95_lbl 015 `"      Economics"', add
label define uocc95_lbl 016 `"      Engineering"', add
label define uocc95_lbl 017 `"      Geology and geophysics"', add
label define uocc95_lbl 018 `"      Mathematics"', add
label define uocc95_lbl 019 `"      Medical sciences"', add
label define uocc95_lbl 023 `"      Physics"', add
label define uocc95_lbl 024 `"      Psychology"', add
label define uocc95_lbl 025 `"      Statistics"', add
label define uocc95_lbl 026 `"      Natural science (n.e.c.)"', add
label define uocc95_lbl 027 `"      Social sciences (n.e.c.)"', add
label define uocc95_lbl 028 `"      Nonscientific subjects"', add
label define uocc95_lbl 029 `"      Professors and instructors, subject not specified"', add
label define uocc95_lbl 031 `"   Dancers and dancing teachers"', add
label define uocc95_lbl 032 `"   Dentists"', add
label define uocc95_lbl 033 `"   Designers"', add
label define uocc95_lbl 034 `"   Dieticians and nutritionists"', add
label define uocc95_lbl 035 `"   Draftsmen"', add
label define uocc95_lbl 036 `"   Editors and reporters"', add
label define uocc95_lbl 041 `"   Engineers, aeronautical"', add
label define uocc95_lbl 042 `"   Engineers, chemical"', add
label define uocc95_lbl 043 `"   Engineers, civil"', add
label define uocc95_lbl 044 `"   Engineers, electrical"', add
label define uocc95_lbl 045 `"   Engineers, industrial"', add
label define uocc95_lbl 046 `"   Engineers, mechanical"', add
label define uocc95_lbl 047 `"   Engineers, metallurgical, metallurgists"', add
label define uocc95_lbl 048 `"   Engineers, mining"', add
label define uocc95_lbl 049 `"   Engineers (n.e.c.)"', add
label define uocc95_lbl 051 `"   Entertainers (n.e.c.)"', add
label define uocc95_lbl 052 `"   Farm and home management advisors"', add
label define uocc95_lbl 053 `"   Foresters and conservationists"', add
label define uocc95_lbl 054 `"   Funeral directors and embalmers"', add
label define uocc95_lbl 055 `"   Lawyers and judges"', add
label define uocc95_lbl 056 `"   Librarians"', add
label define uocc95_lbl 057 `"   Musicians and music teachers"', add
label define uocc95_lbl 058 `"   Nurses, professional"', add
label define uocc95_lbl 059 `"   Nurses, student professional"', add
label define uocc95_lbl 061 `"   Agricultural scientists"', add
label define uocc95_lbl 062 `"   Biological scientists"', add
label define uocc95_lbl 063 `"   Geologists and geophysicists"', add
label define uocc95_lbl 067 `"   Mathematicians"', add
label define uocc95_lbl 068 `"   Physicists"', add
label define uocc95_lbl 069 `"   Miscellaneous natural scientists"', add
label define uocc95_lbl 070 `"   Optometrists"', add
label define uocc95_lbl 071 `"   Osteopaths"', add
label define uocc95_lbl 072 `"   Personnel and labor relations workers"', add
label define uocc95_lbl 073 `"   Pharmacists"', add
label define uocc95_lbl 074 `"   Photographers"', add
label define uocc95_lbl 075 `"   Physicians and surgeons"', add
label define uocc95_lbl 076 `"   Radio operators"', add
label define uocc95_lbl 077 `"   Recreation and group workers"', add
label define uocc95_lbl 078 `"   Religious workers"', add
label define uocc95_lbl 079 `"   Social and welfare workers, except group"', add
label define uocc95_lbl 081 `"   Economists"', add
label define uocc95_lbl 082 `"   Psychologists"', add
label define uocc95_lbl 083 `"   Statisticians and actuaries"', add
label define uocc95_lbl 084 `"   Miscellaneous social scientists"', add
label define uocc95_lbl 091 `"   Sports instructors and officials"', add
label define uocc95_lbl 092 `"   Surveyors"', add
label define uocc95_lbl 093 `"   Teachers (n.e.c.)"', add
label define uocc95_lbl 094 `"   Technicians, medical and dental"', add
label define uocc95_lbl 095 `"   Technicians, testing"', add
label define uocc95_lbl 096 `"   Technicians (n.e.c.)"', add
label define uocc95_lbl 097 `"   Therapists and healers (n.e.c.)"', add
label define uocc95_lbl 098 `"   Veterinarians"', add
label define uocc95_lbl 099 `"   Professional, technical and kindred workers (n.e.c.)"', add
label define uocc95_lbl 100 `"   Farmers (owners and tenants)"', add
label define uocc95_lbl 123 `"   Farm managers"', add
label define uocc95_lbl 200 `"   Buyers and department heads, store"', add
label define uocc95_lbl 201 `"   Buyers and shippers, farm products"', add
label define uocc95_lbl 203 `"   Conductors, railroad"', add
label define uocc95_lbl 204 `"   Credit men"', add
label define uocc95_lbl 205 `"   Floormen and floor managers, store"', add
label define uocc95_lbl 210 `"   Inspectors, public administration"', add
label define uocc95_lbl 230 `"   Managers and superintendents, building"', add
label define uocc95_lbl 240 `"   Officers, pilots, pursers and engineers, ship"', add
label define uocc95_lbl 250 `"   Officials and administrators (n.e.c.), public administration"', add
label define uocc95_lbl 260 `"   Officials, lodge, society, union, etc."', add
label define uocc95_lbl 270 `"   Postmasters"', add
label define uocc95_lbl 280 `"   Purchasing agents and buyers (n.e.c.)"', add
label define uocc95_lbl 290 `"   Managers, officials, and proprietors (n.e.c.)"', add
label define uocc95_lbl 300 `"   Agents (n.e.c.)"', add
label define uocc95_lbl 301 `"   Attendants and assistants, library"', add
label define uocc95_lbl 302 `"   Attendants, physician's and dentist's office"', add
label define uocc95_lbl 304 `"   Baggagemen, transportation"', add
label define uocc95_lbl 305 `"   Bank tellers"', add
label define uocc95_lbl 310 `"   Bookkeepers"', add
label define uocc95_lbl 320 `"   Cashiers"', add
label define uocc95_lbl 321 `"   Collectors, bill and account"', add
label define uocc95_lbl 322 `"   Dispatchers and starters, vehicle"', add
label define uocc95_lbl 325 `"   Express messengers and railway mail clerks"', add
label define uocc95_lbl 335 `"   Mail carriers"', add
label define uocc95_lbl 340 `"   Messengers and office boys"', add
label define uocc95_lbl 341 `"   Office machine operators"', add
label define uocc95_lbl 342 `"   Shipping and receiving clerks"', add
label define uocc95_lbl 350 `"   Stenographers, typists, and secretaries"', add
label define uocc95_lbl 360 `"   Telegraph messengers"', add
label define uocc95_lbl 365 `"   Telegraph operators"', add
label define uocc95_lbl 370 `"   Telephone operators"', add
label define uocc95_lbl 380 `"   Ticket, station, and express agents"', add
label define uocc95_lbl 390 `"   Clerical and kindred workers (n.e.c.)"', add
label define uocc95_lbl 400 `"   Advertising agents and salesmen"', add
label define uocc95_lbl 410 `"   Auctioneers"', add
label define uocc95_lbl 420 `"   Demonstrators"', add
label define uocc95_lbl 430 `"   Hucksters and peddlers"', add
label define uocc95_lbl 450 `"   Insurance agents and brokers"', add
label define uocc95_lbl 460 `"   Newsboys"', add
label define uocc95_lbl 470 `"   Real estate agents and brokers"', add
label define uocc95_lbl 480 `"   Stock and bond salesmen"', add
label define uocc95_lbl 490 `"   Salesmen and sales clerks (n.e.c.)"', add
label define uocc95_lbl 500 `"   Bakers"', add
label define uocc95_lbl 501 `"   Blacksmiths"', add
label define uocc95_lbl 502 `"   Bookbinders"', add
label define uocc95_lbl 503 `"   Boilermakers"', add
label define uocc95_lbl 504 `"   Brickmasons, stonemasons, and tile setters"', add
label define uocc95_lbl 505 `"   Cabinetmakers"', add
label define uocc95_lbl 510 `"   Carpenters"', add
label define uocc95_lbl 511 `"   Cement and concrete finishers"', add
label define uocc95_lbl 512 `"   Compositors and typesetters"', add
label define uocc95_lbl 513 `"   Cranemen, derrickmen, and hoistmen"', add
label define uocc95_lbl 514 `"   Decorators and window dressers"', add
label define uocc95_lbl 515 `"   Electricians"', add
label define uocc95_lbl 520 `"   Electrotypers and stereotypers"', add
label define uocc95_lbl 521 `"   Engravers, except photoengravers"', add
label define uocc95_lbl 522 `"   Excavating, grading, and road machinery operators"', add
label define uocc95_lbl 523 `"   Foremen (n.e.c.)"', add
label define uocc95_lbl 524 `"   Forgemen and hammermen"', add
label define uocc95_lbl 525 `"   Furriers"', add
label define uocc95_lbl 530 `"   Glaziers"', add
label define uocc95_lbl 531 `"   Heat treaters, annealers, temperers"', add
label define uocc95_lbl 532 `"   Inspectors, scalers, and graders, log and lumber"', add
label define uocc95_lbl 533 `"   Inspectors (n.e.c.)"', add
label define uocc95_lbl 534 `"   Jewelers, watchmakers, goldsmiths, and silversmiths"', add
label define uocc95_lbl 535 `"   Job setters, metal"', add
label define uocc95_lbl 540 `"   Linemen and servicemen, telegraph, telephone, and power"', add
label define uocc95_lbl 541 `"   Locomotive engineers"', add
label define uocc95_lbl 542 `"   Locomotive firemen"', add
label define uocc95_lbl 543 `"   Loom fixers"', add
label define uocc95_lbl 544 `"   Machinists"', add
label define uocc95_lbl 545 `"   Mechanics and repairmen, airplane"', add
label define uocc95_lbl 550 `"   Mechanics and repairmen, automobile"', add
label define uocc95_lbl 551 `"   Mechanics and repairmen, office machine"', add
label define uocc95_lbl 552 `"   Mechanics and repairmen, radio and television"', add
label define uocc95_lbl 553 `"   Mechanics and repairmen, railroad and car shop"', add
label define uocc95_lbl 554 `"   Mechanics and repairmen (n.e.c.)"', add
label define uocc95_lbl 555 `"   Millers, grain, flour, feed, etc."', add
label define uocc95_lbl 560 `"   Millwrights"', add
label define uocc95_lbl 561 `"   Molders, metal"', add
label define uocc95_lbl 562 `"   Motion picture projectionists"', add
label define uocc95_lbl 563 `"   Opticians and lens grinders and polishers"', add
label define uocc95_lbl 564 `"   Painters, construction and maintenance"', add
label define uocc95_lbl 565 `"   Paperhangers"', add
label define uocc95_lbl 570 `"   Pattern and model makers, except paper"', add
label define uocc95_lbl 571 `"   Photoengravers and lithographers"', add
label define uocc95_lbl 572 `"   Piano and organ tuners and repairmen"', add
label define uocc95_lbl 573 `"   Plasterers"', add
label define uocc95_lbl 574 `"   Plumbers and pipe fitters"', add
label define uocc95_lbl 575 `"   Pressmen and plate printers, printing"', add
label define uocc95_lbl 580 `"   Rollers and roll hands, metal"', add
label define uocc95_lbl 581 `"   Roofers and slaters"', add
label define uocc95_lbl 582 `"   Shoemakers and repairers, except factory"', add
label define uocc95_lbl 583 `"   Stationary engineers"', add
label define uocc95_lbl 584 `"   Stone cutters and stone carvers"', add
label define uocc95_lbl 585 `"   Structural metal workers"', add
label define uocc95_lbl 590 `"   Tailors and tailoresses"', add
label define uocc95_lbl 591 `"   Tinsmiths, coppersmiths, and sheet metal workers"', add
label define uocc95_lbl 592 `"   Tool makers, and die makers and setters"', add
label define uocc95_lbl 593 `"   Upholsterers"', add
label define uocc95_lbl 594 `"   Craftsmen and kindred workers (n.e.c.)"', add
label define uocc95_lbl 595 `"   Members of the armed services"', add
label define uocc95_lbl 600 `"   Apprentice auto mechanics"', add
label define uocc95_lbl 601 `"   Apprentice bricklayers and masons"', add
label define uocc95_lbl 602 `"   Apprentice carpenters"', add
label define uocc95_lbl 603 `"   Apprentice electricians"', add
label define uocc95_lbl 604 `"   Apprentice machinists and toolmakers"', add
label define uocc95_lbl 605 `"   Apprentice mechanics, except auto"', add
label define uocc95_lbl 610 `"   Apprentice plumbers and pipe fitters"', add
label define uocc95_lbl 611 `"   Apprentices, building trades (n.e.c.)"', add
label define uocc95_lbl 612 `"   Apprentices, metalworking trades (n.e.c.)"', add
label define uocc95_lbl 613 `"   Apprentices, printing trades"', add
label define uocc95_lbl 614 `"   Apprentices, other specified trades"', add
label define uocc95_lbl 615 `"   Apprentices, trade not specified"', add
label define uocc95_lbl 620 `"   Asbestos and insulation workers"', add
label define uocc95_lbl 621 `"   Attendants, auto service and parking"', add
label define uocc95_lbl 622 `"   Blasters and powdermen"', add
label define uocc95_lbl 623 `"   Boatmen, canalmen, and lock keepers"', add
label define uocc95_lbl 624 `"   Brakemen, railroad"', add
label define uocc95_lbl 625 `"   Bus drivers"', add
label define uocc95_lbl 630 `"   Chainmen, rodmen, and axmen, surveying"', add
label define uocc95_lbl 631 `"   Conductors, bus and street railway"', add
label define uocc95_lbl 632 `"   Deliverymen and routemen"', add
label define uocc95_lbl 633 `"   Dressmakers and seamstresses, except factory"', add
label define uocc95_lbl 634 `"   Dyers"', add
label define uocc95_lbl 635 `"   Filers, grinders, and polishers, metal"', add
label define uocc95_lbl 640 `"   Fruit, nut, and vegetable graders, and packers, except factory"', add
label define uocc95_lbl 641 `"   Furnacemen, smeltermen and pourers"', add
label define uocc95_lbl 642 `"   Heaters, metal"', add
label define uocc95_lbl 643 `"   Laundry and dry cleaning operatives"', add
label define uocc95_lbl 644 `"   Meat cutters, except slaughter and packing house"', add
label define uocc95_lbl 645 `"   Milliners"', add
label define uocc95_lbl 650 `"   Mine operatives and laborers"', add
label define uocc95_lbl 660 `"   Motormen, mine, factory, logging camp, etc."', add
label define uocc95_lbl 661 `"   Motormen, street, subway, and elevated railway"', add
label define uocc95_lbl 662 `"   Oilers and greaser, except auto"', add
label define uocc95_lbl 670 `"   Painters, except construction or maintenance"', add
label define uocc95_lbl 671 `"   Photographic process workers"', add
label define uocc95_lbl 672 `"   Power station operators"', add
label define uocc95_lbl 673 `"   Sailors and deck hands"', add
label define uocc95_lbl 674 `"   Sawyers"', add
label define uocc95_lbl 675 `"   Spinners, textile"', add
label define uocc95_lbl 680 `"   Stationary firemen"', add
label define uocc95_lbl 681 `"   Switchmen, railroad"', add
label define uocc95_lbl 682 `"   Taxicab drivers and chauffers"', add
label define uocc95_lbl 683 `"   Truck and tractor drivers"', add
label define uocc95_lbl 684 `"   Weavers, textile"', add
label define uocc95_lbl 685 `"   Welders and flame cutters"', add
label define uocc95_lbl 690 `"   Operative and kindred workers (n.e.c.)"', add
label define uocc95_lbl 700 `"   Housekeepers, private household"', add
label define uocc95_lbl 710 `"   Laundressses, private household"', add
label define uocc95_lbl 720 `"   Private household workers (n.e.c.)"', add
label define uocc95_lbl 730 `"   Attendants, hospital and other institution"', add
label define uocc95_lbl 731 `"   Attendants, professional and personal service (n.e.c.)"', add
label define uocc95_lbl 732 `"   Attendants, recreation and amusement"', add
label define uocc95_lbl 740 `"   Barbers, beauticians, and manicurists"', add
label define uocc95_lbl 750 `"   Bartenders"', add
label define uocc95_lbl 751 `"   Bootblacks"', add
label define uocc95_lbl 752 `"   Boarding and lodging house keepers"', add
label define uocc95_lbl 753 `"   Charwomen and cleaners"', add
label define uocc95_lbl 754 `"   Cooks, except private household"', add
label define uocc95_lbl 760 `"   Counter and fountain workers"', add
label define uocc95_lbl 761 `"   Elevator operators"', add
label define uocc95_lbl 762 `"   Firemen, fire protection"', add
label define uocc95_lbl 763 `"   Guards, watchmen, and doorkeepers"', add
label define uocc95_lbl 764 `"   Housekeepers and stewards, except private household"', add
label define uocc95_lbl 770 `"   Janitors and sextons"', add
label define uocc95_lbl 771 `"   Marshals and constables"', add
label define uocc95_lbl 772 `"   Midwives"', add
label define uocc95_lbl 773 `"   Policemen and detectives"', add
label define uocc95_lbl 780 `"   Porters"', add
label define uocc95_lbl 781 `"   Practical nurses"', add
label define uocc95_lbl 782 `"   Sheriffs and bailiffs"', add
label define uocc95_lbl 783 `"   Ushers, recreation and amusement"', add
label define uocc95_lbl 784 `"   Waiters and waitresses"', add
label define uocc95_lbl 785 `"   Watchmen (crossing) and bridge tenders"', add
label define uocc95_lbl 790 `"   Service workers, except private household (n.e.c.)"', add
label define uocc95_lbl 810 `"   Farm foremen"', add
label define uocc95_lbl 820 `"   Farm laborers, wage workers"', add
label define uocc95_lbl 830 `"   Farm laborers, unpaid family workers"', add
label define uocc95_lbl 840 `"   Farm service laborers, self-employed"', add
label define uocc95_lbl 910 `"   Fishermen and oystermen"', add
label define uocc95_lbl 920 `"   Garage laborers and car washers and greasers"', add
label define uocc95_lbl 930 `"   Gardeners, except farm, and groundskeepers"', add
label define uocc95_lbl 940 `"   Longshoremen and stevedores"', add
label define uocc95_lbl 950 `"   Lumbermen, raftsmen, and woodchoppers"', add
label define uocc95_lbl 960 `"   Teamsters"', add
label define uocc95_lbl 970 `"   Laborers (n.e.c.)"', add
label define uocc95_lbl 975 `"Employed, unclassifiable"', add
label define uocc95_lbl 980 `"   Keeps house/housekeeping at home/housewife"', add
label define uocc95_lbl 982 `"   Helping at home/helps parents/housework"', add
label define uocc95_lbl 983 `"   At school/student"', add
label define uocc95_lbl 984 `"   Retired"', add
label define uocc95_lbl 986 `"   Invalid/disabled w/ no occupation reported"', add
label define uocc95_lbl 987 `"   Inmate"', add
label define uocc95_lbl 995 `"   Other non-occupational response"', add
label define uocc95_lbl 997 `"Occupation missing/unknown"', add
label define uocc95_lbl 999 `"N/A (blank)"', add
label values uocc95 uocc95_lbl

label define incnonwg_lbl 0 `"N/A"'
label define incnonwg_lbl 1 `"Less than $50 nonwage, nonsalary income"', add
label define incnonwg_lbl 2 `"$50+ nonwage, nonsalary income"', add
label define incnonwg_lbl 9 `"Missing"', add
label values incnonwg incnonwg_lbl

label define migrate5_lbl 0 `"N/A"'
label define migrate5_lbl 1 `"Same house"', add
label define migrate5_lbl 2 `"Moved within state"', add
label define migrate5_lbl 3 `"Moved between states"', add
label define migrate5_lbl 4 `"Abroad five years ago"', add
label define migrate5_lbl 8 `"Moved (place not reported)"', add
label define migrate5_lbl 9 `"Unknown"', add
label values migrate5 migrate5_lbl

label define migrate5d_lbl 00 `"N/A"'
label define migrate5d_lbl 10 `"Same house"', add
label define migrate5d_lbl 20 `"Same state (migration status within state unknown)"', add
label define migrate5d_lbl 21 `"Different house, moved within county"', add
label define migrate5d_lbl 22 `"Different house, moved within state, between counties"', add
label define migrate5d_lbl 23 `"Different house, moved within state, within PUMA"', add
label define migrate5d_lbl 24 `"Different house, moved within state, between PUMAs"', add
label define migrate5d_lbl 25 `"Different house, unknown within state"', add
label define migrate5d_lbl 30 `"Different state (general)"', add
label define migrate5d_lbl 31 `"Moved between contiguous states"', add
label define migrate5d_lbl 32 `"Moved between non-contiguous states"', add
label define migrate5d_lbl 33 `"Unknown between states"', add
label define migrate5d_lbl 40 `"Abroad five years ago"', add
label define migrate5d_lbl 80 `"Moved, but place was not reported"', add
label define migrate5d_lbl 90 `"Unknown"', add
label values migrate5d migrate5d_lbl

label define educ_pop_lbl 00 `"N/A or no schooling"'
label define educ_pop_lbl 01 `"Nursery school to grade 4"', add
label define educ_pop_lbl 02 `"Grade 5, 6, 7, or 8"', add
label define educ_pop_lbl 03 `"Grade 9"', add
label define educ_pop_lbl 04 `"Grade 10"', add
label define educ_pop_lbl 05 `"Grade 11"', add
label define educ_pop_lbl 06 `"Grade 12"', add
label define educ_pop_lbl 07 `"1 year of college"', add
label define educ_pop_lbl 08 `"2 years of college"', add
label define educ_pop_lbl 09 `"3 years of college"', add
label define educ_pop_lbl 10 `"4 years of college"', add
label define educ_pop_lbl 11 `"5+ years of college"', add
label values educ_pop educ_pop_lbl

label define educd_pop_lbl 000 `"N/A or no schooling"'
label define educd_pop_lbl 001 `"N/A"', add
label define educd_pop_lbl 002 `"No schooling completed"', add
label define educd_pop_lbl 010 `"Nursery school to grade 4"', add
label define educd_pop_lbl 011 `"Nursery school, preschool"', add
label define educd_pop_lbl 012 `"Kindergarten"', add
label define educd_pop_lbl 013 `"Grade 1, 2, 3, or 4"', add
label define educd_pop_lbl 014 `"Grade 1"', add
label define educd_pop_lbl 015 `"Grade 2"', add
label define educd_pop_lbl 016 `"Grade 3"', add
label define educd_pop_lbl 017 `"Grade 4"', add
label define educd_pop_lbl 020 `"Grade 5, 6, 7, or 8"', add
label define educd_pop_lbl 021 `"Grade 5 or 6"', add
label define educd_pop_lbl 022 `"Grade 5"', add
label define educd_pop_lbl 023 `"Grade 6"', add
label define educd_pop_lbl 024 `"Grade 7 or 8"', add
label define educd_pop_lbl 025 `"Grade 7"', add
label define educd_pop_lbl 026 `"Grade 8"', add
label define educd_pop_lbl 030 `"Grade 9"', add
label define educd_pop_lbl 040 `"Grade 10"', add
label define educd_pop_lbl 050 `"Grade 11"', add
label define educd_pop_lbl 060 `"Grade 12"', add
label define educd_pop_lbl 061 `"12th grade, no diploma"', add
label define educd_pop_lbl 062 `"High school graduate or GED"', add
label define educd_pop_lbl 063 `"Regular high school diploma"', add
label define educd_pop_lbl 064 `"GED or alternative credential"', add
label define educd_pop_lbl 065 `"Some college, but less than 1 year"', add
label define educd_pop_lbl 070 `"1 year of college"', add
label define educd_pop_lbl 071 `"1 or more years of college credit, no degree"', add
label define educd_pop_lbl 080 `"2 years of college"', add
label define educd_pop_lbl 081 `"Associate's degree, type not specified"', add
label define educd_pop_lbl 082 `"Associate's degree, occupational program"', add
label define educd_pop_lbl 083 `"Associate's degree, academic program"', add
label define educd_pop_lbl 090 `"3 years of college"', add
label define educd_pop_lbl 100 `"4 years of college"', add
label define educd_pop_lbl 101 `"Bachelor's degree"', add
label define educd_pop_lbl 110 `"5+ years of college"', add
label define educd_pop_lbl 111 `"6 years of college (6+ in 1960-1970)"', add
label define educd_pop_lbl 112 `"7 years of college"', add
label define educd_pop_lbl 113 `"8+ years of college"', add
label define educd_pop_lbl 114 `"Master's degree"', add
label define educd_pop_lbl 115 `"Professional degree beyond a bachelor's degree"', add
label define educd_pop_lbl 116 `"Doctoral degree"', add
label define educd_pop_lbl 999 `"Missing"', add
label values educd_pop educd_pop_lbl

label define occ1950_pop_lbl 000 `"Accountants and auditors"'
label define occ1950_pop_lbl 001 `"Actors and actresses"', add
label define occ1950_pop_lbl 002 `"Airplane pilots and navigators"', add
label define occ1950_pop_lbl 003 `"Architects"', add
label define occ1950_pop_lbl 004 `"Artists and art teachers"', add
label define occ1950_pop_lbl 005 `"Athletes"', add
label define occ1950_pop_lbl 006 `"Authors"', add
label define occ1950_pop_lbl 007 `"Chemists"', add
label define occ1950_pop_lbl 008 `"Chiropractors"', add
label define occ1950_pop_lbl 009 `"Clergymen"', add
label define occ1950_pop_lbl 010 `"College presidents and deans"', add
label define occ1950_pop_lbl 012 `"Agricultural sciences-Professors and instructors"', add
label define occ1950_pop_lbl 013 `"Biological sciences-Professors and instructors"', add
label define occ1950_pop_lbl 014 `"Chemistry-Professors and instructors"', add
label define occ1950_pop_lbl 015 `"Economics-Professors and instructors"', add
label define occ1950_pop_lbl 016 `"Engineering-Professors and instructors"', add
label define occ1950_pop_lbl 017 `"Geology and geophysics-Professors and instructors"', add
label define occ1950_pop_lbl 018 `"Mathematics-Professors and instructors"', add
label define occ1950_pop_lbl 019 `"Medical Sciences-Professors and instructors"', add
label define occ1950_pop_lbl 023 `"Physics-Professors and instructors"', add
label define occ1950_pop_lbl 024 `"Psychology-Professors and instructors"', add
label define occ1950_pop_lbl 025 `"Statistics-Professors and instructors"', add
label define occ1950_pop_lbl 026 `"Natural science (nec)-Professors and instructors"', add
label define occ1950_pop_lbl 027 `"Social sciences (nec)-Professors and instructors"', add
label define occ1950_pop_lbl 028 `"Non-scientific subjects-Professors and instructors"', add
label define occ1950_pop_lbl 029 `"Subject not specified-Professors and instructors"', add
label define occ1950_pop_lbl 031 `"Dancers and dancing teachers"', add
label define occ1950_pop_lbl 032 `"Dentists"', add
label define occ1950_pop_lbl 033 `"Designers"', add
label define occ1950_pop_lbl 034 `"Dietitians and nutritionists"', add
label define occ1950_pop_lbl 035 `"Draftsmen"', add
label define occ1950_pop_lbl 036 `"Editors and reporters"', add
label define occ1950_pop_lbl 041 `"Aeronautical-Engineers"', add
label define occ1950_pop_lbl 042 `"Chemical-Engineers"', add
label define occ1950_pop_lbl 043 `"Civil-Engineers"', add
label define occ1950_pop_lbl 044 `"Electrical-Engineers"', add
label define occ1950_pop_lbl 045 `"Industrial-Engineers"', add
label define occ1950_pop_lbl 046 `"Mechanical-Engineers"', add
label define occ1950_pop_lbl 047 `"Metallurgical, metallurgists-Engineers"', add
label define occ1950_pop_lbl 048 `"Mining-Engineers"', add
label define occ1950_pop_lbl 049 `"Engineers (nec)"', add
label define occ1950_pop_lbl 051 `"Entertainers (nec)"', add
label define occ1950_pop_lbl 052 `"Farm and home management advisors"', add
label define occ1950_pop_lbl 053 `"Foresters and conservationists"', add
label define occ1950_pop_lbl 054 `"Funeral directors and embalmers"', add
label define occ1950_pop_lbl 055 `"Lawyers and judges"', add
label define occ1950_pop_lbl 056 `"Librarians"', add
label define occ1950_pop_lbl 057 `"Musicians and music teachers"', add
label define occ1950_pop_lbl 058 `"Nurses, professional"', add
label define occ1950_pop_lbl 059 `"Nurses, student professional"', add
label define occ1950_pop_lbl 061 `"Agricultural scientists"', add
label define occ1950_pop_lbl 062 `"Biological scientists"', add
label define occ1950_pop_lbl 063 `"Geologists and geophysicists"', add
label define occ1950_pop_lbl 067 `"Mathematicians"', add
label define occ1950_pop_lbl 068 `"Physicists"', add
label define occ1950_pop_lbl 069 `"Misc. natural scientists"', add
label define occ1950_pop_lbl 070 `"Optometrists"', add
label define occ1950_pop_lbl 071 `"Osteopaths"', add
label define occ1950_pop_lbl 072 `"Personnel and labor relations workers"', add
label define occ1950_pop_lbl 073 `"Pharmacists"', add
label define occ1950_pop_lbl 074 `"Photographers"', add
label define occ1950_pop_lbl 075 `"Physicians and surgeons"', add
label define occ1950_pop_lbl 076 `"Radio operators"', add
label define occ1950_pop_lbl 077 `"Recreation and group workers"', add
label define occ1950_pop_lbl 078 `"Religious workers"', add
label define occ1950_pop_lbl 079 `"Social and welfare workers, except group"', add
label define occ1950_pop_lbl 081 `"Economists"', add
label define occ1950_pop_lbl 082 `"Psychologists"', add
label define occ1950_pop_lbl 083 `"Statisticians and actuaries"', add
label define occ1950_pop_lbl 084 `"Misc social scientists"', add
label define occ1950_pop_lbl 091 `"Sports instructors and officials"', add
label define occ1950_pop_lbl 092 `"Surveyors"', add
label define occ1950_pop_lbl 093 `"Teachers (n.e.c.)"', add
label define occ1950_pop_lbl 094 `"Medical and dental-technicians"', add
label define occ1950_pop_lbl 095 `"Testing-technicians"', add
label define occ1950_pop_lbl 096 `"Technicians (nec)"', add
label define occ1950_pop_lbl 097 `"Therapists and healers (nec)"', add
label define occ1950_pop_lbl 098 `"Veterinarians"', add
label define occ1950_pop_lbl 099 `"Professional, technical and kindred workers (nec)"', add
label define occ1950_pop_lbl 100 `"Farmers (owners and tenants)"', add
label define occ1950_pop_lbl 123 `"Farm managers"', add
label define occ1950_pop_lbl 200 `"Buyers and dept heads, store"', add
label define occ1950_pop_lbl 201 `"Buyers and shippers, farm products"', add
label define occ1950_pop_lbl 203 `"Conductors, railroad"', add
label define occ1950_pop_lbl 204 `"Credit men"', add
label define occ1950_pop_lbl 205 `"Floormen and floor managers, store"', add
label define occ1950_pop_lbl 210 `"Inspectors, public administration"', add
label define occ1950_pop_lbl 230 `"Managers and superintendants, building"', add
label define occ1950_pop_lbl 240 `"Officers, pilots, pursers and engineers, ship"', add
label define occ1950_pop_lbl 250 `"Officials and administratators (nec), public administration"', add
label define occ1950_pop_lbl 260 `"Officials, lodge, society, union, etc."', add
label define occ1950_pop_lbl 270 `"Postmasters"', add
label define occ1950_pop_lbl 280 `"Purchasing agents and buyers (nec)"', add
label define occ1950_pop_lbl 290 `"Managers, officials, and proprietors (nec)"', add
label define occ1950_pop_lbl 300 `"Agents (nec)"', add
label define occ1950_pop_lbl 301 `"Attendants and assistants, library"', add
label define occ1950_pop_lbl 302 `"Attendants, physicians and dentists office"', add
label define occ1950_pop_lbl 304 `"Baggagemen, transportation"', add
label define occ1950_pop_lbl 305 `"Bank tellers"', add
label define occ1950_pop_lbl 310 `"Bookkeepers"', add
label define occ1950_pop_lbl 320 `"Cashiers"', add
label define occ1950_pop_lbl 321 `"Collectors, bill and account"', add
label define occ1950_pop_lbl 322 `"Dispatchers and starters, vehicle"', add
label define occ1950_pop_lbl 325 `"Express messengers and railway mail clerks"', add
label define occ1950_pop_lbl 335 `"Mail carriers"', add
label define occ1950_pop_lbl 340 `"Messengers and office boys"', add
label define occ1950_pop_lbl 341 `"Office machine operators"', add
label define occ1950_pop_lbl 342 `"Shipping and receiving clerks"', add
label define occ1950_pop_lbl 350 `"Stenographers, typists, and secretaries"', add
label define occ1950_pop_lbl 360 `"Telegraph messengers"', add
label define occ1950_pop_lbl 365 `"Telegraph operators"', add
label define occ1950_pop_lbl 370 `"Telephone operators"', add
label define occ1950_pop_lbl 380 `"Ticket, station, and express agents"', add
label define occ1950_pop_lbl 390 `"Clerical and kindred workers (n.e.c.)"', add
label define occ1950_pop_lbl 400 `"Advertising agents and salesmen"', add
label define occ1950_pop_lbl 410 `"Auctioneers"', add
label define occ1950_pop_lbl 420 `"Demonstrators"', add
label define occ1950_pop_lbl 430 `"Hucksters and peddlers"', add
label define occ1950_pop_lbl 450 `"Insurance agents and brokers"', add
label define occ1950_pop_lbl 460 `"Newsboys"', add
label define occ1950_pop_lbl 470 `"Real estate agents and brokers"', add
label define occ1950_pop_lbl 480 `"Stock and bond salesmen"', add
label define occ1950_pop_lbl 490 `"Salesmen and sales clerks (nec)"', add
label define occ1950_pop_lbl 500 `"Bakers"', add
label define occ1950_pop_lbl 501 `"Blacksmiths"', add
label define occ1950_pop_lbl 502 `"Bookbinders"', add
label define occ1950_pop_lbl 503 `"Boilermakers"', add
label define occ1950_pop_lbl 504 `"Brickmasons,stonemasons, and tile setters"', add
label define occ1950_pop_lbl 505 `"Cabinetmakers"', add
label define occ1950_pop_lbl 510 `"Carpenters"', add
label define occ1950_pop_lbl 511 `"Cement and concrete finishers"', add
label define occ1950_pop_lbl 512 `"Compositors and typesetters"', add
label define occ1950_pop_lbl 513 `"Cranemen,derrickmen, and hoistmen"', add
label define occ1950_pop_lbl 514 `"Decorators and window dressers"', add
label define occ1950_pop_lbl 515 `"Electricians"', add
label define occ1950_pop_lbl 520 `"Electrotypers and stereotypers"', add
label define occ1950_pop_lbl 521 `"Engravers, except photoengravers"', add
label define occ1950_pop_lbl 522 `"Excavating, grading, and road machinery operators"', add
label define occ1950_pop_lbl 523 `"Foremen (nec)"', add
label define occ1950_pop_lbl 524 `"Forgemen and hammermen"', add
label define occ1950_pop_lbl 525 `"Furriers"', add
label define occ1950_pop_lbl 530 `"Glaziers"', add
label define occ1950_pop_lbl 531 `"Heat treaters, annealers, temperers"', add
label define occ1950_pop_lbl 532 `"Inspectors, scalers, and graders log and lumber"', add
label define occ1950_pop_lbl 533 `"Inspectors (nec)"', add
label define occ1950_pop_lbl 534 `"Jewelers, watchmakers, goldsmiths, and silversmiths"', add
label define occ1950_pop_lbl 535 `"Job setters, metal"', add
label define occ1950_pop_lbl 540 `"Linemen and servicemen, telegraph, telephone, and power"', add
label define occ1950_pop_lbl 541 `"Locomotive engineers"', add
label define occ1950_pop_lbl 542 `"Locomotive firemen"', add
label define occ1950_pop_lbl 543 `"Loom fixers"', add
label define occ1950_pop_lbl 544 `"Machinists"', add
label define occ1950_pop_lbl 545 `"Airplane-mechanics and repairmen"', add
label define occ1950_pop_lbl 550 `"Automobile-mechanics and repairmen"', add
label define occ1950_pop_lbl 551 `"Office machine-mechanics and repairmen"', add
label define occ1950_pop_lbl 552 `"Radio and television-mechanics and repairmen"', add
label define occ1950_pop_lbl 553 `"Railroad and car shop-mechanics and repairmen"', add
label define occ1950_pop_lbl 554 `"Mechanics and repairmen (nec)"', add
label define occ1950_pop_lbl 555 `"Millers, grain, flour, feed, etc"', add
label define occ1950_pop_lbl 560 `"Millwrights"', add
label define occ1950_pop_lbl 561 `"Molders, metal"', add
label define occ1950_pop_lbl 562 `"Motion picture projectionists"', add
label define occ1950_pop_lbl 563 `"Opticians and lens grinders and polishers"', add
label define occ1950_pop_lbl 564 `"Painters, construction and maintenance"', add
label define occ1950_pop_lbl 565 `"Paperhangers"', add
label define occ1950_pop_lbl 570 `"Pattern and model makers, except paper"', add
label define occ1950_pop_lbl 571 `"Photoengravers and lithographers"', add
label define occ1950_pop_lbl 572 `"Piano and organ tuners and repairmen"', add
label define occ1950_pop_lbl 573 `"Plasterers"', add
label define occ1950_pop_lbl 574 `"Plumbers and pipe fitters"', add
label define occ1950_pop_lbl 575 `"Pressmen and plate printers, printing"', add
label define occ1950_pop_lbl 580 `"Rollers and roll hands, metal"', add
label define occ1950_pop_lbl 581 `"Roofers and slaters"', add
label define occ1950_pop_lbl 582 `"Shoemakers and repairers, except factory"', add
label define occ1950_pop_lbl 583 `"Stationary engineers"', add
label define occ1950_pop_lbl 584 `"Stone cutters and stone carvers"', add
label define occ1950_pop_lbl 585 `"Structural metal workers"', add
label define occ1950_pop_lbl 590 `"Tailors and tailoresses"', add
label define occ1950_pop_lbl 591 `"Tinsmiths, coppersmiths, and sheet metal workers"', add
label define occ1950_pop_lbl 592 `"Tool makers, and die makers and setters"', add
label define occ1950_pop_lbl 593 `"Upholsterers"', add
label define occ1950_pop_lbl 594 `"Craftsmen and kindred workers (nec)"', add
label define occ1950_pop_lbl 595 `"Members of the armed services"', add
label define occ1950_pop_lbl 600 `"Auto mechanics apprentice"', add
label define occ1950_pop_lbl 601 `"Bricklayers and masons apprentice"', add
label define occ1950_pop_lbl 602 `"Carpenters apprentice"', add
label define occ1950_pop_lbl 603 `"Electricians apprentice"', add
label define occ1950_pop_lbl 604 `"Machinists and toolmakers apprentice"', add
label define occ1950_pop_lbl 605 `"Mechanics, except auto apprentice"', add
label define occ1950_pop_lbl 610 `"Plumbers and pipe fitters apprentice"', add
label define occ1950_pop_lbl 611 `"Apprentices, building trades (nec)"', add
label define occ1950_pop_lbl 612 `"Apprentices, metalworking trades (nec)"', add
label define occ1950_pop_lbl 613 `"Apprentices, printing  trades"', add
label define occ1950_pop_lbl 614 `"Apprentices, other specified trades"', add
label define occ1950_pop_lbl 615 `"Apprentices, trade not specified"', add
label define occ1950_pop_lbl 620 `"Asbestos and insulation workers"', add
label define occ1950_pop_lbl 621 `"Attendants, auto service and parking"', add
label define occ1950_pop_lbl 622 `"Blasters and powdermen"', add
label define occ1950_pop_lbl 623 `"Boatmen, canalmen, and lock keepers"', add
label define occ1950_pop_lbl 624 `"Brakemen, railroad"', add
label define occ1950_pop_lbl 625 `"Bus drivers"', add
label define occ1950_pop_lbl 630 `"Chainmen, rodmen, and axmen, surveying"', add
label define occ1950_pop_lbl 631 `"Conductors, bus and street railway"', add
label define occ1950_pop_lbl 632 `"Deliverymen and routemen"', add
label define occ1950_pop_lbl 633 `"Dressmakers and seamstresses, except factory"', add
label define occ1950_pop_lbl 634 `"Dyers"', add
label define occ1950_pop_lbl 635 `"Filers, grinders, and polishers, metal"', add
label define occ1950_pop_lbl 640 `"Fruit, nut, and vegetable graders, and packers, except facto"', add
label define occ1950_pop_lbl 641 `"Furnacemen, smeltermen and pourers"', add
label define occ1950_pop_lbl 642 `"Heaters, metal"', add
label define occ1950_pop_lbl 643 `"Laundry and dry cleaning Operatives"', add
label define occ1950_pop_lbl 644 `"Meat cutters, except slaughter and packing house"', add
label define occ1950_pop_lbl 645 `"Milliners"', add
label define occ1950_pop_lbl 650 `"Mine operatives and laborers"', add
label define occ1950_pop_lbl 660 `"Motormen, mine, factory, logging camp, etc"', add
label define occ1950_pop_lbl 661 `"Motormen, street, subway, and elevated railway"', add
label define occ1950_pop_lbl 662 `"Oilers and greaser, except auto"', add
label define occ1950_pop_lbl 670 `"Painters, except construction or maintenance"', add
label define occ1950_pop_lbl 671 `"Photographic process workers"', add
label define occ1950_pop_lbl 672 `"Power station operators"', add
label define occ1950_pop_lbl 673 `"Sailors and deck hands"', add
label define occ1950_pop_lbl 674 `"Sawyers"', add
label define occ1950_pop_lbl 675 `"Spinners, textile"', add
label define occ1950_pop_lbl 680 `"Stationary firemen"', add
label define occ1950_pop_lbl 681 `"Switchmen, railroad"', add
label define occ1950_pop_lbl 682 `"Taxicab drivers and chauffeurs"', add
label define occ1950_pop_lbl 683 `"Truck and tractor drivers"', add
label define occ1950_pop_lbl 684 `"Weavers, textile"', add
label define occ1950_pop_lbl 685 `"Welders and flame cutters"', add
label define occ1950_pop_lbl 690 `"Operative and kindred workers (nec)"', add
label define occ1950_pop_lbl 700 `"Housekeepers, private household"', add
label define occ1950_pop_lbl 710 `"Laundressses, private household"', add
label define occ1950_pop_lbl 720 `"Private household workers (nec)"', add
label define occ1950_pop_lbl 730 `"Attendants, hospital and other institution"', add
label define occ1950_pop_lbl 731 `"Attendants, professional and personal service (nec)"', add
label define occ1950_pop_lbl 732 `"Attendants, recreation and amusement"', add
label define occ1950_pop_lbl 740 `"Barbers, beauticians, and manicurists"', add
label define occ1950_pop_lbl 750 `"Bartenders"', add
label define occ1950_pop_lbl 751 `"Bootblacks"', add
label define occ1950_pop_lbl 752 `"Boarding and lodging house keepers"', add
label define occ1950_pop_lbl 753 `"Charwomen and cleaners"', add
label define occ1950_pop_lbl 754 `"Cooks, except private household"', add
label define occ1950_pop_lbl 760 `"Counter and fountain workers"', add
label define occ1950_pop_lbl 761 `"Elevator operators"', add
label define occ1950_pop_lbl 762 `"Firemen, fire protection"', add
label define occ1950_pop_lbl 763 `"Guards, watchmen, and doorkeepers"', add
label define occ1950_pop_lbl 764 `"Housekeepers and stewards, except private household"', add
label define occ1950_pop_lbl 770 `"Janitors and sextons"', add
label define occ1950_pop_lbl 771 `"Marshals and constables"', add
label define occ1950_pop_lbl 772 `"Midwives"', add
label define occ1950_pop_lbl 773 `"Policemen and detectives"', add
label define occ1950_pop_lbl 780 `"Porters"', add
label define occ1950_pop_lbl 781 `"Practical nurses"', add
label define occ1950_pop_lbl 782 `"Sheriffs and bailiffs"', add
label define occ1950_pop_lbl 783 `"Ushers, recreation and amusement"', add
label define occ1950_pop_lbl 784 `"Waiters and waitresses"', add
label define occ1950_pop_lbl 785 `"Watchmen (crossing) and bridge tenders"', add
label define occ1950_pop_lbl 790 `"Service workers, except private household (nec)"', add
label define occ1950_pop_lbl 810 `"Farm foremen"', add
label define occ1950_pop_lbl 820 `"Farm laborers, wage workers"', add
label define occ1950_pop_lbl 830 `"Farm laborers, unpaid family workers"', add
label define occ1950_pop_lbl 840 `"Farm service laborers, self-employed"', add
label define occ1950_pop_lbl 910 `"Fishermen and oystermen"', add
label define occ1950_pop_lbl 920 `"Garage laborers and car washers and greasers"', add
label define occ1950_pop_lbl 930 `"Gardeners, except farm and groundskeepers"', add
label define occ1950_pop_lbl 940 `"Longshoremen and stevedores"', add
label define occ1950_pop_lbl 950 `"Lumbermen, raftsmen, and woodchoppers"', add
label define occ1950_pop_lbl 960 `"Teamsters"', add
label define occ1950_pop_lbl 970 `"Laborers (nec)"', add
label define occ1950_pop_lbl 979 `"Not yet classified"', add
label define occ1950_pop_lbl 980 `"Keeps house/housekeeping at home/housewife"', add
label define occ1950_pop_lbl 981 `"Imputed keeping house (1850-1900)"', add
label define occ1950_pop_lbl 982 `"Helping at home/helps parents/housework"', add
label define occ1950_pop_lbl 983 `"At school/student"', add
label define occ1950_pop_lbl 984 `"Retired"', add
label define occ1950_pop_lbl 985 `"Unemployed/without occupation"', add
label define occ1950_pop_lbl 986 `"Invalid/disabled w/ no occupation reported"', add
label define occ1950_pop_lbl 987 `"Inmate"', add
label define occ1950_pop_lbl 990 `"New Worker"', add
label define occ1950_pop_lbl 991 `"Gentleman/lady/at leisure"', add
label define occ1950_pop_lbl 995 `"Other non-occupation"', add
label define occ1950_pop_lbl 997 `"Occupation missing/unknown"', add
label define occ1950_pop_lbl 999 `"N/A (blank)"', add
label values occ1950_pop occ1950_pop_lbl

label define ind1950_pop_lbl 000 `"N/A or none reported"'
label define ind1950_pop_lbl 105 `"Agriculture"', add
label define ind1950_pop_lbl 116 `"Forestry"', add
label define ind1950_pop_lbl 126 `"Fisheries"', add
label define ind1950_pop_lbl 206 `"Metal mining"', add
label define ind1950_pop_lbl 216 `"Coal mining"', add
label define ind1950_pop_lbl 226 `"Crude petroleum and natural gas extraction"', add
label define ind1950_pop_lbl 236 `"Nonmettalic  mining and quarrying, except fuel"', add
label define ind1950_pop_lbl 239 `"Mining, not specified"', add
label define ind1950_pop_lbl 246 `"Construction"', add
label define ind1950_pop_lbl 306 `"Logging"', add
label define ind1950_pop_lbl 307 `"Sawmills, planing mills, and mill work"', add
label define ind1950_pop_lbl 308 `"Misc wood products"', add
label define ind1950_pop_lbl 309 `"Furniture and fixtures"', add
label define ind1950_pop_lbl 316 `"Glass and glass products"', add
label define ind1950_pop_lbl 317 `"Cement, concrete, gypsum and plaster products"', add
label define ind1950_pop_lbl 318 `"Structural clay products"', add
label define ind1950_pop_lbl 319 `"Pottery and related prods"', add
label define ind1950_pop_lbl 326 `"Misc nonmetallic mineral and stone products"', add
label define ind1950_pop_lbl 336 `"Blast furnaces, steel works, and rolling mills"', add
label define ind1950_pop_lbl 337 `"Other primary iron and steel industries"', add
label define ind1950_pop_lbl 338 `"Primary nonferrous industries"', add
label define ind1950_pop_lbl 346 `"Fabricated steel products"', add
label define ind1950_pop_lbl 347 `"Fabricated nonferrous metal products"', add
label define ind1950_pop_lbl 348 `"Not specified metal industries"', add
label define ind1950_pop_lbl 356 `"Agricultural machinery and tractors"', add
label define ind1950_pop_lbl 357 `"Office and store machines"', add
label define ind1950_pop_lbl 358 `"Misc machinery"', add
label define ind1950_pop_lbl 367 `"Electrical machinery, equipment and supplies"', add
label define ind1950_pop_lbl 376 `"Motor vehicles and motor vehicle equipment"', add
label define ind1950_pop_lbl 377 `"Aircraft and parts"', add
label define ind1950_pop_lbl 378 `"Ship and boat building and repairing"', add
label define ind1950_pop_lbl 379 `"Railroad and misc transportation equipment"', add
label define ind1950_pop_lbl 386 `"Professional equipment"', add
label define ind1950_pop_lbl 387 `"Photographic equipment and supplies"', add
label define ind1950_pop_lbl 388 `"Watches, clocks, and clockwork-operated devices"', add
label define ind1950_pop_lbl 399 `"Misc manufacturing industries"', add
label define ind1950_pop_lbl 406 `"Meat products"', add
label define ind1950_pop_lbl 407 `"Dairy products"', add
label define ind1950_pop_lbl 408 `"Canning and preserving fruits, vegetables, and seafoods"', add
label define ind1950_pop_lbl 409 `"Grain-mill products"', add
label define ind1950_pop_lbl 416 `"Bakery products"', add
label define ind1950_pop_lbl 417 `"Confectionery and related products"', add
label define ind1950_pop_lbl 418 `"Beverage industries"', add
label define ind1950_pop_lbl 419 `"Misc food preparations and kindred products"', add
label define ind1950_pop_lbl 426 `"Not specified food industries"', add
label define ind1950_pop_lbl 429 `"Tobacco manufactures"', add
label define ind1950_pop_lbl 436 `"Knitting mills"', add
label define ind1950_pop_lbl 437 `"Dyeing and finishing textiles, except knit goods"', add
label define ind1950_pop_lbl 438 `"Carpets, rugs, and other floor coverings"', add
label define ind1950_pop_lbl 439 `"Yarn, thread, and fabric"', add
label define ind1950_pop_lbl 446 `"Misc textile mill products"', add
label define ind1950_pop_lbl 448 `"Apparel and accessories"', add
label define ind1950_pop_lbl 449 `"Misc fabricated textile products"', add
label define ind1950_pop_lbl 456 `"Pulp, paper, and paper-board mills"', add
label define ind1950_pop_lbl 457 `"Paperboard containers and boxes"', add
label define ind1950_pop_lbl 458 `"Misc paper and pulp products"', add
label define ind1950_pop_lbl 459 `"Printing, publishing, and allied industries"', add
label define ind1950_pop_lbl 466 `"Synthetic fibers"', add
label define ind1950_pop_lbl 467 `"Drugs and medicines"', add
label define ind1950_pop_lbl 468 `"Paints, varnishes, and related products"', add
label define ind1950_pop_lbl 469 `"Misc chemicals and allied products"', add
label define ind1950_pop_lbl 476 `"Petroleum refining"', add
label define ind1950_pop_lbl 477 `"Misc petroleum and coal products"', add
label define ind1950_pop_lbl 478 `"Rubber products"', add
label define ind1950_pop_lbl 487 `"Leather: tanned, curried, and finished"', add
label define ind1950_pop_lbl 488 `"Footwear, except rubber"', add
label define ind1950_pop_lbl 489 `"Leather products, except footwear"', add
label define ind1950_pop_lbl 499 `"Not specified manufacturing industries"', add
label define ind1950_pop_lbl 506 `"Railroads and railway"', add
label define ind1950_pop_lbl 516 `"Street railways and bus lines"', add
label define ind1950_pop_lbl 526 `"Trucking service"', add
label define ind1950_pop_lbl 527 `"Warehousing and storage"', add
label define ind1950_pop_lbl 536 `"Taxicab service"', add
label define ind1950_pop_lbl 546 `"Water transportation"', add
label define ind1950_pop_lbl 556 `"Air transportation"', add
label define ind1950_pop_lbl 567 `"Petroleum and gasoline pipe lines"', add
label define ind1950_pop_lbl 568 `"Services incidental to transportation"', add
label define ind1950_pop_lbl 578 `"Telephone"', add
label define ind1950_pop_lbl 579 `"Telegraph"', add
label define ind1950_pop_lbl 586 `"Electric light and power"', add
label define ind1950_pop_lbl 587 `"Gas and steam supply systems"', add
label define ind1950_pop_lbl 588 `"Electric-gas utilities"', add
label define ind1950_pop_lbl 596 `"Water supply"', add
label define ind1950_pop_lbl 597 `"Sanitary services"', add
label define ind1950_pop_lbl 598 `"Other and not specified utilities"', add
label define ind1950_pop_lbl 606 `"Motor vehicles and equipment"', add
label define ind1950_pop_lbl 607 `"Drugs, chemicals, and allied products"', add
label define ind1950_pop_lbl 608 `"Dry goods apparel"', add
label define ind1950_pop_lbl 609 `"Food and related products"', add
label define ind1950_pop_lbl 616 `"Electrical goods, hardware, and plumbing equipment"', add
label define ind1950_pop_lbl 617 `"Machinery, equipment, and supplies"', add
label define ind1950_pop_lbl 618 `"Petroleum products"', add
label define ind1950_pop_lbl 619 `"Farm prods--raw materials"', add
label define ind1950_pop_lbl 626 `"Misc wholesale trade"', add
label define ind1950_pop_lbl 627 `"Not specified wholesale trade"', add
label define ind1950_pop_lbl 636 `"Food stores, except dairy"', add
label define ind1950_pop_lbl 637 `"Dairy prods stores and milk retailing"', add
label define ind1950_pop_lbl 646 `"General merchandise"', add
label define ind1950_pop_lbl 647 `"Five and ten cent stores"', add
label define ind1950_pop_lbl 656 `"Apparel and accessories stores, except shoe"', add
label define ind1950_pop_lbl 657 `"Shoe stores"', add
label define ind1950_pop_lbl 658 `"Furniture and house furnishings stores"', add
label define ind1950_pop_lbl 659 `"Household appliance and radio stores"', add
label define ind1950_pop_lbl 667 `"Motor vehicles and accessories retailing"', add
label define ind1950_pop_lbl 668 `"Gasoline service stations"', add
label define ind1950_pop_lbl 669 `"Drug stores"', add
label define ind1950_pop_lbl 679 `"Eating and drinking  places"', add
label define ind1950_pop_lbl 686 `"Hardware and farm implement stores"', add
label define ind1950_pop_lbl 687 `"Lumber and building material retailing"', add
label define ind1950_pop_lbl 688 `"Liquor stores"', add
label define ind1950_pop_lbl 689 `"Retail florists"', add
label define ind1950_pop_lbl 696 `"Jewelry stores"', add
label define ind1950_pop_lbl 697 `"Fuel and ice retailing"', add
label define ind1950_pop_lbl 698 `"Misc retail stores"', add
label define ind1950_pop_lbl 699 `"Not specified retail trade"', add
label define ind1950_pop_lbl 716 `"Banking and credit"', add
label define ind1950_pop_lbl 726 `"Security and commodity brokerage and invest companies"', add
label define ind1950_pop_lbl 736 `"Insurance"', add
label define ind1950_pop_lbl 746 `"Real estate"', add
label define ind1950_pop_lbl 756 `"Real estate-insurance-law  offices"', add
label define ind1950_pop_lbl 806 `"Advertising"', add
label define ind1950_pop_lbl 807 `"Accounting, auditing, and bookkeeping services"', add
label define ind1950_pop_lbl 808 `"Misc business services"', add
label define ind1950_pop_lbl 816 `"Auto repair services and garages"', add
label define ind1950_pop_lbl 817 `"Misc repair services"', add
label define ind1950_pop_lbl 826 `"Private households"', add
label define ind1950_pop_lbl 836 `"Hotels and lodging places"', add
label define ind1950_pop_lbl 846 `"Laundering, cleaning, and dyeing"', add
label define ind1950_pop_lbl 847 `"Dressmaking shops"', add
label define ind1950_pop_lbl 848 `"Shoe repair shops"', add
label define ind1950_pop_lbl 849 `"Misc personal services"', add
label define ind1950_pop_lbl 856 `"Radio broadcasting and television"', add
label define ind1950_pop_lbl 857 `"Theaters and motion pictures"', add
label define ind1950_pop_lbl 858 `"Bowling alleys, and billiard and pool parlors"', add
label define ind1950_pop_lbl 859 `"Misc entertainment and recreation services"', add
label define ind1950_pop_lbl 868 `"Medical and other health services, except hospitals"', add
label define ind1950_pop_lbl 869 `"Hospitals"', add
label define ind1950_pop_lbl 879 `"Legal services"', add
label define ind1950_pop_lbl 888 `"Educational services"', add
label define ind1950_pop_lbl 896 `"Welfare and religious services"', add
label define ind1950_pop_lbl 897 `"Nonprofit membership organizs."', add
label define ind1950_pop_lbl 898 `"Engineering and architectural services"', add
label define ind1950_pop_lbl 899 `"Misc professional and related"', add
label define ind1950_pop_lbl 906 `"Postal service"', add
label define ind1950_pop_lbl 916 `"Federal public administration"', add
label define ind1950_pop_lbl 926 `"State public administration"', add
label define ind1950_pop_lbl 936 `"Local public administration"', add
label define ind1950_pop_lbl 946 `"Public Administration, level not specified"', add
label define ind1950_pop_lbl 976 `"Common or general laborer"', add
label define ind1950_pop_lbl 979 `"Not yet specified"', add
label define ind1950_pop_lbl 982 `"Housework at home"', add
label define ind1950_pop_lbl 983 `"School response (students, etc.)"', add
label define ind1950_pop_lbl 984 `"Retired"', add
label define ind1950_pop_lbl 987 `"Institution response"', add
label define ind1950_pop_lbl 991 `"Lady/Man of leisure"', add
label define ind1950_pop_lbl 995 `"Non-industrial response"', add
label define ind1950_pop_lbl 997 `"Nonclassifiable"', add
label define ind1950_pop_lbl 998 `"Industry not reported"', add
label define ind1950_pop_lbl 999 `"Blank or blank equivalent"', add
label values ind1950_pop ind1950_pop_lbl

label define wkswork2_pop_lbl 0 `"N/A"'
label define wkswork2_pop_lbl 1 `"1-13 weeks"', add
label define wkswork2_pop_lbl 2 `"14-26 weeks"', add
label define wkswork2_pop_lbl 3 `"27-39 weeks"', add
label define wkswork2_pop_lbl 4 `"40-47 weeks"', add
label define wkswork2_pop_lbl 5 `"48-49 weeks"', add
label define wkswork2_pop_lbl 6 `"50-52 weeks"', add
label values wkswork2_pop wkswork2_pop_lbl

label define hrswork2_pop_lbl 0 `"N/A"'
label define hrswork2_pop_lbl 1 `"1-14 hours"', add
label define hrswork2_pop_lbl 2 `"15-29 hours"', add
label define hrswork2_pop_lbl 3 `"30-34 hours"', add
label define hrswork2_pop_lbl 4 `"35-39 hours"', add
label define hrswork2_pop_lbl 5 `"40 hours"', add
label define hrswork2_pop_lbl 6 `"41-48 hours"', add
label define hrswork2_pop_lbl 7 `"49-59 hours"', add
label define hrswork2_pop_lbl 8 `"60+ hours"', add
label values hrswork2_pop hrswork2_pop_lbl

label define uocc95_pop_lbl 000 `"   Accountants and auditors"'
label define uocc95_pop_lbl 001 `"   Actors and actresses"', add
label define uocc95_pop_lbl 002 `"   Airplane pilots and navigators"', add
label define uocc95_pop_lbl 003 `"   Architects"', add
label define uocc95_pop_lbl 004 `"   Artists and art teachers"', add
label define uocc95_pop_lbl 005 `"   Athletes"', add
label define uocc95_pop_lbl 006 `"   Authors"', add
label define uocc95_pop_lbl 007 `"   Chemists"', add
label define uocc95_pop_lbl 008 `"   Chiropractors"', add
label define uocc95_pop_lbl 009 `"   Clergymen"', add
label define uocc95_pop_lbl 010 `"   College presidents and deans"', add
label define uocc95_pop_lbl 012 `"      Agricultural sciences"', add
label define uocc95_pop_lbl 013 `"      Biological sciences"', add
label define uocc95_pop_lbl 014 `"      Chemistry"', add
label define uocc95_pop_lbl 015 `"      Economics"', add
label define uocc95_pop_lbl 016 `"      Engineering"', add
label define uocc95_pop_lbl 017 `"      Geology and geophysics"', add
label define uocc95_pop_lbl 018 `"      Mathematics"', add
label define uocc95_pop_lbl 019 `"      Medical sciences"', add
label define uocc95_pop_lbl 023 `"      Physics"', add
label define uocc95_pop_lbl 024 `"      Psychology"', add
label define uocc95_pop_lbl 025 `"      Statistics"', add
label define uocc95_pop_lbl 026 `"      Natural science (n.e.c.)"', add
label define uocc95_pop_lbl 027 `"      Social sciences (n.e.c.)"', add
label define uocc95_pop_lbl 028 `"      Nonscientific subjects"', add
label define uocc95_pop_lbl 029 `"      Professors and instructors, subject not specified"', add
label define uocc95_pop_lbl 031 `"   Dancers and dancing teachers"', add
label define uocc95_pop_lbl 032 `"   Dentists"', add
label define uocc95_pop_lbl 033 `"   Designers"', add
label define uocc95_pop_lbl 034 `"   Dieticians and nutritionists"', add
label define uocc95_pop_lbl 035 `"   Draftsmen"', add
label define uocc95_pop_lbl 036 `"   Editors and reporters"', add
label define uocc95_pop_lbl 041 `"   Engineers, aeronautical"', add
label define uocc95_pop_lbl 042 `"   Engineers, chemical"', add
label define uocc95_pop_lbl 043 `"   Engineers, civil"', add
label define uocc95_pop_lbl 044 `"   Engineers, electrical"', add
label define uocc95_pop_lbl 045 `"   Engineers, industrial"', add
label define uocc95_pop_lbl 046 `"   Engineers, mechanical"', add
label define uocc95_pop_lbl 047 `"   Engineers, metallurgical, metallurgists"', add
label define uocc95_pop_lbl 048 `"   Engineers, mining"', add
label define uocc95_pop_lbl 049 `"   Engineers (n.e.c.)"', add
label define uocc95_pop_lbl 051 `"   Entertainers (n.e.c.)"', add
label define uocc95_pop_lbl 052 `"   Farm and home management advisors"', add
label define uocc95_pop_lbl 053 `"   Foresters and conservationists"', add
label define uocc95_pop_lbl 054 `"   Funeral directors and embalmers"', add
label define uocc95_pop_lbl 055 `"   Lawyers and judges"', add
label define uocc95_pop_lbl 056 `"   Librarians"', add
label define uocc95_pop_lbl 057 `"   Musicians and music teachers"', add
label define uocc95_pop_lbl 058 `"   Nurses, professional"', add
label define uocc95_pop_lbl 059 `"   Nurses, student professional"', add
label define uocc95_pop_lbl 061 `"   Agricultural scientists"', add
label define uocc95_pop_lbl 062 `"   Biological scientists"', add
label define uocc95_pop_lbl 063 `"   Geologists and geophysicists"', add
label define uocc95_pop_lbl 067 `"   Mathematicians"', add
label define uocc95_pop_lbl 068 `"   Physicists"', add
label define uocc95_pop_lbl 069 `"   Miscellaneous natural scientists"', add
label define uocc95_pop_lbl 070 `"   Optometrists"', add
label define uocc95_pop_lbl 071 `"   Osteopaths"', add
label define uocc95_pop_lbl 072 `"   Personnel and labor relations workers"', add
label define uocc95_pop_lbl 073 `"   Pharmacists"', add
label define uocc95_pop_lbl 074 `"   Photographers"', add
label define uocc95_pop_lbl 075 `"   Physicians and surgeons"', add
label define uocc95_pop_lbl 076 `"   Radio operators"', add
label define uocc95_pop_lbl 077 `"   Recreation and group workers"', add
label define uocc95_pop_lbl 078 `"   Religious workers"', add
label define uocc95_pop_lbl 079 `"   Social and welfare workers, except group"', add
label define uocc95_pop_lbl 081 `"   Economists"', add
label define uocc95_pop_lbl 082 `"   Psychologists"', add
label define uocc95_pop_lbl 083 `"   Statisticians and actuaries"', add
label define uocc95_pop_lbl 084 `"   Miscellaneous social scientists"', add
label define uocc95_pop_lbl 091 `"   Sports instructors and officials"', add
label define uocc95_pop_lbl 092 `"   Surveyors"', add
label define uocc95_pop_lbl 093 `"   Teachers (n.e.c.)"', add
label define uocc95_pop_lbl 094 `"   Technicians, medical and dental"', add
label define uocc95_pop_lbl 095 `"   Technicians, testing"', add
label define uocc95_pop_lbl 096 `"   Technicians (n.e.c.)"', add
label define uocc95_pop_lbl 097 `"   Therapists and healers (n.e.c.)"', add
label define uocc95_pop_lbl 098 `"   Veterinarians"', add
label define uocc95_pop_lbl 099 `"   Professional, technical and kindred workers (n.e.c.)"', add
label define uocc95_pop_lbl 100 `"   Farmers (owners and tenants)"', add
label define uocc95_pop_lbl 123 `"   Farm managers"', add
label define uocc95_pop_lbl 200 `"   Buyers and department heads, store"', add
label define uocc95_pop_lbl 201 `"   Buyers and shippers, farm products"', add
label define uocc95_pop_lbl 203 `"   Conductors, railroad"', add
label define uocc95_pop_lbl 204 `"   Credit men"', add
label define uocc95_pop_lbl 205 `"   Floormen and floor managers, store"', add
label define uocc95_pop_lbl 210 `"   Inspectors, public administration"', add
label define uocc95_pop_lbl 230 `"   Managers and superintendents, building"', add
label define uocc95_pop_lbl 240 `"   Officers, pilots, pursers and engineers, ship"', add
label define uocc95_pop_lbl 250 `"   Officials and administrators (n.e.c.), public administration"', add
label define uocc95_pop_lbl 260 `"   Officials, lodge, society, union, etc."', add
label define uocc95_pop_lbl 270 `"   Postmasters"', add
label define uocc95_pop_lbl 280 `"   Purchasing agents and buyers (n.e.c.)"', add
label define uocc95_pop_lbl 290 `"   Managers, officials, and proprietors (n.e.c.)"', add
label define uocc95_pop_lbl 300 `"   Agents (n.e.c.)"', add
label define uocc95_pop_lbl 301 `"   Attendants and assistants, library"', add
label define uocc95_pop_lbl 302 `"   Attendants, physician's and dentist's office"', add
label define uocc95_pop_lbl 304 `"   Baggagemen, transportation"', add
label define uocc95_pop_lbl 305 `"   Bank tellers"', add
label define uocc95_pop_lbl 310 `"   Bookkeepers"', add
label define uocc95_pop_lbl 320 `"   Cashiers"', add
label define uocc95_pop_lbl 321 `"   Collectors, bill and account"', add
label define uocc95_pop_lbl 322 `"   Dispatchers and starters, vehicle"', add
label define uocc95_pop_lbl 325 `"   Express messengers and railway mail clerks"', add
label define uocc95_pop_lbl 335 `"   Mail carriers"', add
label define uocc95_pop_lbl 340 `"   Messengers and office boys"', add
label define uocc95_pop_lbl 341 `"   Office machine operators"', add
label define uocc95_pop_lbl 342 `"   Shipping and receiving clerks"', add
label define uocc95_pop_lbl 350 `"   Stenographers, typists, and secretaries"', add
label define uocc95_pop_lbl 360 `"   Telegraph messengers"', add
label define uocc95_pop_lbl 365 `"   Telegraph operators"', add
label define uocc95_pop_lbl 370 `"   Telephone operators"', add
label define uocc95_pop_lbl 380 `"   Ticket, station, and express agents"', add
label define uocc95_pop_lbl 390 `"   Clerical and kindred workers (n.e.c.)"', add
label define uocc95_pop_lbl 400 `"   Advertising agents and salesmen"', add
label define uocc95_pop_lbl 410 `"   Auctioneers"', add
label define uocc95_pop_lbl 420 `"   Demonstrators"', add
label define uocc95_pop_lbl 430 `"   Hucksters and peddlers"', add
label define uocc95_pop_lbl 450 `"   Insurance agents and brokers"', add
label define uocc95_pop_lbl 460 `"   Newsboys"', add
label define uocc95_pop_lbl 470 `"   Real estate agents and brokers"', add
label define uocc95_pop_lbl 480 `"   Stock and bond salesmen"', add
label define uocc95_pop_lbl 490 `"   Salesmen and sales clerks (n.e.c.)"', add
label define uocc95_pop_lbl 500 `"   Bakers"', add
label define uocc95_pop_lbl 501 `"   Blacksmiths"', add
label define uocc95_pop_lbl 502 `"   Bookbinders"', add
label define uocc95_pop_lbl 503 `"   Boilermakers"', add
label define uocc95_pop_lbl 504 `"   Brickmasons, stonemasons, and tile setters"', add
label define uocc95_pop_lbl 505 `"   Cabinetmakers"', add
label define uocc95_pop_lbl 510 `"   Carpenters"', add
label define uocc95_pop_lbl 511 `"   Cement and concrete finishers"', add
label define uocc95_pop_lbl 512 `"   Compositors and typesetters"', add
label define uocc95_pop_lbl 513 `"   Cranemen, derrickmen, and hoistmen"', add
label define uocc95_pop_lbl 514 `"   Decorators and window dressers"', add
label define uocc95_pop_lbl 515 `"   Electricians"', add
label define uocc95_pop_lbl 520 `"   Electrotypers and stereotypers"', add
label define uocc95_pop_lbl 521 `"   Engravers, except photoengravers"', add
label define uocc95_pop_lbl 522 `"   Excavating, grading, and road machinery operators"', add
label define uocc95_pop_lbl 523 `"   Foremen (n.e.c.)"', add
label define uocc95_pop_lbl 524 `"   Forgemen and hammermen"', add
label define uocc95_pop_lbl 525 `"   Furriers"', add
label define uocc95_pop_lbl 530 `"   Glaziers"', add
label define uocc95_pop_lbl 531 `"   Heat treaters, annealers, temperers"', add
label define uocc95_pop_lbl 532 `"   Inspectors, scalers, and graders, log and lumber"', add
label define uocc95_pop_lbl 533 `"   Inspectors (n.e.c.)"', add
label define uocc95_pop_lbl 534 `"   Jewelers, watchmakers, goldsmiths, and silversmiths"', add
label define uocc95_pop_lbl 535 `"   Job setters, metal"', add
label define uocc95_pop_lbl 540 `"   Linemen and servicemen, telegraph, telephone, and power"', add
label define uocc95_pop_lbl 541 `"   Locomotive engineers"', add
label define uocc95_pop_lbl 542 `"   Locomotive firemen"', add
label define uocc95_pop_lbl 543 `"   Loom fixers"', add
label define uocc95_pop_lbl 544 `"   Machinists"', add
label define uocc95_pop_lbl 545 `"   Mechanics and repairmen, airplane"', add
label define uocc95_pop_lbl 550 `"   Mechanics and repairmen, automobile"', add
label define uocc95_pop_lbl 551 `"   Mechanics and repairmen, office machine"', add
label define uocc95_pop_lbl 552 `"   Mechanics and repairmen, radio and television"', add
label define uocc95_pop_lbl 553 `"   Mechanics and repairmen, railroad and car shop"', add
label define uocc95_pop_lbl 554 `"   Mechanics and repairmen (n.e.c.)"', add
label define uocc95_pop_lbl 555 `"   Millers, grain, flour, feed, etc."', add
label define uocc95_pop_lbl 560 `"   Millwrights"', add
label define uocc95_pop_lbl 561 `"   Molders, metal"', add
label define uocc95_pop_lbl 562 `"   Motion picture projectionists"', add
label define uocc95_pop_lbl 563 `"   Opticians and lens grinders and polishers"', add
label define uocc95_pop_lbl 564 `"   Painters, construction and maintenance"', add
label define uocc95_pop_lbl 565 `"   Paperhangers"', add
label define uocc95_pop_lbl 570 `"   Pattern and model makers, except paper"', add
label define uocc95_pop_lbl 571 `"   Photoengravers and lithographers"', add
label define uocc95_pop_lbl 572 `"   Piano and organ tuners and repairmen"', add
label define uocc95_pop_lbl 573 `"   Plasterers"', add
label define uocc95_pop_lbl 574 `"   Plumbers and pipe fitters"', add
label define uocc95_pop_lbl 575 `"   Pressmen and plate printers, printing"', add
label define uocc95_pop_lbl 580 `"   Rollers and roll hands, metal"', add
label define uocc95_pop_lbl 581 `"   Roofers and slaters"', add
label define uocc95_pop_lbl 582 `"   Shoemakers and repairers, except factory"', add
label define uocc95_pop_lbl 583 `"   Stationary engineers"', add
label define uocc95_pop_lbl 584 `"   Stone cutters and stone carvers"', add
label define uocc95_pop_lbl 585 `"   Structural metal workers"', add
label define uocc95_pop_lbl 590 `"   Tailors and tailoresses"', add
label define uocc95_pop_lbl 591 `"   Tinsmiths, coppersmiths, and sheet metal workers"', add
label define uocc95_pop_lbl 592 `"   Tool makers, and die makers and setters"', add
label define uocc95_pop_lbl 593 `"   Upholsterers"', add
label define uocc95_pop_lbl 594 `"   Craftsmen and kindred workers (n.e.c.)"', add
label define uocc95_pop_lbl 595 `"   Members of the armed services"', add
label define uocc95_pop_lbl 600 `"   Apprentice auto mechanics"', add
label define uocc95_pop_lbl 601 `"   Apprentice bricklayers and masons"', add
label define uocc95_pop_lbl 602 `"   Apprentice carpenters"', add
label define uocc95_pop_lbl 603 `"   Apprentice electricians"', add
label define uocc95_pop_lbl 604 `"   Apprentice machinists and toolmakers"', add
label define uocc95_pop_lbl 605 `"   Apprentice mechanics, except auto"', add
label define uocc95_pop_lbl 610 `"   Apprentice plumbers and pipe fitters"', add
label define uocc95_pop_lbl 611 `"   Apprentices, building trades (n.e.c.)"', add
label define uocc95_pop_lbl 612 `"   Apprentices, metalworking trades (n.e.c.)"', add
label define uocc95_pop_lbl 613 `"   Apprentices, printing trades"', add
label define uocc95_pop_lbl 614 `"   Apprentices, other specified trades"', add
label define uocc95_pop_lbl 615 `"   Apprentices, trade not specified"', add
label define uocc95_pop_lbl 620 `"   Asbestos and insulation workers"', add
label define uocc95_pop_lbl 621 `"   Attendants, auto service and parking"', add
label define uocc95_pop_lbl 622 `"   Blasters and powdermen"', add
label define uocc95_pop_lbl 623 `"   Boatmen, canalmen, and lock keepers"', add
label define uocc95_pop_lbl 624 `"   Brakemen, railroad"', add
label define uocc95_pop_lbl 625 `"   Bus drivers"', add
label define uocc95_pop_lbl 630 `"   Chainmen, rodmen, and axmen, surveying"', add
label define uocc95_pop_lbl 631 `"   Conductors, bus and street railway"', add
label define uocc95_pop_lbl 632 `"   Deliverymen and routemen"', add
label define uocc95_pop_lbl 633 `"   Dressmakers and seamstresses, except factory"', add
label define uocc95_pop_lbl 634 `"   Dyers"', add
label define uocc95_pop_lbl 635 `"   Filers, grinders, and polishers, metal"', add
label define uocc95_pop_lbl 640 `"   Fruit, nut, and vegetable graders, and packers, except factory"', add
label define uocc95_pop_lbl 641 `"   Furnacemen, smeltermen and pourers"', add
label define uocc95_pop_lbl 642 `"   Heaters, metal"', add
label define uocc95_pop_lbl 643 `"   Laundry and dry cleaning operatives"', add
label define uocc95_pop_lbl 644 `"   Meat cutters, except slaughter and packing house"', add
label define uocc95_pop_lbl 645 `"   Milliners"', add
label define uocc95_pop_lbl 650 `"   Mine operatives and laborers"', add
label define uocc95_pop_lbl 660 `"   Motormen, mine, factory, logging camp, etc."', add
label define uocc95_pop_lbl 661 `"   Motormen, street, subway, and elevated railway"', add
label define uocc95_pop_lbl 662 `"   Oilers and greaser, except auto"', add
label define uocc95_pop_lbl 670 `"   Painters, except construction or maintenance"', add
label define uocc95_pop_lbl 671 `"   Photographic process workers"', add
label define uocc95_pop_lbl 672 `"   Power station operators"', add
label define uocc95_pop_lbl 673 `"   Sailors and deck hands"', add
label define uocc95_pop_lbl 674 `"   Sawyers"', add
label define uocc95_pop_lbl 675 `"   Spinners, textile"', add
label define uocc95_pop_lbl 680 `"   Stationary firemen"', add
label define uocc95_pop_lbl 681 `"   Switchmen, railroad"', add
label define uocc95_pop_lbl 682 `"   Taxicab drivers and chauffers"', add
label define uocc95_pop_lbl 683 `"   Truck and tractor drivers"', add
label define uocc95_pop_lbl 684 `"   Weavers, textile"', add
label define uocc95_pop_lbl 685 `"   Welders and flame cutters"', add
label define uocc95_pop_lbl 690 `"   Operative and kindred workers (n.e.c.)"', add
label define uocc95_pop_lbl 700 `"   Housekeepers, private household"', add
label define uocc95_pop_lbl 710 `"   Laundressses, private household"', add
label define uocc95_pop_lbl 720 `"   Private household workers (n.e.c.)"', add
label define uocc95_pop_lbl 730 `"   Attendants, hospital and other institution"', add
label define uocc95_pop_lbl 731 `"   Attendants, professional and personal service (n.e.c.)"', add
label define uocc95_pop_lbl 732 `"   Attendants, recreation and amusement"', add
label define uocc95_pop_lbl 740 `"   Barbers, beauticians, and manicurists"', add
label define uocc95_pop_lbl 750 `"   Bartenders"', add
label define uocc95_pop_lbl 751 `"   Bootblacks"', add
label define uocc95_pop_lbl 752 `"   Boarding and lodging house keepers"', add
label define uocc95_pop_lbl 753 `"   Charwomen and cleaners"', add
label define uocc95_pop_lbl 754 `"   Cooks, except private household"', add
label define uocc95_pop_lbl 760 `"   Counter and fountain workers"', add
label define uocc95_pop_lbl 761 `"   Elevator operators"', add
label define uocc95_pop_lbl 762 `"   Firemen, fire protection"', add
label define uocc95_pop_lbl 763 `"   Guards, watchmen, and doorkeepers"', add
label define uocc95_pop_lbl 764 `"   Housekeepers and stewards, except private household"', add
label define uocc95_pop_lbl 770 `"   Janitors and sextons"', add
label define uocc95_pop_lbl 771 `"   Marshals and constables"', add
label define uocc95_pop_lbl 772 `"   Midwives"', add
label define uocc95_pop_lbl 773 `"   Policemen and detectives"', add
label define uocc95_pop_lbl 780 `"   Porters"', add
label define uocc95_pop_lbl 781 `"   Practical nurses"', add
label define uocc95_pop_lbl 782 `"   Sheriffs and bailiffs"', add
label define uocc95_pop_lbl 783 `"   Ushers, recreation and amusement"', add
label define uocc95_pop_lbl 784 `"   Waiters and waitresses"', add
label define uocc95_pop_lbl 785 `"   Watchmen (crossing) and bridge tenders"', add
label define uocc95_pop_lbl 790 `"   Service workers, except private household (n.e.c.)"', add
label define uocc95_pop_lbl 810 `"   Farm foremen"', add
label define uocc95_pop_lbl 820 `"   Farm laborers, wage workers"', add
label define uocc95_pop_lbl 830 `"   Farm laborers, unpaid family workers"', add
label define uocc95_pop_lbl 840 `"   Farm service laborers, self-employed"', add
label define uocc95_pop_lbl 910 `"   Fishermen and oystermen"', add
label define uocc95_pop_lbl 920 `"   Garage laborers and car washers and greasers"', add
label define uocc95_pop_lbl 930 `"   Gardeners, except farm, and groundskeepers"', add
label define uocc95_pop_lbl 940 `"   Longshoremen and stevedores"', add
label define uocc95_pop_lbl 950 `"   Lumbermen, raftsmen, and woodchoppers"', add
label define uocc95_pop_lbl 960 `"   Teamsters"', add
label define uocc95_pop_lbl 970 `"   Laborers (n.e.c.)"', add
label define uocc95_pop_lbl 975 `"Employed, unclassifiable"', add
label define uocc95_pop_lbl 980 `"   Keeps house/housekeeping at home/housewife"', add
label define uocc95_pop_lbl 982 `"   Helping at home/helps parents/housework"', add
label define uocc95_pop_lbl 983 `"   At school/student"', add
label define uocc95_pop_lbl 984 `"   Retired"', add
label define uocc95_pop_lbl 986 `"   Invalid/disabled w/ no occupation reported"', add
label define uocc95_pop_lbl 987 `"   Inmate"', add
label define uocc95_pop_lbl 995 `"   Other non-occupational response"', add
label define uocc95_pop_lbl 997 `"Occupation missing/unknown"', add
label define uocc95_pop_lbl 999 `"N/A (blank)"', add
label values uocc95_pop uocc95_pop_lbl

label define incnonwg_pop_lbl 0 `"N/A"'
label define incnonwg_pop_lbl 1 `"Less than $50 nonwage, nonsalary income"', add
label define incnonwg_pop_lbl 2 `"$50+ nonwage, nonsalary income"', add
label define incnonwg_pop_lbl 9 `"Missing"', add
label values incnonwg_pop incnonwg_pop_lbl

label define migrate5_pop_lbl 0 `"N/A"'
label define migrate5_pop_lbl 1 `"Same house"', add
label define migrate5_pop_lbl 2 `"Moved within state"', add
label define migrate5_pop_lbl 3 `"Moved between states"', add
label define migrate5_pop_lbl 4 `"Abroad five years ago"', add
label define migrate5_pop_lbl 8 `"Moved (place not reported)"', add
label define migrate5_pop_lbl 9 `"Unknown"', add
label values migrate5_pop migrate5_pop_lbl

label define migrate5d_pop_lbl 00 `"N/A"'
label define migrate5d_pop_lbl 10 `"Same house"', add
label define migrate5d_pop_lbl 20 `"Same state (migration status within state unknown)"', add
label define migrate5d_pop_lbl 21 `"Different house, moved within county"', add
label define migrate5d_pop_lbl 22 `"Different house, moved within state, between counties"', add
label define migrate5d_pop_lbl 23 `"Different house, moved within state, within PUMA"', add
label define migrate5d_pop_lbl 24 `"Different house, moved within state, between PUMAs"', add
label define migrate5d_pop_lbl 25 `"Different house, unknown within state"', add
label define migrate5d_pop_lbl 30 `"Different state (general)"', add
label define migrate5d_pop_lbl 31 `"Moved between contiguous states"', add
label define migrate5d_pop_lbl 32 `"Moved between non-contiguous states"', add
label define migrate5d_pop_lbl 33 `"Unknown between states"', add
label define migrate5d_pop_lbl 40 `"Abroad five years ago"', add
label define migrate5d_pop_lbl 80 `"Moved, but place was not reported"', add
label define migrate5d_pop_lbl 90 `"Unknown"', add
label values migrate5d_pop migrate5d_pop_lbl


