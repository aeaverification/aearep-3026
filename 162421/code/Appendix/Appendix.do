**********************************
** Appendix ****************
**********************************/

cd "$do_path"
do "./Appendix/TableB1.do"

cd "$do_path"
do "./Appendix/TableB2.do"

cd "$do_path"
do "./Appendix/TableB3.do"

cd "$do_path"
do "./Appendix/TableB4.do"

cd "$do_path"
do "./Appendix/TableB5.do"

cd "$do_path"
do "./Appendix/TableB6.do"

cd "$do_path"
do "./Appendix/TableB7.do"

cd "$do_path"
do "./Appendix/FigureB1.do"