

*
* Table B2: Effect of Competition in Local Labor Markets
*
eststo clear

use "${temp_path}/F1B", clear



* Dummy treatment variables
g D_station = (station_1950>0 & station_1950!=.)
g D_stationComp = (station_1950>1 & station_1950!=.)
g D_stationComp_fro	 = ((frozen_1950+station_1950)>1 & station_1950==1 & station_1950!=.)
label var D_station "Station dummy"
label var D_stationComp "Competition dummy"
label var D_stationComp_fro "Placebo competition dummy"


** REGRESSIONS
** effect of station dummy
eststo: reghdfe Pct_A99_PTW   D_station   amenities [aw=CZpeople] , absorb( czone year#occ195)  cluster(czone)
estadd local clusterID `e(N_clust)'

** + multiple stations dummy 
eststo: reghdfe Pct_A99_PTW   D_station D_stationComp  amenities [aw=CZpeople] , absorb( czone year#occ195)  cluster(czone)
estadd local clusterID `e(N_clust)'

** + placebo dummy fo multiple stations
eststo: reghdfe Pct_A99_PTW   D_station D_stationComp D_stationComp_fro  amenities [aw=CZpeople] , absorb( czone year#occ195)  cluster(czone)
estadd local clusterID `e(N_clust)'


esttab using "${output_path}/TableB2.csv", label replace keep(  D_*  ) varwidth(20)   rename(1.TVinCZ TVinCZ)  scalars("clusterID number of cluster")   sfmt(a2) se

eststo clear



