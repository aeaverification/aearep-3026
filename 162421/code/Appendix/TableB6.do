*
* Table B6: Tripple Difference
*

use "${temp_path}/TA6", clear
char group[omit] 3

*************************************************************
* REGRESSIONS

* Baseline with broader control group
eststo: reghdfe Pct_A99_PTW c.station_1950#treated c.amenities#treated  [aw=CZpeople], absorb( czone year#occ195) cluster(czone)
estadd local FED "{$\checkmark$}"
estadd local clusterID "{`:var lab `e(clustvar)''}"
* Effects on each occupation in control group
eststo: reghdfe Pct_A99_PTW c.station_1950#group c.amenities#group  [aw=CZpeople], absorb( czone year#occ195) cluster(czone)
estadd local FED "{$\checkmark$}"
estadd local clusterID "{`:var lab `e(clustvar)''}"
* Tripple difference estimate
eststo: reghdfe Pct_A99_PTW c.station_1950#1b.treated c.amenities#treated [aw=CZpeople], absorb( czone#occ czone#year year#occ195) cluster(czone)
estadd local D3 "{$\checkmark$}"
estadd local clusterID "{`:var lab `e(clustvar)''}"
char group[omit] 3

esttab using "${output_path}/TableB6", label replace keep( *tation_195*  ) varwidth(20)    interactio(" X \\")  se  scalars("demog demographics"  "CZ commuting zone trends" "sample sample" "clusterID number of cluster" ) csv

eststo clear