**********************************************************
* Figure 
**********************************************************
use "${temp_path}/rank_mobility", clear

* Position in distribution
label var wage_percentile "Percentile of pre-TV Wage Distribution (1939)"
distplot wage_percentile , lcolor("49 130 189") ytitle("Cumulative Probability")
graph export "${output_path}/FigureB1.pdf", replace

