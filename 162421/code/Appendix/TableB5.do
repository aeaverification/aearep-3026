*
* Table B5: Effect of TV on Top Earning Entertainers
*

use "${temp_path}/F1B", clear


*************************************************************
* REGRESSIONS

/********************************************************
**	PANEL A: Entertainer among Top 1% of US Earners 
** 		(percent of entertainer)			 		*****
*********************************************************/


* Baseline
eststo PTW1 : reghdfe Pct_A99_PTW   station_1950   amenities [aw=CZpeople] , absorb( czone year#occ195)  cluster(czone)
* pre-treatment outcome variable mean (in treated locations)
sum Pct_A99_PTW if count_stations>0 & e(sample)==1 & year!=1950 
estadd local MDV = round(r(mean), 0.01)
* baseline + demographics
eststo PTW2: reghdfe Pct_A99_PTW med_age share_male med_inc pop_density c.year#urban_place share_NW   station_1950 amenities [aw=CZpeople], absorb( czone year#occ195)  cluster(czone)
estadd local demog "{$\checkmark$}"
* baseline + CZ specific trends
eststo PTW3: reghdfe Pct_A99_PTW  station_1950 amenities [aw=CZpeople], absorb( czone##c.year year#occ195)  cluster(czone)
estadd local CZ "{$\checkmark$}"




/*******************************************************
**	PANEL B: Entertainer among Top 1% of US Earners 
**	(per capita) 									*****
*********************************************************/

* define Entertainer per capita 
g entertPC= Pct_A99/CZpeople * 100000
label var entertPC "Top Paid Entertainer per Capita (in 100,000)"

eststo p1p: reghdfe entertPC  station_1950   amenities [aw=CZpeople] , absorb( czone year#occ195)  cluster(czone)
* pre-treatment outcome variable mean (in treated locations)
sum entertPC if count_stations>0 & e(sample)==1 & year!=1950 
estadd local MDV = round(r(mean), 0.01)

eststo p1p2: reghdfe entertPC med_age share_male med_inc pop_density c.year#urban_place share_NW   station_1950 amenities [aw=CZpeople], absorb( czone year#occ195)  cluster(czone)
estadd local demog "{$\checkmark$}"

eststo p1p3: reghdfe entertPC  station_1950 amenities [aw=CZpeople], absorb( czone##c.year year#occ195)  cluster(czone)
estadd local CZ "{$\checkmark$}"

/********************************************************
**	PANEL C: Entertainer in Top 1% of US Earners ********
**	(raw counts)								 ********
*********************************************************/

eststo p1pC: reghdfe Pct_A99  station_1950   amenities [aw=CZpeople] , absorb( czone year#occ195)  cluster(czone)
* pre-treatment outcome variable mean (in treated locations)
sum Pct_A99 if count_stations>0 & e(sample)==1 & year!=1950 
estadd local MDV = round(r(mean), 0.01)

eststo p1p2C: reghdfe Pct_A99 med_age share_male med_inc pop_density c.year#urban_place share_NW   station_1950 amenities [aw=CZpeople], absorb( czone year#occ195)  cluster(czone)
estadd local demog "{$\checkmark$}"

eststo p1p3C: reghdfe Pct_A99  station_1950 amenities [aw=CZpeople], absorb( czone##c.year year#occ195)  cluster(czone)
estadd local CZ "{$\checkmark$}"

** Put results in EXCEL
esttab PTW*  p* using "${output_path}/TableB5.csv", label replace keep( station_1950   ) varwidth(20) nobase   scalars("demog demographics"  "CZ commuting zone trends" "MDV mean LHS") sfmt(a2) se  csv 
est clear