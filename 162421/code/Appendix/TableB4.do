
*
* Table B 4: Top Income Shares
*

use year czone cell_size rel_share_01_1 rel_share_10_1 l_T1 l_T01 l_T10 using "${temp_path}/TopEarnings", clear


** add data on TV (studio)
merge 1:1 czone year using "${orig}/TVStudio", keep(3)
drop _m


* TI shares

* top 0.1%
eststo est1: reghdfe l_T01  station_1950 amenities [aw=cell_size], absorb( czone year)  cluster(czone)
estadd local cluster `=e(N_clust)': est1
* compute P-value of equal growth rate at top 1 and top 0.1 percentile
reghdfe rel_share_01_1  station_1950 amenities [aw=cell_size], absorb( czone year)  cluster(czone)
test station_1950
estadd local rel_growth = round(r(p), 0.0001) : est1

* top 1%
eststo est2: reghdfe l_T1  station_1950 amenities [aw=cell_size], absorb( czone year)  cluster(czone)
estadd local cluster `=e(N_clust)': est2

* top 10%
eststo est3: reghdfe l_T10  station_1950 amenities [aw=cell_size], absorb( czone year)  cluster(czone)
estadd local cluster `=e(N_clust)': est3
* compute P-value of equal growth rate at top 1 and top 10 percentile
reghdfe rel_share_10_1  station_1950 amenities [aw=cell_size], absorb( czone year)  cluster(czone)
test station_1950
estadd local rel_growth = round(r(p), 0.0001) : est3


* PANEL B - larger markets only (freeze vs TV places)


* top 0.1%
eststo est4: areg l_T01  i.year station_1950 amenities [aw=cell_size] if count_stations >0 | frozen_permit >0, absorb( czone)  cluster(czone)
estadd local cluster `=e(N_clust)': est4
* compute P-value of equal growth rate at top 1 and top 0.1 percentile
areg rel_share_01_1  i.year station_1950 amenities [aw=cell_size] if count_stations >0 | frozen_permit >0, absorb( czone)  cluster(czone)
test station_1950
estadd local rel_growth = round(r(p), 0.0001) : est4

* top 1%
eststo est5: areg l_T1  i.year station_1950 amenities [aw=cell_size] if count_stations >0 | frozen_permit >0, absorb( czone)  cluster(czone)
estadd local cluster `=e(N_clust)' : est5

* top 10%
eststo est6: areg l_T10  i.year station_1950 amenities [aw=cell_size] if count_stations >0 | frozen_permit >0, absorb( czone)  cluster(czone)
estadd local cluster `=e(N_clust)' : est6
* compute P-value of equal growth rate at top 1 and top 10 percentile
areg rel_share_10_1  i.year station_1950 amenities [aw=cell_size] if count_stations >0 | frozen_permit >0, absorb( czone)  cluster(czone)
test station_1950
estadd local rel_growth = round(r(p), 0.0001) : est6

esttab using "${output_path}/TableB4.csv", label scalar("rel_growth p-value same growth as s_10" "cluster No. of CZ Cluster") replace keep(*tation_*)  se 
est clear
