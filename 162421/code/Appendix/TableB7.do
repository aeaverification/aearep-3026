*
* Table B7: state level analysis
*

use "${temp_path}/data_state", clear


* dynamic treatment variable
char year[omit] 1915
tab year, g(dum_year)
* interact local station in 1949 with year dummies
forvalue i=1/5 {
	g dum_`i' = dum_year`i'*count_stations
}
label var dum_1 "1915 x TV"
label var dum_2 "1940 x TV"
label var dum_3 "1950 x TV"
label var dum_4 "1960 x TV"
label var dum_5 "1970 x TV"

*************************************************************
** REGRESSIONS

* Sample years 1940-1970
eststo: reghdfe maxEarner dum_3 amenities [aw=CZpeople] if year>1916, absorb( statef year##occ195)  cluster(statef)
* Sample years 1915-1970
eststo: reghdfe maxEarner dum_3 amenities  [aw=CZpeople] , absorb( statef year##occ195)  cluster(statef)
* Dynamic treatment effects
eststo: reghdfe maxEarner  dum_1 dum_3-dum_5 amenities  [aw=CZpeople]  , absorb( statef year##occ195)  cluster(statef)

esttab using "${output_path}/TableB7.csv", label replace keep( dum_* station_1950) varwidth(20) nobase  interactio(" X \\")  nostar rename(dum_3 station_1950) nomtitles cells(b(fmt(%4.3f))se( par fmt(%4.3f)))  csv 
eststo clear
