*
* Table B 3: Effects on the 99th Percentile
*

use year czone lcentile99 lpareto_inc lfixed_multiple using   "${temp_path}/TopEarnings", clear

** add data on TV (studio)
merge 1:1 czone year using "${orig}/TVStudio", keep(3)
drop _m

** add CZ demographics
merge m:1 czone year  using "${temp_path}/CZ_demographics",  keep(3)
drop _m

* Panel A
eststo t999 : areg lcentile99 i.year station_1950 amenities  [aw=CZpeople ] if (count_stations >0 | frozen_permit >0), absorb( czone )  cluster(czone)
estadd local clusterID `e(N_clust)'

eststo t998 : areg lcentile99 i.year station_1950 amenities share_male med_inc pop_density c.year#urban_place share_NW   [aw=CZpeople ] if (count_stations >0 | frozen_permit >0), absorb( czone )  cluster(czone)
estadd local demog "{$\checkmark$}"
estadd local clusterID `e(N_clust)'

eststo t997 : areg lcentile99 i.year czone##c.year station_1950 amenities  [aw=CZpeople ] if (count_stations >0 | frozen_permit >0), absorb( czone )  cluster(czone)
estadd local CZ "{$\checkmark$}"
estadd local clusterID `e(N_clust)'


* Panel B
eststo t996 : areg lfixed_multiple i.year station_1950 amenities  [aw=CZpeople ] if (count_stations >0 | frozen_permit >0), absorb( czone )  cluster(czone)
estadd local clusterID `e(N_clust)'

eststo t995 : areg lfixed_multiple i.year station_1950 amenities share_male med_inc pop_density c.year#urban_place share_NW    [aw=CZpeople ] if (count_stations >0 | frozen_permit >0), absorb( czone )  cluster(czone)
estadd local demog "{$\checkmark$}"
estadd local clusterID `e(N_clust)'

eststo t994 : areg lfixed_multiple i.year czone##c.year station_1950 amenities  [aw=CZpeople ] if (count_stations >0 | frozen_permit >0), absorb( czone )  cluster(czone)
estadd local CZ "{$\checkmark$}"
estadd local clusterID `e(N_clust)'


* PANEL C:
eststo t993 : areg lpareto_inc i.year station_1950 amenities  [aw=CZpeople ] if (count_stations >0 | frozen_permit >0) , absorb( czone )  cluster(czone)
estadd local clusterID `e(N_clust)'

eststo t992 : areg lpareto_inc i.year station_1950 amenities share_male med_inc pop_density c.year#urban_place share_NW  [aw=CZpeople ] if (count_stations >0 | frozen_permit >0) , absorb( czone )  cluster(czone)
estadd local demog "{$\checkmark$}"
estadd local clusterID `e(N_clust)'

eststo t991 : areg lpareto_inc i.year czone##c.year station_1950 amenities  [aw=CZpeople ] if (count_stations >0 | frozen_permit >0) , absorb( czone )  cluster(czone)
estadd local CZ "{$\checkmark$}"
estadd local clusterID `e(N_clust)'


** Put results in EXCEL
esttab t99* using "${output_path}/TableB3.csv", label replace keep( station_1950   ) varwidth(20) nobase  rename(1.TVinCZ TVinCZ)     scalars("clusterID number of cluster" "demog demographics"  "CZ commuting zone trends" "MDV mean LHS" "group placebo") cells(b(fmt(%4.3f))se( par fmt(%4.3f)))  csv 
est clear


