

****************************************************************
** summary table
******************************************************************

*
* Three Panels of CZ-decade descriptives: 
* A) Television access
* B) Local Entertainers 
* C) CZ Demographics


******************************************************************
* PANEL A: TV access
******************************************************************
est clear
*TV signal data
use year czone ITM_dummy using "${orig}/TVsignal", clear
drop if year<1940

** add data on TV (studio)
merge m:1 czone year using "${orig}/TVStudio", keepusing(station_1950 amenities)

* generate time invariant filming cost 
bys czone: egen amenities2 = max(amenities )
drop amenities


*labels
label var amenities "Local Filming Cost"
label var station_1950 "Local TV Stations"
label var ITM_dummy "TV signal"

*summary stats for T1
local TV  "station_1950  amenities2  ITM_dummy"
estpost tabstat  `TV' ,  statistic(count mean sd )  elabels columns(statistics)
esttab . using "${output_path}/TableB1.csv",  cells("count mean(fmt(a2)) sd") nonumber replace label

*********************************************************
* Penal B: Entertainment industry
*********************************************************
use group czone year worker treated_occupation using "${temp_path}/employment", clear
keep if year>1930 & (group==1 | group==.) /// restrict to core sample years and entertainer occupation

* aggregate employment at CZ-year level
rename worker entertainer_emp
g worker = entertainer_emp if treated_occupation==1
collapse (sum) worker entertainer_emp , by(czone year)

label var worker "Employment in Performance Entertainment"
label var entertainer_emp "Employment in Leisure Activities"


*summary stats for T1
local entertainment "worker entertainer_emp"
estpost tabstat  `entertainment' ,  statistic(mean sd count)  elabels columns(statistics)
esttab . using "${output_path}/TableB1.csv",  cells("count mean(fmt(a2)) sd  ") nonumber append label

* 99th wage percentile
use "${temp_path}/earningsTB1", clear

label var centile99 "99th Wage Percentile"

estpost tabstat  centile99 ,   statistic(count mean  sd)  elabels columns(statistics)
esttab . using "${output_path}/TableB1.csv",  cells("count mean(fmt(a2))  sd " ) nonumber append label


********************************************
* Penal C: demographics
********************************************

use "${temp_path}/CZ_demographics", clear
drop if year<1940 /// core sample

* re-scale
foreach var in  urban_place share_NW  share_male   {
	replace `var' = `var'*100
}

* Re-scale counts to per 1,000
replace CZpeople = CZpeople/1000
replace CZworker = CZworker/1000

*summary stats for T1
local demographics " CZpeople CZworker med_inc pop_density urban_place share_NW  share_male   med_age"
estpost tabstat  `demographics' ,  statistic(count mean sd  )  elabels columns(statistics)
esttab . using "${output_path}/TableB1.csv",  cells("count mean(fmt(a2)) sd  ") nonumber append label
