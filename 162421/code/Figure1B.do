* Figure 1b

************************************************
* TV's Effect Across Income Distribution
************************************************
eststo clear

* load data
use "${temp_path}/F1B", clear

* Run DiD for alternative outcomes
foreach var of varlist Pct_*_PTW {

	* Outcome: % change relative to average bin size
	sum `var'  if count_stations>0
	replace `var' = `var'/r(mean)

	* Effect of Television
	eststo `var': reghdfe `var'   station_1950   amenities [aw=CZpeople] , absorb( czone year#occ195)  cluster(czone)
	
	
}

coefplot (Pct_B25_PTW, baselevels  keep(station_1950) ciopts(recast(rcap) lcolor(gs15) lwidth(medthin) ) mcolor(navy*0.6) ) ///
	(Pct_2550_PTW, baselevels  keep(station_1950) ciopts(recast(rcap) lcolor(gs15) lwidth(medthin) ) mcolor(dknavy) ) ///
	(Pct_5075_PTW, baselevels  keep(station_1950) ciopts(recast(rcap) lcolor(gs15) lwidth(medthin) ) mcolor(midblue)) ///
	(Pct_7590_PTW, baselevels  keep(station_1950) ciopts(recast(rcap) lcolor(gs15) lwidth(medthin) ) mcolor(dkorange*0.8)) ///
	(Pct_9095_PTW, baselevels  keep(station_1950) ciopts(recast(rcap) lcolor(gs15) lwidth(medthin) ) mcolor(orange_red*0.5)  ) ///
	(Pct_9599_PTW, baselevels  keep(station_1950) ciopts(recast(rcap) lcolor(gs15) lwidth(medthin) ) mcolor(cranberry) ) ///
	(Pct_A99_PTW, baselevels  keep(station_1950) ciopts(recast(rcap) lcolor(gs15) lwidth(medthin) ) mcolor(red) ) ///
	, vertical yline(0) ytitle("Effect of TV on % {&Delta} Employed") xtitle("Percentile of US Wage Distribution") ///
 	graphregion(color(white)) yscale(range(-1.5 1.5)) ylabel(-1(0.5)1) xlabel("") /// 
 	legend(order(2 "0-25" 4 "25-50" 6 "50-75" 8 "75-90" 10 "90-95" 12 "95-99" 14 "99+") rows(2) subtitle("Percentile") position(6))
graph export "${output_path}/Figure1B.pdf", replace

esttab using "${output_path}/TVEarner_acrossOvaDistPC.csv", label replace keep( station_1950  ) varwidth(20) nobase  rename(1.TVinCZ TVinCZ)    sfmt(a2)  se scalars("MDV mean LHS") plain
