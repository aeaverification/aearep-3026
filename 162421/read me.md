
## Overview

**Technical Change and Superstar Effects: Evidence from the Rollout of Television**

**Felix Koenig**
**May 2022**

This document describes the replication archive for "Technical Change and Superstar Effects: Evidence from the Rollout of Television." The archive contains all of the programs to produce the results in the main text and the online appendix. 


## Data Availability and Provenance Statements


1. IPUMS

The main data source in this study is IPUMS USA. The study uses 
This data can be obtained by registering [here](ipums.org/usa). IPUMS redistribution rules are:

'You may not redistribute any data from IPUMS-USA Full Count data. For IPUMS-USA Sample data, you may publish a subset of the data to meet journal requirements for accessing data related to a particular publication. Contact us for permission for any other redistribution of IPUMS-USA Sample data; we will consider requests for free and commercial redistribution.'

In line, with these requirements, IPUMS and can be accessed from the IPUMS webpage after registering [here](ipums.org/usa). Extract descriptions (DICT files) are included in this replication package.

Note that version 1 of the 1920-1940 full-count data was used in the analysis. On April 19, 2021 IPUMS data revision  introduced a harmonized top-code for the INCWAGE variable and currently available data may thus produce different results. See the revision history for details: https://usa.ipums.org/usa-action/revisions.
Previous vintages of the data can be accessed as follows: 'Data are available by request to registered IPUMS-USA users. Please contact ipums@umn.edu. Archived versions of the data have been stored in fixed format files with accompanying documentation and set-up files.'

2. TV Studios

Data on TV studios and blocked TV stations was hand collected from "TV Digest" magazine records, republished here and accessed in July 2016: https://worldradiohistory.com/Archive-TV-Digest/40s/. The resulting data are provided as part of this replication package. Additional data on the number of film shoots per locations is derived from the IMDb, accessed in November 2016 [here]{ftp://ftp.fu-berlin.de/pub/misc/movies/database/frozendata/}. But the database has since been migrated to [S3]{https://community-imdb.sprinklr.com/conversations/data-issues-policy-discussions/imdb-data-now-available-in-amazon-s3/5f4a79d08815453dba8bde44?page=4}. The data is courtesy of IMDb and used with permission and is provided as part of the replication package. 

3. CZ crosswalks

Links from IPUMS geography identifiers to commuting zones where build manually by overlaying maps of counties and commuting zones for 1920-1940 and 1960. These maps were obtained from NHGIS and can be downloaded from https://www.nhgis.org/ after registration. The resulting crosswalks are provided as part of this replication package.

These files define a set of commuting zones that can be constructed from US Census data. These areas span the conginent US and can be used as consistent geographies across Census vintages. I provide crosswalks that map the smallest geographic identifiers available in the IPUMS US Census micro data to 1990 commuting zones. The commuting zones are consistently defined and devide the mainland US into 722 geographic areas. In some years additional CZs for Alaska and Hawaii are provided but not used in this project and have not been stress-tested. Please see the CZreadme file (in folder orig/CZ crosswalk) for further details on the data construction.

4. IRS tabulations

Aggregate tax data tabulations at the occupation-state-year level for 1916 published by the IRS. Original tables are published in IRS publication number 2817.

5. Demographics of TV stars

Demographic information on TV stars is transcribed from the "Radio Annual and Television Yearbook" (1949) re-published by the internet archive: https://archive.org/details/radioannualtelev00radi/mode/2up/search/year+book+of+television (p. 795ff) and hand matched to the US 1940 Census records (these records are available here:https://1940census.archives.gov/). The combined data is included in the replication package.

6. Prior Studies

Data from previous studies were obtained from published data repositories or obtained directly from the authors. See the text below for links to data repositories and lists of data sets that need to be requested from authors.


### Statement about Rights

- [x ] I certify that the author(s) of the manuscript have legitimate access to and permission to use the data used in this manuscript. 


### Summary of Availability

- [ ] All data **are** publicly available.
- [x] Some data **cannot be made** publicly available.
- [ ] **No data can be made** publicly available.

### Details on each Data Source

** I. Study data sets available online (may require registration)**

- IPUMS sample data:
US Census 1970 - both metro samples 
US Census 1960 - 5% sample
US Census 1950 - 1% sample

> The paper uses IPUMS USA data (Ruggles et al, 2019) for the years 1950, 1960, 1970. This data is derived from decenial population Census records of the US Census Bureau. The data is subject to a redistribution restriction, but can be freely downloaded from https://usa.ipums.org/usa after registration. 

DCT files: Lists of required IPUMS variables are available in the `data/IPUMS` folder.

- IPUMS full count data:
US Census 1940 - full count
US Census 1930 - full count
US Census 1920 - full count

>IPUMS USA full count data (U Minesota & Ancestry, 2013;) for 1920, 1930 and 1940. More details on accessing this data is provided in section 'Data Availability and Provenance Statements.'

- Commuting Zones Crosswalk II

>Crosswalks from IPUMS identifiers in the 1950 and 1970 data to Commuting Zones was obtained from David Dorn's website: https://www.ddorn.net/data.htm

>The data are replication files for Autor & Dorn (2013) and I use the files E1, E2 and E8.


- State FIPS to ICSPR Codes Crosswalk
file: `orig/StateFIPSicsprAB.xls`

>The data is publically available and can be downloaded [here](
http://staff.washington.edu/glynn/StateFIPSicsprAB.xls)



** II. Study data digitised/generated by the author (provided in the replication package)**

- Television studio data 
file: `orig/TVstudio.dta`

> This data was digitized by the author from historic magazines. For details on this data see section "Data Availability and Provenance Statements"

- IRS tax data
file: `orig/IRS.dta`

> Manually digitized tax tabulations from from IRS (1918).
Source: Treasury Department, Internal Revenue (1918). Statistics of Income, Compiled from the Returns for 1916. Document No 2817. available [here](https://www.irs.gov/pub/irs-soi/16soirepar.pdf).

- Commuting Zones Crosswalk
file: `orig/CZ crosswalk/cw_puma1960_czone.dta` and `orig/CZ crosswalk/cw_ctyYYYY.dta` for 1920, 1930, 1940 respectively.

> Manually created crosswalks from IPUMS location identifiers to 1990 commuting zones. The data was created by the author by overlapping historic county maps with CZ boundaries as described in section "Data Availability and Provenance Statements."

- Area of Commuting Zone
file: `orig/CZ_area.dta`

> This file includes teh land area of each CZ. Based on authors calculations from CZ maps described in section "Data Availability and Provenance Statements."


- Sample Definition
file: `orig/sample_definition.dta`

> List of treated & untreated occupations, CZs and years that are part of the sample.


- Demographics of TV Stars
file: `orig/Top artist.xls`

> Hand collected data on demographic information and 1940 earnings of later television stars. 




** III. Study data sets received from study authors (not available in deposit)**

- Television Signal 

> Data was obtained from Fenton, G., & Koenig, F. (2021) and permission was obtained to use the data for this study. The data may be requested from the authors directly.



## Dataset list

| Data file | Source | Notes    |Provided |
|-----------|--------|----------|---------|

| `orig/IRS.dta` | IRS |  | Yes |
| `orig/TVstudio.dta` | digitized from TV digest | | Yes|
| `orig/TVsignal.dta` | Fenton & Koenig (2021) | unpublished data may be requested from authors| No|
| `orig/Top artist.xlsx` | digitized from Radio & TV Yearbook and 1940 US Census enumeration cards| | Yes|
| `orig/StateFIPSicsprAB.xls` | Glynn | | Yes|
| `data/IPUMS/usa_YYYYYYY` | IPUMS | available through IPUMS, DICT files provided | No|
| `orig/CZ crosswalk/cw_ctyYYYY.dta` | Author | years 1920, 1930, 1940| Yes |
| `orig/CZ crosswalk/cw_puma1960_czone.dta` | Author |  | Yes |
| `orig/CZ crosswalk/cw_XXXYYYY.dta` | Autor & Dorn (2013) | Available from Authors for 1950 & 1970| Yes|
| `orig/sample_definition.dta` | Author | List of sample occupations/CZs | Yes|
| `code/ID lists/XXXXX.txt`| Author | Lists of occ, year, czone, state IDs in IPUMS data and sample respectively| Yes |




## Computational requirements

The code was last run in STATA 17.

### Software Requirements


- Non-standard STATA packages used in the process. The 0.master.do file contains instructions on how to install the packages if needed
  - `estout` 
  - `reghdfe` 
  - `egenmore` 
  - `gtools` 

### Memory and Runtime Requirements


- There are no specific computational requirements other than those necessary to run STATA. The code was last run on a **8-core M1-based laptop with MacOS version 11.5.2 and using STATA/MP 17** and took 2 hours and 18 minutes to run.



## Description of programs/code



>The code assumes the following folder structure:

  - root "YOURPATH" : chose a folder that hosts all the other project files.
  
    - "$root/code" : contains all do-files necessary to generate the results of the study and the necassary input files.
         -- "$root/code/build_data" : contains all do-files necessary to clean and process the original raw data. The files output datasets used in the Figures and Tables.
    
    - "$dir/orig" : contains the following data files (either from the replication package or otherwise obtained): `IRS.dta, TVsignal.dta, TVstudio.dta, CZ_area.dta, usa_xxxxxxx.dat` 

      -- "$dir/orig/CZ crosswalk" : contains crosswalks from Census geography IDs to 1990 CZs. There is one crosswalk per Census.

      -- "$dir/orig/ID lists" : contains files with list of IDs. Specifically, files with IDs for IPUMS states (`state_list.txt, state_list70.txt, StateFIPSicsprAB.xls`) and CZs (`czones.txt`), as well as a list of all the occupation-year-CZ IDs that are part of the study sample (`sample_definition.dta`).

    
    -  "$root/processed_data" : the do-files will save temporary files in this directory. 
    
    -  "$root/results" : the do-files will save tables and figures of the paper in this directory.
    
    -  "$root/log" :  the do-files will save log files in this directory.




## Instructions to Replicators

- Download and obtain the data files referenced above. Each should be stored in as described above. Make sure to unzip the files. 

- Before running any of the do-files, set global directory path in `0.master.do` (line 25).
Also change the path in all the `root/code/IPUMS/usa_yyyy.do` files and make sure they point to the IPUMS data you downloaded in the previous step.

- Run `root/code/0-master.do` in STATA. This file executes all steps used to generate the Tables and Figures in the paper.

- Figure 1B in the text uses different colors and fonts than the Stata output. The formatting was dones in Excel. You need to use `code/styleFig1B.xlsx` to  replicate the formating & style. Here is how: The STATA do-files `Figure 1B.do` saves table `output/TVEarner_acrossOvaDistPC.csv` which contains the point estimates and coefficients shown in Figure 1B. If you paste these values into cells A14:B20 on sheet "Tabelle 1" in `styleFig1B.xlsx,` the correctly formatted Figure 1B appears on sheet "Chart1."

_ Appendix Figure B5: To compute the effect magnitude (reported in row "increase on baseline") you need to divide the point estimate by the mean LHS values reported in `output/FigureB5.`

## List of tables and programs


The provided code reproduces:

- [ ] All numbers provided in text in the paper
- [ ] All tables and figures in the paper
- [x ] Selected tables and figures in the paper, as explained and justified below.


| Figure/Table #    | Program                  | Line Number | Output file                      | Note                            |
|-------------------|--------------------------|-------------|----------------------------------|---------------------------------|
| Figure 1           | code/Figure1B.do    | 34           | Figure1B.pdf                 || Panel B is part of the replication package. Panel A is based on theory, does not contain data, and is not part of the replication package. The Figure note explains how F1A is created (it is compiled directly in the latex code).
| Figure 2           | code/Figure2.do| 17 & 27          | Figure2A.pdf & Figure2B.pdf                      ||
| Figure 3           | code/Figure3.do| 113         | Figure3.pdf                      ||
| Table 1           | code/Table1.do      |             | Table1.csv                      |  Requires data from Fenton & Koenig (2021)|
| Table 2           | code/Table2.do      |             | Table2.csv           |       |
| Table B1           | code/Appendix/TableB1.do      |            | TableB1.csv           |       |
| Table B2           | code/Appendix/TableB2.do      |            | TableB2.csv           |       |
| Table B3           | code/Appendix/TableB3.do      |            | TableB3.csv           |       |
| Table B4           | code/Appendix/TableB4.do      |            | TableB4.csv           |       |
| Table B5           | code/Appendix/TableB5.do      |            | TableB5.csv           |       |
| Table B6           | code/Appendix/TableB6.do      |            | TableB6.csv           |       |
| Table B7           | code/Appendix/TableB7.do      |            | TableB7.csv           |       |
| Figure B1           | code/Appendix/FigureB1.do      |   9         | FigureB1.csv           |       |


## References

- Data

Autor, David H. Dorn, David. Replication Data for: The Growth of Low-Skill Service Jobs and the Polarization of the US Labor Market American Economic Review 103 5 1553-97 2013 10.1257/aer.103.5.1553, [data access](https://www.ddorn.net/data.htm)

Fenton, G., & Koenig, F. (2021). Data for: Labor Supply and Entertainment Innovations: Evidence From the U.S. TV Rollout, unpublished data. Accessed 10 May 2019.

Internet Movie Database [dataset]. www.imdb.com . Accessed May 2018: ftp://ftp.fu-berlin.de/pub/misc/movies/database/frozendata/

Steven Manson, Jonathan Schroeder, David Van Riper, Tracy Kugler, and Steven Ruggles (2012). IPUMS National Historical Geographic Information System: Version 12.0 [dataset]. Minneapolis, MN: IPUMS. https://www.ipums.org/projects/ipums-nhgis/d050.v12.0

Minnesota Population Center and Ancestry.com (2013). IPUMS Anonymized Complete Count Data: Version 1.0 [dataset]. Minneapolis, MN: University of Minnesota. https://doi.org/10.18128/D014.V1.0

Steven Ruggles, Sarah Flood, Ronald Goeken, Josiah Grover, Erin Meyer, Jose Pacas, and Matthew Sobek. Integrated Public Use Microdata Series: Version 7.0 [dataset]. Minneapolis: University of Minnesota, 2017. https://www.ipums.org/projects/ipums-usa/d010.v7.0


- Primary Sources used during Data Collection

Radio Annual and Television Yearbook (1949). Accessed May 2022: https://archive.org/details/radioannualtelev00radi/page/n5/mode/2up

Statistics of Income (1918). Treasury Department, Internal Revenue. Document No 2817.

Television Digest (1949). Accessed July 2016: https://worldradiohistory.com/Archive-TV-Digest/40s/


---

## Acknowledgements

I thank Lars Vilhuber, Miklos Kóren, Joan Llull, Marie Connolly, Peter MorrowSome for providing this readMe template. They use content from [Hindawi](https://www.hindawi.com/research.data/#statement.templates). Other content was adapted  from [Fort (2016)](https://doi.org/10.1093/restud/rdw057), Supplementary data, with the author's permission.
