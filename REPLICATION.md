# [AERI-2021-0539.R5] [Technical Change and Superstar Effects: Evidence from the Rollout of Television] Validation and Replication results

> Some useful links:
> - [Official Data and Code Availability Policy](https://www.aeaweb.org/journals/policies/data-code)
> - [Step by step guidance](https://aeadataeditor.github.io/aea-de-guidance/) 
> - [Template README](https://social-science-data-editors.github.io/template_README/)

SUMMARY
-------

(updated 2022-07-15)

Thank you for your updated replication archive. The manuscript relies on several sources of IPUMS data, which were problematic in the first round. We have obtained copies of the data from the author, with IPUMS permission, and the author has provided extensive information on how to re-extract the data from IPUMS. All sample data (not 100% data) should be provided. We are still investigating a discrepancy between the author's extract specification and what IPUMS claims is available, but currently attribute the ambiguity to incomplete record keeping at IPUMS with respect to changes.

All figures and tables were successfully created. Some are hard to assess (formatting differs substantially from paper). The manuscript tables 1 and 2 both have small numerical differences (not attributable to rounding). Figure 1 has small but visible differences. Other manuscript figures seem to have reproduced exactly. Figure A1 is mislabeled in output as Figure B1. 

The code still retains several bugs which the author will need to fix prior to publication of the deposit. They significantly delayed running of the code, which otherwise is relatively clean. All computational requirements need to be listed in the README, and a setup program should be provided that installs all identified Stata packages.

The author has  added  no data citations to the manuscript. Much of the data is described - attribution-free - on pages 8 and 9 of the manuscript. While the details may be in an appendix, the data sources that are key to the analysis must be cited upon first mention. The appendix also does not cite the data sources. Please add data citations throughout the manuscript, at all relevant locations. 

One data set (`TVsignal.dta`) is of particular concern. It is attributed to a co-authored unpublished working paper; however, the author refers to it in the text as a solo-created dataset. The dataset must be published, but can be published separately to be able to be re-used in other publications more easily and/or establish precedence and/or assign different co-authorship. If published separately, it must be properly cited in manuscript and README.

**Conditional on making the requested changes to the openICPSR deposit prior to publication, the replication package is accepted.**

In assessing compliance with our [Data and Code Availability Policy](https://www.aeaweb.org/journals/policies/data-code), we have identified the following issues, which we ask you to address:


### Action Items (manuscript)

- [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).


### Action Items (openICPSR)

- [REQUIRED] Please consolidate all data sources in one section in the README as per the template README guidance.
- [REQUIRED] Please address concerns about proper data authorship of the TV signal data. It must be published. 
- [REQUIRED] Please clarify Dorn data. If the data were provided privately, and do not correspond to any data on David Dorn's website, you should not cite David Dorn's website as the source. 
- [REQUIRED] Please provide debugged code.
- [REQUIRED] Please provide code for Figure 1A. 
- [REQUIRED] Please rename programs and output to match the paper.
- [REQUIRED] Please provide complete computational requirements in the README.
- [REQUIRED] Please add a setup program that installs all Stata packages. Please specify all necessary commands. An example of a setup file can be found at [https://gist.github.com/c3dddbcf73e7534a22e3583b3422d7c5](https://gist.github.com/c3dddbcf73e7534a22e3583b3422d7c5)

### Previously 

> [We REQUESTED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

This is still incomplete. See notes.

### Action Items (openICPSR)

> [We REQUESTED] Please clarify data citations and data usage (IPUMS).

Usage clarified. Citations added.

> [We REQUESTED] Please provide complete code, including for raw data ingest, appendix tables and figures, and identify source for inline numbers.

Additional code was provided, but code for Figure 1A, which is partially empirical and is computational, has not been provided.

> [We REQUESTED] Please provide your IPUMS extract specifications (the dofiles and DCT files provided with your extract).

Done.

> [We REQUESTED] Please provide a clear description of access modality and source location for each dataset. Some examples are given [on this website](https://social-science-data-editors.github.io/guidance/Requested_information_dcas.html). 

Done.

> [We REQUESTED] Please review authors and affiliations on the openICPSR deposit. In general, they are the same, and in the same order, as for the manuscript; however, authors can deviate from that order. In particular, it looks like your full affiliation has been added as an additional PI.

Fixed.

> [We REQUESTED] As specified in the [Policy](https://www.aeaweb.org/journals/data/data-code-policy) and the [DCAF](https://www.aeaweb.org/journals/forms/data-code-availability), the README shall follow the schema provided by the [Social Science Data Editors' template README](https://social-science-data-editors.github.io/guidance/template-README.html).

Done.

> [We REQUESTED] Please ensure that a (properly formatted) ASCII (txt), Markdown (md), or PDF version of the README are available in the data and code deposit.

Done. We do ask that you create a PDF version (unfortunately at this time, openICPSR cannot preview Markdown)

- [We SUGGESTED] We suggest you update the openICPSR metadata fields marked as (highly recommended) and (suggested), in order to improve findability of your data and code supplement. 

Done.


> The openICPSR submission process has changed. If you have not already done so, please "Change Status -> Submit to AEA" from your deposit Workspace.

> [NOTE] Since July 1, 2021, we will publish replication packages as soon as all requested changes to the deposit have been made. Please process any requested changes as soon as possible.



General
-------

> [We REQUESTED] As specified in the [Policy](https://www.aeaweb.org/journals/data/data-code-policy) and the [DCAF](https://www.aeaweb.org/journals/forms/data-code-availability), the README shall follow the schema provided by the [Social Science Data Editors' template README](https://social-science-data-editors.github.io/guidance/template-README.html).

Done.

> [We REQUESTED] Please ensure that a (properly formatted) ASCII (txt), Markdown (md), or PDF version of the README are available in the data and code deposit.

Done.

Data description
----------------

### Data Sources 

The data source section in the README is incomplete. Additional data sources are listed throughout the text. 

> [REQUIRED] Please consolidate all data sources in one section in the README as per the template README guidance.

#### IPUMS decenial Census 1920-1970

- Dataset is not provided, and no link is provided in the README. 
- No codebooks or direction for what to download are provided.
- The data are cited in the references section of the manuscript and the README. Data citation:
  > Steven Ruggles, Catherine A. Fitch, Ronald Goeken, J. David Hacker, Matt A. Nelson, Evan Roberts, Megan Schouweiler, and Matthew Sobek. IPUMS Ancestry Full Count Data: Version 3.0 [dataset]. Minneapolis, MN: IPUMS, 2021.
- The README states that these are "confidential data", however, they are only confidential if using the "restricted name data". Otherwise, they are available to the public, but subject to a non-redistribution requirement. This should be clarified.
- README describes "full count 1920-1970" - There is no such thing as "full count data 1950-1970" (such data has not yet been released - 1950 data will be released this year). You must specify whether you used the 1% or 5% versions of all data, as per the IPUMS extract system (e.g., https://usa.ipums.org/usa/sampdesc.shtml#us1950a)

> [We REQUESTED] Please provide your IPUMS extract specification (the dofiles and DCT files provided with your extract).

- Done. The IPUMS ingestion dofiles (in code/IPUMS/) and dictionary files (in orig/IPUMS dictionary/) are now part of the replication package.

> [We REQUESTED] Please provide a clear description of access modality and source location for this dataset. Some examples are given [on this website](https://social-science-data-editors.github.io/guidance/Requested_information_dcas.html).

IPUMS read-in files are provided, and JSON extract specifications for IPUMS beta have been requested from IPUMS. These will be added by the Data Editor to the deposit.

> [We REQUESTED] Please clarify data citations and data usage.

- Done. The list of IPUMS data sources is now part of the README. 

#### IPUMS decenial Census, full count 1940

- Dataset is not provided (confidential), and no link is provided in the README. 
- Access conditions are not described. IPUMS requires an application for access, but once registered data is free of charge. 
- No codebooks or direction for what to download are provided.
- The data are cited in the references section of the manuscript and the README. Data citation:
  > Steven Ruggles, Sarah Flood, Sophia Foster, Ronald Goeken, Jose Pacas, Megan Schouweiler and Matthew Sobek. IPUMS USA: Version 11.0 [dataset]. Minneapolis, MN: IPUMS, 2021. https://doi.org/10.18128/D010.V11.0

> [We REQUESTED] Please provide your IPUMS data extract and the IPUMS extract specification (the dofiles and DCT files provided with your extract).

- Done. The IPUMS ingestion dofiles (in code/IPUMS/) and dictionary files (in orig/IPUMS dictionary/) are now part of the replication package.

> [We REQUESTED] Please provide a clear description of access modality and source location for this dataset. Some examples are given [on this website](https://social-science-data-editors.github.io/guidance/Requested_information_dcas.html). 
  - In particular, describe the variables that need to be requested.
- Not done. While the updated README states the access modality and source location, and that the lists of required IPUMS variables are available in the `data/IPUMS` folder, it seems that this directory is not provided in the replication package.

> [We REQUESTED] Please clarify data citations.
- Done. The list of IPUMS data sources is now part of the README.


#### Fenton & König(TV signal data) (2021)

- Dataset is not provided
- Data are only available upon request (present author is co-creator of the data)
- The paper is cited in the references section of the manuscript and the README, but differently as seen below. However, neither of which are a data citation.

> Fenton, G., & Koenig, F. (2021). Labor Supply and Entertainment Innovations: Evidence From the U.S. TV Rollout.

> Fenton, George and Felix Koenig (2020). “Labor Supply and Innovation in Entertainment: Evidence from TV”. Working paper

> [We REQUESTED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

- Not done. While the proper data citation has been provided in the response to the data editor and the README, the right citation is missing from the manuscript. 
- We also note that in the manuscript, the likely reference to these data mentions "solo authorship" ("I digitize information on these blocked locations, and use them for placebo tests in the analysis"). 

> [REQUIRED] Please address concerns about proper data authorship of the TV signal data.

> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

> [We REQUESTED] Please provide a clear description of access modality and source location for this dataset. Some examples are given [on this website](https://social-science-data-editors.github.io/guidance/Requested_information_dcas.html). 

- Done. The README states that the data was provided to the authors privately from original authors upon request.

> [REQUIRED] While we acknowledge the desire of the author to guarantee credit for this data file, it must be published. 

It is acceptable to publish separately in a data deposit (if co-authorship of the deposit). It is not acceptable to maintain "upon-request-only" data. If the data is actually solo authored, there is no exception to this rule.

#### TV studio data (1949)

> The description of TV studio (shoots) data and TV Stations is commingled in the README. This should be separated, for clarity. These are two different data sources.

- Dataset is provided, extracted from IMBD data
- An archival link for the source is provided. 
- Access conditions are provided, and permission acknowledged.
- The data is not cited in the manuscript or README. 

> [We REQUESTED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

- Not done. While a data citation is provided in the response to the data editor and the README, it is still missing from the manuscript. Data citation provided in response and README:

    > Internet Movie Database [dataset]. www.imdb.com . Accessed May 2018: ftp://ftp.fu-berlin.de/pub/misc/movies/database/frozendata/ 


> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

> [We REQUESTED] Please provide a clear description of access modality and source location for this dataset. Some examples are given [on this website](https://social-science-data-editors.github.io/guidance/Requested_information_dcas.html). 

- Done. 

#### TV Station data (from TV Digest)

- README states that data was handcollected and digitized by the author from historic magazines called "TV Digest". Manuscript mentions "Television Digest".
- Data are provided
- Source is acknowledged, with URL provided.
- Data are NOT cited in the manuscript ("Television Digest" is noted, but not cited)

> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

#### Autor and Dorn (2013)

- Dataset is provided, but a link is not provided in the readme.
- Census to Commuting Zone Crosswalks from years 1950, 1970. 
- It is not clear to the replicator where the author downloaded this data from. 
- The paper is cited in the references section of the manuscript and the README. However, this is not a data citation.

  > Autor, David H. and David Dorn (2013). “The Growth of Low Skill Service Jobs and the Polarization of the U.S. Labor Market”. In: American Economic Review 103.5, pp. 1553–97.

> [We REQUESTED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

- Not done. While the proper data citation has been added to the resonse to the data editor and the README, it is not included in the manuscript. Data citation provided in response and README:

    > Autor, David H. Dorn, David. Replication Data for: The Growth of Low-Skill Service Jobs and the Polarization of the US Labor Market American Economic Review 103 5 1553-97 2013 10.1257/aer.103.5.1553, [data access](https://www.ddorn.net/data.htm)

Correct citation:

> >   Dorn, David. "Replication Data for: The Growth of Low-Skill Service Jobs and the Polarization of the US Labor Market". Accessed at(https://www.ddorn.net/data.htm) on DATE.

> [We REQUESTED] Please provide a clear description of access modality and source location for this dataset. Some examples are given [on this website](https://social-science-data-editors.github.io/guidance/Requested_information_dcas.html). 

- Done. While not completely clear, the README suggests that this data was provided to the authors privately from original authors upon request.

> [REQUIRED] Please clarify. If the data were provided privately, and do not correspond to any data on David Dorn's website, you should not cite David Dorn's website as the source. 
  - The source is then "private correspondence". 
  - However, you should validate whether the data are, in fact, available on David Dorn's website.

#### Manson et al. (IPUMS) (2021) 

- Dataset is not provided, (confidential).
- Access conditions are not described. IPUMS requires an application for access, but once registered data is free of charge.  
- No codebooks or direction for what to download are provided.
- Census to Commuting Zone Crosswalks for Years 1920, 1930, 1940, 1960. 
- The data are cited in the README. Data citation:
  > Steven Manson, Jonathan Schroeder, David Van Riper, Tracy Kugler, and Steven Ruggles. IPUMS National Historical Geographic Information System: Version 16.0 [dataset]. Minneapolis, MN: IPUMS. 2021.

> [We REQUESTED] Please provide a clear description of access modality and source location for this dataset. Some examples are given [on this website](https://social-science-data-editors.github.io/guidance/Requested_information_dcas.html). 

- Done. README states that data can be downloaded from NHGIS upon registration. 

> [NOTE] IPUMS has a new API system for creating (reproducible) extracts from NHGIS data. See [https://developer.ipums.org/docs/get-started/](https://developer.ipums.org/docs/get-started/). We strongly encourage you to Construct a valid JSON-formatted request, and provide that JSON file as part of the repository. Please feel free to reach out to IPUMS for assistance.

### Demographics of TV stars

- Source is "Radio Annual and Television Yearbook" (1949)
- Not cited.
- Data are provided.
- Source (internet archive) is provided.


> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

### US 1940 Census records 

- These are different from IPUMS files
- (these records are available here:https://1940census.archives.gov/)
- Not cited.
- Provided via a hand-matched file.


> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

### State FIPS to ICSPR Codes Crosswalk

- URL provided
- Data provided
- Data not cited


> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

### IRS data

- Source provided
- Data provided
- Data cited inline in README, not in the correct section, not in the manuscript.


> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

### Analysis Data Files

- [x] No analysis data file mentioned
- [ ] Analysis data files mentioned, not provided (explain reasons below)
- [ ] Analysis data files mentioned, provided. File names listed below.

Data deposit
------------

### Requirements 

- [x] README is in TXT, MD, PDF format
- [x] openICPSR deposit has no ZIP files
- [x] Title conforms to guidance (starts with "Data and Code for:" or "Code for:", is properly capitalized)
- [x] Authors (with affiliations) are listed in the same order as on the paper

> [We REQUESTED] Please review authors and affiliations on the openICPSR deposit. In general, they are the same, and in the same order, as for the manuscript; however, authors can deviate from that order. In particular, it looks like your full affiliation has been added as an additional PI.

- Done. Author affiliation on the openICPSR deposit matches that of the manuscript. 


### Deposit Metadata

- [x] JEL Classification (required)
- [x] Manuscript Number (required)
- [x] Subject Terms (highly recommended)
- [x] Geographic coverage (highly recommended)
- [x] Time period(s) (highly recommended)
- [ ] Collection date(s) (suggested)
- [ ] Universe (suggested)
- [x] Data Type(s) (suggested)
- [ ] Data Source (suggested)
- [x] Units of Observation (suggested)

- [NOTE] openICPSR metadata is sufficient.

> [We SUGGESTED] We suggest you update the openICPSR metadata fields marked as (highly recommended), in order to improve findability of your data and code supplement. 

- Done. Geographic coverage and time periods have now been added to the openICPSR deposit.

> [We SUGGESTED] We suggest you update the openICPSR metadata fields marked as (suggested), in order to improve findability of your data and code supplement. 

- Not done. collection dates, universe, and data source are still missing from the openICPSR deposit. This is fine.


For additional guidance, see [https://aeadataeditor.github.io/aea-de-guidance/data-deposit-aea-guidance.html](https://aeadataeditor.github.io/aea-de-guidance/data-deposit-aea-guidance.html).

Data checks
-----------

- Data is in .dta format. It can be read using Stata. All .dta files are missing some if not all variable labels. 
- Ran check for PII ([PII_stata_scan.do](PII_stata_scan.do), sourced from [here](https://github.com/J-PAL/stata_PII_scan)) and results look to be false positives.


Code description
----------------

There are 40 provided Stata do files, including a "master.do".

- The replicator could not identify code that generated the appendix figures and appendix tables files. It appears it was not provided in the deposit.  

- [x] The replication package contains a "main" or "master" file(s) which calls all other auxiliary programs.

| Data Preparation Program | Dataset created                            |
|--------------------------|--------------------------------------------|
| data_preparation.do      | census_CZ.dta                              |
| earnings_rank.do         | earnings_rank.dta                          |
| employment.do            | F2B.dta, employment_T1.dta, employment.dta |
| migration.do             | migration.dta                              |

| Figure #  | Program     | Line Number |
|-----------|-------------|-------------|
| Figure 1A | none        |             |
| Figure 1B | Figure1B.do | 34          |
| Figure 2A | Figure2.do  | 17          |
| Figure 2B | Figure2.do  | 25          |
| Figure 3  | Figure3.do  | 112         |
| Figure A1 | FigureB1.do | 9           |

| Table #  | Program    | Line Number |
|----------|------------|-------------|
| Table 1  | Table1.do  | 95          |
| Table 2  | Table2.do  | 40          |
| Table B1 | TableB1.do | 38, 58, 88  |
| Table B2 | TableB2.do | 35          |
| Table B3 | TableB3.do | 55          |
| Table B4 | TableB4.do | 60          |
| Table B5 | TableB5.do | 69          |
| Table B6 | TableB6.do | 25          |
| Table B7 | TableB7.do | 31          |

> [We REQUESTED] Please provide complete code, including for appendix tables and figures, and identify source for inline numbers.

- Mostly Done. Replication package now includes all programs for appendix tables and figures, except for Figure 1A.

Figure 1A is "theoretical", yet author states in Figure notes "stylized shock that changes the scale parameter by factor 1.3 and the intercept by 0.2. The wage levels that correspond to Pxx are chosen from the US wage distribution to simplify comparison with the empirical results." As per AEA Policy, all computations need to be provided as part of the replication package. The source of the "Pxx" are not theoretical, as they relate to the US wage distribution at a particular point in time. 

We suggest reproducing the calculations in Stata, identifying the source of the US wage distribution numbers.

> [REQUIRED] Please provide code for Figure 1A. 

> [REQUIRED] Please rename programs and output to match the paper.
  - Figure A1 is called `Figure B1.pdf` and produced by `Figure B1.do`.

> [NOTE] You could simplify the computation of Figure 1B by using the "`putexcel`" or "`export excel`" functionality in Stata, rather than providing manual instructions.

Stated Requirements
--------------------

- [ ] No requirements specified
- [x] Software Requirements specified as follows:
  - [x] Stata version 17
    - Packages: `estout`, `reghdfe`, `gtools`, `egenmore`
- [x] Computational Requirements specified as follows:
  - code last run on 8-core M1-based laptop with MacOS version 11.5.2
- [x] Time Requirements specified as follows:
  - 2 hours and 18 minutes

- [ ] Requirements are complete.


> [We SUGGESTED] Please specify hardware requirements, and duration (execution time) for the last run, to allow replicators to assess the computational requirements.

- Done. README now includes computational and time requirements.

Missing Requirements
--------------------

- [x] Software Requirements 
  - [x] Stata  Packages: `ftools coefplot parmest distplot` and scheme `lean2` (`gr0002_3`)

> [REQUIRED] Please provide complete computational requirements in the README.

> [REQUIRED] Please add a setup program that installs all Stata packages. Please specify all necessary commands. An example of a setup file can be found at [https://gist.github.com/c3dddbcf73e7534a22e3583b3422d7c5](https://gist.github.com/c3dddbcf73e7534a22e3583b3422d7c5)

Computing Environment of the Replicator
---------------------

* "openSUSE Leap 15.3"
* AMD Ryzen 9 3900X 12-Core Processor, 24 cores , 31GB memory 
* Docker version 20.10.14-ce, build 87a90dc786bd 
* stata version 17 (Docker image dataeditors/stata17:2022-01-17)

Replication steps
-----------------

1. Copied code from ICPSR
2. Obtained data files from author (see separate note above about using the JSON method), placed in `data/IPUMS` as instructed by README
3. Added config.do to install packages, set directories
   - [NOTE] Directories need to be created (`processed_data`,`results`,`log`) when using our Git clone. These are present on ICPSR.
4. Ran `0.master.do`
   - [ERROR] `${do_path}/IPUMS/usa_1920.do"` reads `dat` files from `${orig}=orig`, not from `data/IPUMS` as specified in README. [FIX] Created symbolic links to `dat` files in `${orig}`
   - [ERROR] `"${orig}/CZ crosswalk/cw_cty'year'_czone",` is actually in `CZ-crosswalk` - all code seems to refer to the directory with a space, renamed the directory on-disk
   - Same occurs for directory `"${orig}/ID lists"`
   - [ERROR] wants to read `micro_med1920_1_100.dta` but there's only `micro_med1920_1_20901.dta`. Non-fatal, but might need explaining.
   - [ERROR] wants to read "`TVStudio.dta`" but deposit contains "`TVstudio.dta`" (case-sensitive file system). Renamed file, and adjusted the one instance where "`TVstudio.dta`" is read - needs to have consistency imposed.
   - [ERROR] Wanted to use scheme `lean2` - not present. Added `net install gr0002_3, from(http://www.stata-journal.com/software/sj4-3)` to include it.
   - [ERROR] `ftools` not installed - added to installation parts.
   - [ERROR] `coefplot` not installed - added to installation parts.
   - [ERROR] `parmest` not installed - added to installation parts.
   - [ERROR] `distplot` not installed - added to installation parts.
5. Code finished.

Findings
--------

| Data Preparation Program | Dataset created                            |             | Reproduced? |
|--------------------------|--------------------------------------------|-------------|-------------|
| data_preparation.do      | census_CZ.dta                              |             | yes         |
| earnings_rank.do         | earnings_rank.dta                          |             | yes         |
| employment.do            | F2B.dta, employment_T1.dta, employment.dta |             | yes         |
| migration.do             | migration.dta                              |             | yes         |

| Figure #                 | Program                                    | Line Number | Reproduced? |
|--------------------------|--------------------------------------------|-------------|-------------|
| Figure 1A                | none                                       |             | no          |
| Figure 1B                | Figure1B.do                                | 34          | yes         |
| Figure 2A                | Figure2.do                                 | 17          | yes         |
| Figure 2B                | Figure2.do                                 | 25          | yes         |
| Figure 3                 | Figure3.do                                 | 112         | yes         |
| Figure A1                | FigureB1.do                                | 9           | No          |

| Table #                  | Program                                    | Line Number | Reproduced? |
|--------------------------|--------------------------------------------|-------------|-------------|
| Table 1                  | Table1.do                                  | 95          | no          |
| Table 2                  | Table2.do                                  | 40          | no          |
| Table B1                 | TableB1.do                                 | 38, 58, 88  | not checked |
| Table B2                 | TableB2.do                                 | 35          | not checked |
| Table B3                 | TableB3.do                                 | 55          | not checked |
| Table B4                 | TableB4.do                                 | 60          | not checked |
| Table B5                 | TableB5.do                                 | 69          | not checked |
| Table B6                 | TableB6.do                                 | 25          | not checked |
| Table B7                 | TableB7.do                                 | 31          | not checked |

| In-text numbers          | Program                                    | Line Number | Reproduced? |
|--------------------------|--------------------------------------------|-------------|-------------|
| n/a                      | not checked                                |

Figure A1 as reproduced:
![Figure A1 reproduced](/162421/results/FigureB1.png)

Figure A1 in the paper:

![paper A1](reproduced-FigureA1.png)

Classification
--------------

- [ ] full reproduction
- [x] full reproduction with minor issues
- [ ] partial reproduction (see above)
- [ ] not able to reproduce most or all of the results (reasons see above)

### Reason for incomplete reproducibility


- [x] `Discrepancy in output` (either figures or numbers in tables or text differ)
- [x] `Bugs in code`  that  were fixable by the replicator (but should be fixed in the final deposit)
- [x] `Code missing`, in particular if it  prevented the replicator from completing the reproducibility check
  - [ ] `Data preparation code missing` should be checked if the code missing seems to be data preparation code
- [ ] `Code not functional` is more severe than a simple bug: it  prevented the replicator from completing the reproducibility check
- [ ] `Software not available to replicator`  may happen for a variety of reasons, but in particular (a) when the software is commercial, and the replicator does not have access to a licensed copy, or (b) the software is open-source, but a specific version required to conduct the reproducibility check is not available.
- [ ] `Insufficient time available to replicator` is applicable when (a) running the code would take weeks or more (b) running the code might take less time if sufficient compute resources were to be brought to bear, but no such resources can be accessed in a timely fashion (c) the replication package is very complex, and following all (manual and scripted) steps would take too long.
- [ ] `Data missing` is marked when data *should* be available, but was erroneously not provided, or is not accessible via the procedures described in the replication package
- [ ] `Data not available` is marked when data requires additional access steps, for instance purchase or application procedure. 
- [x] `Other` - data extract not reproducible at this time. Probably provider problem, not under author control.

